import oauth2orize = require('oauth2orize');
import passport = require('passport');
import passportHttp = require('passport-http');
import passportHttpBearer = require('passport-http-bearer');
import { tokenRepository } from "./models/database/repository/implementation/mongoose4/oauth2/token-repository";
import { clientRepository } from "./models/database/repository/implementation/mongoose4/oauth2/client-repository";
import { codeRepository } from "./models/database/repository/implementation/mongoose4/oauth2/code-repository";
import { IClient } from "./models/interface/oauth2/client";
import { IUser } from "./models/interface/oauth2/user";
import { ICode } from "./models/interface/oauth2/code";
import { tokenExpiresIn } from "./config/oauth2";
import { IToken } from "./models/interface/oauth2/token";
import { userRepository } from "./models/database/repository/implementation/mongoose4/oauth2/user-repository";
import {
    verifyPassword,
    createTokenPair,
    TokenPair,
    createAccessToken,
    createAccessCode
} from "./middleware/authentication";
import { ObjectID } from "mongodb";

export const oauthServer = oauth2orize.createServer();

oauthServer.serializeClient((client: IClient, done) => {
    return done(null, client.id);
});

oauthServer.deserializeClient((id: string, done) => {
    clientRepository.findById(id)
        .then((client: IClient) => done(null, client))
        .catch(err => done(err));
});

oauthServer.grant(oauth2orize.grant.code((client: IClient, redirectURI: string, user: IUser, ares, done) => {
    createAccessCode()
        .then((code: string) => codeRepository.create(
            {
                client: client.id,
                user:   user.id,
                value:  code
            }
        ))
        .then((code: ICode) => done(null, code.value))
        .catch(err => done(err));
}));

oauthServer.exchange(oauth2orize.exchange.code((client: IClient, code: string, redirectURI: string, done) => {
    let codeDocument: ICode;
    codeRepository.findByValue(code)
        .then((_codeDocument: ICode) => {
            codeDocument = _codeDocument;

            if (!client.id.equals(<ObjectID>codeDocument.client)) return Promise.reject(false);
            if (redirectURI !== client.redirectUri) return Promise.reject(false);

            return createTokenPair();
        })
        .then((t: TokenPair) => tokenRepository.create(
            {
                client:       client.id,
                user:         codeDocument.user,
                value:        t.accessToken,
                refreshToken: t.refreshToken,
                expiresIn:    new Date(Date.now() + tokenExpiresIn * 1000.0)
            }
        ))
        .then((token: IToken) => done(null, token.value, token.refreshToken, {expires_in: tokenExpiresIn}))
        .catch(err => {
            if (err === false) {
                done(null, false);
            } else {
                done(err);
            }
        });
}));

oauthServer.exchange(oauth2orize.exchange.password((client: IClient, username: string, password: string, scope, done) => {
    let user: IUser;
    userRepository.findByEmail(username)
        .then((_user: IUser) => {
            user = _user;
            return verifyPassword(password, user.password);
        })
        .then(() => createTokenPair())
        .then((t: TokenPair) => tokenRepository.create(
            {
                client:       client.id,
                user:         user.id,
                value:        t.accessToken,
                refreshToken: t.refreshToken,
                expiresIn:    new Date(Date.now() + tokenExpiresIn * 1000.0)
            }
        ))
        .then((token: IToken) => done(null, token.value, token.refreshToken, {expires_in: tokenExpiresIn}))
        .catch(err => done(err));
}));

oauthServer.exchange(oauth2orize.exchange.refreshToken((client: IClient, refreshToken: string, scope, done) => {
    let token: IToken;
    tokenRepository.findByRefreshToken(refreshToken)
        .then((_token: IToken) => {
            token = _token;
            return createAccessToken();
        })
        .then((_token: string) => {
            token.value     = _token;
            token.expiresIn = new Date(Date.now() + tokenExpiresIn * 1000.0);
            return tokenRepository.update(token);
        })
        .then(() => done(null, token.value, token.refreshToken, {expires_in: tokenExpiresIn}))
        .catch(err => done(err));
}));