import { IBooking } from "../models/interface/booking";
import { IRegularBooking } from "../models/interface/regular-booking";
import { IChristmasBooking } from "../models/interface/christmas-booking";
import { IExternalBooking } from "../models/interface/external-booking";
import { regularBookingRepository } from "../models/database/repository/implementation/mongoose4/regular-booking-repository";
import { christmasBookingRepository } from "../models/database/repository/implementation/mongoose4/christmas-booking-repository";
import { externalBookingRepository } from "../models/database/repository/implementation/mongoose4/external-booking-repository";

export function fetchBookings(): Promise<IBooking[]> {
    let allRegular   = fetchRegularBookings();
    let allChristmas = fetchChristmasBookings();
    let allExternal  = fetchExternalBookings();

    return Promise.all([allRegular, allChristmas, allExternal])
        .then(bookings => [...bookings[0], ...bookings[1], ...bookings[2]]);
}


// === Regular Booking ===
export function addRegularBooking(regularBooking: IRegularBooking): Promise<IRegularBooking> {
    return regularBookingRepository.create(regularBooking);
}

export function updateRegularBooking(regularBooking: IRegularBooking): Promise<IRegularBooking> {
    return regularBookingRepository.update(regularBooking);
}

export function updateRegularBookingById(id: string, regularBooking: IRegularBooking): Promise<IRegularBooking> {
    return regularBookingRepository.updateById(id, regularBooking);
}

export function deleteRegularBooking(regularBooking: IRegularBooking): Promise<void> {
    return regularBookingRepository.remove(regularBooking);
}

export function deleteRegularBookingById(id: string): Promise<void> {
    return regularBookingRepository.removeById(id);
}

export function fetchRegularBookings(): Promise<IRegularBooking[]> {
    return regularBookingRepository.findAll();
}

export function getRegularBookingById(id: string): Promise<IRegularBooking> {
    return regularBookingRepository.findById(id);
}


// === Christmas Booking ===
export function addChristmasBooking(christmasBooking: IChristmasBooking): Promise<IChristmasBooking> {
    return christmasBookingRepository.create(christmasBooking);
}

export function updateChristmasBooking(christmasBooking: IChristmasBooking): Promise<IChristmasBooking> {
    return christmasBookingRepository.update(christmasBooking);
}

export function updateChristmasBookingById(id: string, christmasBooking: IChristmasBooking): Promise<IChristmasBooking> {
    return christmasBookingRepository.updateById(id, christmasBooking);
}

export function deleteChristmasBooking(christmasBooking: IChristmasBooking): Promise<void> {
    return christmasBookingRepository.remove(christmasBooking);
}

export function deleteChristmasBookingById(id: string): Promise<void> {
    return christmasBookingRepository.removeById(id);
}

export function fetchChristmasBookings(): Promise<IChristmasBooking[]> {
    return christmasBookingRepository.findAll();
}

export function getChristmasBookingById(id: string): Promise<IChristmasBooking> {
    return christmasBookingRepository.findById(id);
}


// === External Booking ===
export function addExternalBooking(externalBooking: IExternalBooking): Promise<IExternalBooking> {
    return externalBookingRepository.create(externalBooking);
}

export function updateExternalBooking(externalBooking: IExternalBooking): Promise<IExternalBooking> {
    return externalBookingRepository.update(externalBooking);
}

export function updateExternalBookingById(id: string, externalBooking: IExternalBooking): Promise<IExternalBooking> {
    return externalBookingRepository.updateById(id, externalBooking);
}

export function deleteExternalBooking(externalBooking: IExternalBooking): Promise<void> {
    return externalBookingRepository.remove(externalBooking);
}

export function deleteExternalBookingById(id: string): Promise<void> {
    return externalBookingRepository.removeById(id);
}

export function fetchExternalBookings(): Promise<IExternalBooking[]> {
    return externalBookingRepository.findAll();
}

export function getExternalBookingById(id: string): Promise<IExternalBooking> {
    return externalBookingRepository.findById(id);
}