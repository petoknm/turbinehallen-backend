export function checkPermissions(username: string): Promise<void> {
    return new Promise<void>((resolve, reject) => {
        if (username === 'peter') {
            resolve(null);
        } else {
            reject('Access denied');
        }
    });
}