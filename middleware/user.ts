import { IUser } from "../models/interface/oauth2/user";
import { validateUser } from "../validation/user";
import { userRepository } from "../models/database/repository/implementation/mongoose4/oauth2/user-repository";

export function extractUser(req): Promise<IUser> {
    const user = req.body;
    if (validateUser(user)) {
        return Promise.resolve(user);
    } else {
        return Promise.reject('Invalid user');
    }
}

export function saveUser(user: IUser): Promise<IUser> {
    return userRepository.create(user);
}

export function removeUser(user: IUser): Promise<void> {
    return userRepository.remove(user);
}