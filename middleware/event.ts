import { IEvent } from "../models/interface/event";
import { IRegularEvent } from "../models/interface/regular-event";
import { IChristmasEvent } from "../models/interface/christmas-event";
import { IExternalEvent } from "../models/interface/external-event";
import { regularEventRepository } from "../models/database/repository/implementation/mongoose4/regular-event-repository";
import { christmasEventRepository } from "../models/database/repository/implementation/mongoose4/christmas-event-repository";
import { externalEventRepository } from "../models/database/repository/implementation/mongoose4/external-event-repository";
import { validateRegularEvent, validateRegularEventFromJson } from "../validation/event";
import { isString } from "util";
import { internalLocationRepository } from "../models/database/repository/implementation/mongoose4/internal-location-repository";
import { IInternalLocation } from "../models/interface/internal-location";
import { ObjectID } from "mongodb";

export function fetchEvents(): Promise<IEvent[]> {
    let allRegular   = fetchRegularEvents();
    let allChristmas = fetchChristmasEvents();
    let allExternal  = fetchExternalEvents();

    return Promise.all([allRegular, allChristmas, allExternal])
        .then(events => [...events[0], ...events[1], ...events[2]]);
}

// === Regular Event ===
export function addRegularEvent(regularEvent: IRegularEvent): Promise<IRegularEvent> {
    regularEvent.bookings = [];
    return internalLocationRepository.create(<IInternalLocation>regularEvent.place)
        .then(location => {
            regularEvent.place = location.id;
            return regularEventRepository.create(regularEvent);
        });
}

export function updateRegularEvent(regularEvent: IRegularEvent): Promise<IRegularEvent> {
    return regularEventRepository.update(regularEvent);
}

export function updateRegularEventById(id: string, regularEvent: IRegularEvent): Promise<IRegularEvent> {
    return regularEventRepository.updateById(id, regularEvent);
}

export function deleteRegularEvent(regularEvent: IRegularEvent): Promise<void> {
    return regularEventRepository.remove(regularEvent);
}

export function deleteRegularEventById(id: string): Promise<void> {
    return regularEventRepository.removeById(id);
}

export function fetchRegularEvents(): Promise<IRegularEvent[]> {
    return regularEventRepository.findAll()
        .then(events => Promise.all(events.map(e => internalLocationRepository.findById(e.place.toString()).then(l => {
                e.place = l;
                return e;
            })))
        );
}

export function getRegularEventById(id: string): Promise<IRegularEvent> {
    return regularEventRepository.findById(id);
}

export function extractRegularEvent(req): Promise<IRegularEvent> {
    let regularEvent = req.body;
    if (isString(regularEvent.startDate))
        regularEvent.startDate = new Date(regularEvent.startDate);

    if (isString(regularEvent.endDate))
        regularEvent.endDate = new Date(regularEvent.endDate);

    console.log(regularEvent);
    if (validateRegularEventFromJson(regularEvent)) {
        console.log(regularEvent);
        return Promise.resolve(regularEvent);
    } else {
        return Promise.reject('Invalid event');
    }
}
// === Christmas Event ===
export function addChristmasEvent(christmasEvent: IChristmasEvent): Promise<IChristmasEvent> {
    return christmasEventRepository.create(christmasEvent);
}

export function updateChristmasEvent(christmasEvent: IChristmasEvent): Promise<IChristmasEvent> {
    return christmasEventRepository.update(christmasEvent);
}

export function updateChristmasEventById(id: string, christmasEvent: IChristmasEvent): Promise<IChristmasEvent> {
    return christmasEventRepository.updateById(id, christmasEvent);
}

export function deleteChristmasEvent(christmasEvent: IChristmasEvent): Promise<void> {
    return christmasEventRepository.remove(christmasEvent);
}

export function deleteChristmasEventById(id: string): Promise<void> {
    return christmasEventRepository.removeById(id);
}

export function fetchChristmasEvents(): Promise<IChristmasEvent[]> {
    return christmasEventRepository.findAll();
}

export function getChristmasEventById(id: string): Promise<IChristmasEvent> {
    return christmasEventRepository.findById(id);
}


// === External Event ===
export function addExternalEvent(externalEvent: IExternalEvent): Promise<IExternalEvent> {
    return externalEventRepository.create(externalEvent);
}

export function updateExternalEvent(externalEvent: IExternalEvent): Promise<IExternalEvent> {
    return externalEventRepository.update(externalEvent);
}

export function updateExternalEventById(id: string, externalEvent: IExternalEvent): Promise<IExternalEvent> {
    return externalEventRepository.updateById(id, externalEvent);
}

export function deleteExternalEvent(externalEvent: IExternalEvent): Promise<void> {
    return externalEventRepository.remove(externalEvent);
}

export function deleteExternalEventById(id: string): Promise<void> {
    return externalEventRepository.removeById(id);
}

export function fetchExternalEvents(): Promise<IExternalEvent[]> {
    return externalEventRepository.findAll();
}

export function getExternalEventById(id: string): Promise<IExternalEvent> {
    return externalEventRepository.findById(id);
}