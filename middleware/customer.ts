import { ICustomer } from "../models/interface/customer";
import { IPrivateCustomer } from "../models/interface/private-customer";
import { IBusinessCustomer } from "../models/interface/business-customer";
import { privateCustomerRepository } from "../models/database/repository/implementation/mongoose4/private-customer-repository";
import { businessCustomerRepository } from "../models/database/repository/implementation/mongoose4/business-customer-repository";
import { Request } from "express-serve-static-core";
import { validateBusinessCustomer, validatePrivateCustomer } from "../validation/customer";

export function fetchCustomers(): Promise<ICustomer[]> {
    let allPrivate = fetchPrivateCustomers();
    let alBusiness = fetchBusinessCustomers();

    return Promise.all([allPrivate, alBusiness])
        .then(customers => [...customers[0], ...customers[1]]);
}


// === Private Customer ===
export function addPrivateCustomer(privateCustomer: IPrivateCustomer): Promise<IPrivateCustomer> {
    return privateCustomerRepository.create(privateCustomer);
}

export function updatePrivateCustomer(privateCustomer: IPrivateCustomer): Promise<IPrivateCustomer> {
    return privateCustomerRepository.update(privateCustomer);
}

export function updatePrivateCustomerById(id: string, privateCustomer: IPrivateCustomer): Promise<IPrivateCustomer> {
    return privateCustomerRepository.updateById(id, privateCustomer);
}

export function deletePrivateCustomer(privateCustomer: IPrivateCustomer): Promise<void> {
    return privateCustomerRepository.remove(privateCustomer);
}

export function deletePrivateCustomerById(id: string): Promise<void> {
    return privateCustomerRepository.removeById(id);
}

export function fetchPrivateCustomers(): Promise<IPrivateCustomer[]> {
    return privateCustomerRepository.findAll();
}

export function getPrivateCustomerById(id: string): Promise<IPrivateCustomer> {
    return privateCustomerRepository.findById(id);
}


// === BusinessCustomer ===
export function addBusinessCustomer(businessCustomer: IBusinessCustomer): Promise<IBusinessCustomer> {
    return businessCustomerRepository.create(businessCustomer);
}

export function updateBusinessCustomer(businessCustomer: IBusinessCustomer): Promise<IBusinessCustomer> {
    return businessCustomerRepository.update(businessCustomer);
}

export function updateBusinessCustomerById(id: string, businessCustomer: IBusinessCustomer): Promise<IBusinessCustomer> {
    return businessCustomerRepository.updateById(id, businessCustomer);
}

export function deleteBusinessCustomer(businessCustomer: IBusinessCustomer): Promise<void> {
    return businessCustomerRepository.remove(businessCustomer);
}

export function deleteBusinessCustomerById(id: string): Promise<void> {
    return businessCustomerRepository.removeById(id);
}

export function fetchBusinessCustomers(): Promise<IBusinessCustomer[]> {
    return businessCustomerRepository.findAll();
}

export function getBusinessCustomerById(id: string): Promise<IBusinessCustomer> {
    return businessCustomerRepository.findById(id);
}


export function extractPrivateCustomer(req: Request): Promise<IPrivateCustomer> {
    const customer = req.body;
    if (validatePrivateCustomer(customer)) {
        return Promise.resolve(customer);
    } else {
        return Promise.reject('Invalid customer');
    }
}

export function extractBusinessCustomer(req: Request): Promise<IBusinessCustomer> {
    const customer = req.body;
    if (validateBusinessCustomer(customer)) {
        return Promise.resolve(customer);
    } else {
        return Promise.reject('Invalid customer');
    }
}