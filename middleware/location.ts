import { ILocation } from "../models/interface/location";
import { IInternalLocation } from "../models/interface/internal-location";
import { IExternalLocation } from "../models/interface/external-location";
import { internalLocationRepository } from "../models/database/repository/implementation/mongoose4/internal-location-repository";
import { externalLocationRepository } from "../models/database/repository/implementation/mongoose4/external-location-repository";

export function fetchLocations(): Promise<ILocation[]> {
    let allInternal = fetchInternalLocations();
    let allExternal = fetchExternalLocations();

    return Promise.all([allInternal, allExternal])
        .then(locations => [...locations[0], ...locations[1]]);
}


// === Internal Location ===
export function addInternalLocation(internalLocation: IInternalLocation): Promise<IInternalLocation> {
    return internalLocationRepository.create(internalLocation);
}

export function updateInternalLocation(internalLocation: IInternalLocation): Promise<IInternalLocation> {
    return internalLocationRepository.update(internalLocation);
}

export function updateInternalLocationById(id: string, internalLocation: IInternalLocation): Promise<IInternalLocation> {
    return internalLocationRepository.updateById(id, internalLocation);
}

export function deleteInternalLocation(internalLocation: IInternalLocation): Promise<void> {
    return internalLocationRepository.remove(internalLocation);
}

export function deleteInternalLocationById(id: string): Promise<void> {
    return internalLocationRepository.removeById(id);
}

export function fetchInternalLocations(): Promise<IInternalLocation[]> {
    return internalLocationRepository.findAll();
}

export function getInternalLocationById(id: string): Promise<IInternalLocation> {
    return internalLocationRepository.findById(id);
}


// === External Location ===
export function addExternalLocation(externalLocation: IExternalLocation): Promise<IExternalLocation> {
    return externalLocationRepository.create(externalLocation);
}

export function updateExternalLocation(externalLocation: IExternalLocation): Promise<IExternalLocation> {
    return externalLocationRepository.update(externalLocation);
}

export function updateExternalLocationById(id: string, externalLocation: IExternalLocation): Promise<IExternalLocation> {
    return externalLocationRepository.updateById(id, externalLocation);
}

export function deleteExternalLocation(externalLocation: IExternalLocation): Promise<void> {
    return externalLocationRepository.remove(externalLocation);
}

export function deleteExternalLocationById(id: string): Promise<void> {
    return externalLocationRepository.removeById(id);
}

export function fetchExternalLocations(): Promise<IExternalLocation[]> {
    return externalLocationRepository.findAll();
}

export function getExternalLocationById(id: string): Promise<IExternalLocation> {
    return externalLocationRepository.findById(id);
}