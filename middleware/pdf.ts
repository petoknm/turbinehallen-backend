'use strict';
import PDFDocument = require("pdfkit");
import TextOptions = PDFKit.Mixins.TextOptions;
import IPDFDocument = PDFKit.PDFDocument;
import { IChristmasEvent } from "../models/interface/christmas-event";
import { IRegularEvent } from "../models/interface/regular-event";
import { IExternalEvent } from "../models/interface/external-event";
import { IUser } from "../models/interface/oauth2/user";
import { IRegularBooking } from "../models/interface/regular-booking";
import { ICustomer } from "../models/interface/customer";
import { ILocation } from "../models/interface/location";
import { IContactInfo } from "../models/interface/contact-info";
import { IChristmasBooking } from "../models/interface/christmas-booking";
import { IExternalBooking } from "../models/interface/external-booking";
import { IRegularEventInformation } from "../models/interface/regular-event-information";
import { IExternalEventInformation } from "../models/interface/external-event-information";
import { IChristmasEventInformation } from "../models/interface/christmas-event-information";

const FONT: string = 'Helvetica';
const BOLD_FONT: string = FONT + '-Bold';

function toStringDate(date: Date): string{
    const monthNames = [
        "January", "February", "March",
        "April", "May", "June", "July",
        "August", "September", "October",
        "November", "December"
    ];
    let day: string = date.getDay().toString();
    let year: string = date.getFullYear().toString();
    let stringDate: string = 'Aarhus, den ' + day + ', ' + monthNames[date.getMonth()] + ', ' + year;
    return stringDate;
}

function setContact(doc: IPDFDocument, customer1: ICustomer, customer2: ICustomer, contact: IContactInfo): void {
    let customer1Name: string = customer1['name']? customer1['name'] : customer1['firstName'] + ' '
    + customer1['lastName'];
    let customer2Name: string = customer2 && customer2['name']? customer2['name'] : customer2['firstName'] + ' '
    + customer2['lastName'];
    let contactName = contact? contact.firstName + ' ' + contact.lastName : customer1Name;// contact name
    let address = customer1.addresses.length > 0? customer1.addresses[0].street : '';// contact address
    let zipCode = customer1.addresses.length > 0? customer1.addresses[0].postcode + ' ' + customer1.addresses[0].city : '';// contact address
    let range = (<any>doc).bufferedPageRange();
    let tlf = contact && contact.phoneNumbers.length > 0? contact.phoneNumbers[0] : '';// contact address
    let email = contact && contact.emailAddresses.length > 0? contact.emailAddresses[0] : customer1.emailAddresses.length > 0? customer1.emailAddresses[0] : '';// contact email
    doc.switchToPage(range.start);

    let y = 100;
    let x = doc.page.margins.left;
    doc.fontSize(10)
        .font(BOLD_FONT);
    if(customer2Name){
        doc.text(customer1Name + ' & ' + customer2Name, x, y);
    } else {
        doc.text(customer1Name, x, y);
    }
    doc.font(FONT)
        .text(address)
        .text(zipCode);
    if(contactName){
        doc.font(BOLD_FONT)
            .text('Att.: ' + contactName)
            .font(FONT);
    }
    doc.text(tlf, x+20)
        .text(email, x+35);
}

function setDate(doc: IPDFDocument): void {
    let y: number    = 200;
    let range = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.start);
    let x: number = doc.page.margins.left;
    let stringDate: string = toStringDate(new Date());

    doc.font(FONT)
        .fontSize(10)
        .text(stringDate, x, y, {align: 'right'});
}

function signDocument(doc: IPDFDocument, user: IUser): void {
    let range                = (<any>doc).bufferedPageRange();
    let userName: string     = user.firstName + ' ' + user.lastName;
    let userPosition: string = 'Salgs- og projektkoordinator';// TODO system user position

    doc.switchToPage(range.count - 1);
    doc.font(BOLD_FONT)
        .text(userName, 50, 678)
        .font(FONT)
        .text(userPosition)
}

/*  TEMPLATES   */
function setBasicTemplate(doc: IPDFDocument): void {
    let x: number    = doc.page.margins.left;
    let y: number    = doc.page.margins.top;
    let range = (<any>doc).bufferedPageRange();

    // food
    y = 720;
    for (let i: number = range.start; i < range.count; i++) {
        doc.switchToPage(i);
        doc.image('./public/images/logo.png', 500, 15, {width: 100});
        doc.fontSize(9)
            .font(BOLD_FONT)
            .text('TURBINEHALLEN APS', x, y, {align: 'center'})
            .font(FONT)
            .text('Filmbyen 19 / Kalkværksvej 12', {align: 'center'})
            .text('8000 Aarhus C', {align: 'center'})
            .text("CVR-nr. 31280990    Tlf. +45 70277071    info@turbinehallen.dk    www.turbinehallen.dk",
                  {align: 'center'});

        let width: number  = doc.widthOfString('www.turbinehallen.dk');
        let height: number = doc.currentLineHeight();
        doc.underline(x + 345, y + 30, width, height, {color: 'blue'})
            .link(x + 345, y + 30, width, height, 'www.turbinehallen.dk');
    }
}

function setConditionsTemplate(doc: IPDFDocument): void {
    let x: number    = doc.page.margins.left;
    let y: number    = 500;
    let range = (<any>doc).bufferedPageRange();
    // conditions and sign
    doc.switchToPage(range.count - 1);

    let conditionsText: string = 'Ved afbestilling foretaget mellem 180 dage og 1 måned før planlagt startdato, beregner' +
        ' Turbinehallen sig et afbestillingsgebyr svarende til 40 % af den bekræftede pris. Foretages afbestillingen mindre' +
        ' end 1 måned før planlagt startdato, beregner Turbinehallen sig et afbestillingsgebyr svarende til 75 % af den' +
        ' bekræftede pris. Afbestilling af, indtil 10 % af det i ordrebekræftelsens anførte deltagerantal, kan dog ske' +
        ' omkostningsfrit indtil 5 dage før arrangementsdagen. Derefter faktureres afmeldte eller udeblevne deltagere' +
        ' til fuld pris. Ved afbestilling, som altid skal ske skriftligt, forstås annullering, reduktion, afkortning,' +
        ' omplacering og lign. væsentlige ændringer i henhold til ordrebekræftelsen. Vi gør opmærksom på, at overstående' +
        ' priser er udregnet på baggrund af det oplyste deltagerantal.';

    doc.fontSize(10)
        .font(FONT);
    doc.text('Vi håber ovennævnte stemmer overens med det aftalte. Har I spørgsmål eller ændringer til arrangementet' +
             ' er I altid velkommen til at kontakte os.', x, 450);

    doc.fontSize(8)
        .text(conditionsText, x, y, {align: 'center', lineGap: 1})
        .fontSize(10);

    y = 590;
    doc.text('Med venlig hilsen', x, y)
        .font(BOLD_FONT)
        .text('Turbinehallen ApS');
    // sign
    x = 230;
    y = 680;
    doc.moveTo(x, y)
        .lineTo(550, y)
        .stroke()
        .fontSize(10);
    y += 10;
    doc.font(BOLD_FONT)
        .text('Dato', x, y, {align: 'left'})
        .font(FONT);
}

function setChristmasConditionsTemplate(doc: IPDFDocument): void {
    let range = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.count - 1);
    doc.page.margins = {
        top: 50,
        bottom: 10,
        left: 50,
        right: 50
    };
    let x = 50;
    let y = 390;
    doc.text('Vi beder dig bekræfte arrangementet og returnere kopien i underskrevet stand inden ', x, y, {continued: true})
        .font(BOLD_FONT)
        .text('8 dage', {underline: true, continued: true})
        .font(FONT)
        .text(' samt indbetale ', {underline: false, continued: true})
        .moveDown(2)
        .text('Bemærk: Slutfaktura påføres kr. 40,- i administrationsgebyr')
        .moveDown(1)
        .text('Vi håber ovennævnte stemmer overens med det aftalte. Har I spørgsmål eller ændringer til arrangementet' +
              ' er I altid velkommen til at kontakte os.', x, 450);
    setConditionsTemplate(doc);
}

function setContactCornerTemplate(doc: IPDFDocument): void {
    let y: number    = 147;
    let range = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.start);
    let x: number = doc.page.margins.left;
    doc.font(FONT)
        .fontSize(10)
        .text('Tlf.:', x, y)
        .text('E-Mail:');
}

function getLocationConfirmationTemplate(): IPDFDocument {
    let doc: IPDFDocument = new PDFDocument({bufferPages: true});
    let y: number         = 50;
    let x: number         = 50;
    doc.page.margins      = {
        top:    y,
        bottom: 10,
        left:   x,
        right:  50
    };
    doc.font(FONT)
        .fontSize(10);

    // body
    y = 267;
    doc.font(BOLD_FONT)
        .text('LOKALEBEKRÆFTELSE', x, y, {underline: true})
        .font(FONT)
        .moveDown(1)
        .text('Ifølge aftale fremsendes hermed lokalebekræftelse vedr. jeres arrangement i Turbinehallen')
        .moveDown(1)
        .font(BOLD_FONT)
        .text('Lokale ')
        .moveDown(1)
        .text('Dato')
        .moveDown(1)
        .text('Lokaleleje')
        .font(FONT)
        .moveDown(2)
        .text('Vi beder Jer bekræfte ovenstående lokalereservation og returnere kopien i underskreven stand samt' +
              ' indbetale depositum svarende til lokalelejen på kr.');

    setBasicTemplate(doc);
    setDate(doc);
    setContactCornerTemplate(doc);
    setConditionsTemplate(doc);
    return doc;
}

function getRegularEventConfirmationTemplate(): IPDFDocument {
    let doc: IPDFDocument = new PDFDocument({bufferPages: true});
    let y: number         = 50;
    let x: number         = 50;
    doc.page.margins      = {
        top:    y,
        bottom: 10,
        left:   x,
        right:  50
    };
    doc.font(FONT)
        .fontSize(10);

    y = 267;
    doc.font(BOLD_FONT)
        .text('BEKRÆFTELSE', x, y, {underline: true})
        .moveDown(1)
        .font(FONT)
        .text('Ifølge aftale fremsender vi hermed bekræftelse vedr. jeres arrangement.')
        .font(BOLD_FONT)
        .moveDown(1)
        .text('Vi har noteret følgende:', {underline: true})
        .moveDown(1)
        .text('Dato')
        .moveDown(1)
        .text('Lokale')
        .moveDown(1)
        .text('Antal')
        .moveTo(x, y + 150)
        .lineTo(550, y + 150)
        .stroke();

    setDate(doc);
    setContactCornerTemplate(doc);
    return doc;
}

function getExternalEventConfirmationTemplate(): IPDFDocument {
    let doc: IPDFDocument = new PDFDocument({bufferPages: true});
    let y: number         = 50;
    let x: number         = 50;
    doc.page.margins      = {
        top:    y,
        bottom: 10,
        left:   x,
        right:  50
    };
    doc.font(FONT)
        .fontSize(10);

    y = 267;
    doc.font(BOLD_FONT)
        .text('BEKRÆFTELSE', x, y, {underline: true})
        .moveDown(1)
        .font(FONT)
        .text('Ifølge aftale fremsender vi hermed bekræftelse vedr. mad ud af huset fra Turbinehallen.')
        .font(BOLD_FONT)
        .moveDown(1)
        .text('Vi har noteret følgende:', {underline: true})
        .moveDown(1)
        .text('Dato')
        .moveDown(1)
        .text('Leveringsted')
        .moveDown(1)
        .text('Antal')
        .moveDown(1)
        .text('Kommentar')
        .moveTo(x, y + 200)
        .lineTo(550, y + 200)
        .stroke();

    setDate(doc);
    setContactCornerTemplate(doc);
    return doc;
}

// TODO Add Date field: DateLimitChangeParticipation for the next version
function getChristmasConfirmationTemplate(): IPDFDocument {
    let doc: IPDFDocument = new PDFDocument({bufferPages: true});
    let y: number         = 50;
    let x: number         = 50;
    doc.page.margins      = {
        top:    y,
        bottom: 10,
        left:   x,
        right:  50
    };
    doc.font(FONT)
        .fontSize(10);

    y = 267;
    doc.font(BOLD_FONT)
        .text('BEKRÆFTELSE - Fællesjulefrokost', x, y, {underline: true})
        .moveDown(1)
        .font(FONT)
        .text('Ifølge aftale fremsender vi hermed bekræftelse vedr. fællesjulefrokost i Turbinehallen.')
        .font(BOLD_FONT)
        .moveDown(1)
        .text('Dato')
        .moveDown(1)
        .text('Lokale')
        .moveDown(1)
        .text('Antal')
        .moveDown(1)
        .text('Endeligt deltagerantal bedes oplyst pr. mail senest', 100)
        .text('(Opjustering af pladser kun så længe der er ledig kapacitet)')
        .moveTo(x, y + 150)
        .lineTo(550, y + 150)
        .stroke()
        .fontSize(10)
        .font(FONT);
    setDate(doc);
    setContactCornerTemplate(doc);
    return doc;
}

/*  INFORMATION */
function setSection(doc: IPDFDocument, sectionName: string) {
    doc.font(BOLD_FONT)
        .fontSize(10)
        .moveDown(1)
        .text(sectionName, 50)
        .font(FONT)
        .moveDown(1);
}

function setRegularInformation(doc: IPDFDocument, information: IRegularEventInformation): void {
    if(!information){
        return;
    }
    let range = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.start);
    doc.page.margins = {
        top: 50,
        bottom: 80,
        left: 50,
        right: 50
    };
    doc.moveDown(1);
    if(information.program){
        setSection(doc, 'PROGRAM');
        for(let i = 0; i < information.program.length; i++) {
            let item = information.program[i];
            doc.text(item.time, {continued: true})
                .text(item.description, 100);
        }
    }
    if(information.menu){
        setSection(doc, 'MENU');
        for(let i = 0; i < information.menu.normal.length; i++){
            let item = information.menu.normal[i];
            doc.text(item.title, {continued: true})
                .text(item.description, 100);
        }
    }
    if(information.drinks){
        setSection(doc, 'DRINKS');
        doc.text(information.drinks.description);
        if(information.drinks.longDrinks){
            doc.moveDown(1)
                .text('Long Drinks: ' + information.drinks.longDrinks.join(', '));
        }
    }
    if(information.extraMaterial){
        setSection(doc, 'EXTRA MATERIAL');
        doc.text(information.extraMaterial);
    }
    if(information.entertainment){
        setSection(doc, 'ENTERTAINMENT');
        doc.text(information.entertainment);
    }
    if(information.tableSettings) {
        setSection(doc, 'TABLE SETTINGS');
        for(let i = 0; i < information.tableSettings.length; i++) {
            let item = information.tableSettings[i];
            doc.text(item.title, 50);
            for(let ii = 0; ii < item.descriptions.length; ii++){
                doc.text(item.descriptions[ii], 100);
            }
            doc.moveDown(1);
        }
    }
    if(information.tableLinen) {
        setSection(doc, 'TABLE LINEN');
        doc.text(information.tableLinen);
    }
    if(information.extension) {
        setSection(doc, 'EXTENSION');
        doc.text(information.extension);
    }
    if(information.price){
        setSection(doc, 'PRICE');
        doc.text(information.price);
    }
    if(information.payment){
        setSection(doc, 'PAYMENT');
        doc.text(information.payment);
    }
    if(doc.y > 430){
        doc.addPage();
    }
}

function setExternalInformation(doc: IPDFDocument, information: IExternalEventInformation): void {
    let range = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.start);
    doc.page.margins = {
        top: 50,
        bottom: 80,
        left: 50,
        right: 50
    };

    doc.moveDown(5);
    if (information.program) {
        setSection(doc, 'PROGRAM');
        for (let i = 0; i < information.program.length; i++) {
            let item = information.program[i];
            doc.text(item.time, {continued: true})
                .text(item.description, 100);
        }
    }
    if(information.service){
        setSection(doc, 'SERVICE');
        doc.text(information.service);
    }
    if(information.drinks){
        setSection(doc, 'DRINKS');
        doc.text(information.drinks);
    }
    if (information.tableSettings) {
        setSection(doc, 'TABLE SETTINGS');
        doc.text(information.tableSettings);
    }
    if (information.extension) {
        setSection(doc, 'EXTENSION');
        doc.text(information.extension);
    }
    if (information.pickedUp) {
        setSection(doc, 'PICKED UP');
        doc.text(information.pickedUp);
    }
    if (information.price) {
        setSection(doc, 'PRICE');
        doc.text(information.price);
    }
    if (information.payment) {
        setSection(doc, 'PAYMENT');
        doc.text(information.payment);
    }

    if (doc.y > 430) {
        doc.addPage();
    }
}

function setChristmasInformation(doc: IPDFDocument, information: IChristmasEventInformation): void {
    let range = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.start);
    doc.page.margins = {
        top: 50,
        bottom: 80,
        left: 50,
        right: 50
    };

    doc.moveDown(5);
    if (information.program) {
        setSection(doc, 'PROGRAM');
        for (let i = 0; i < information.program.length; i++) {
            let item = information.program[i];
            doc.text(item.time, {continued: true})
                .text(item.description, 100);
        }
    }
    if (information.menu) {
        setSection(doc, 'MENU');
        for (let i = 0; i < information.menu.normal.length; i++) {
            let item = information.menu.normal[i];
            doc.text(item.title, {continued: true})
                .text(item.description, 100);
        }
    }
    if (information.drinks) {
        setSection(doc, 'DRINKS');
        doc.text(information.drinks);
    }
    if (information.coffee) {
        setSection(doc, 'COFFEE');
        doc.text(information.coffee);
    }
    if (information.bar) {
        setSection(doc, 'BAR');
        doc.text(information.bar);
    }
    if (information.personalBill) {
        setSection(doc, 'PERSONAL BILL');
        doc.text(information.personalBill);
    }
    if (information.drinksOrders) {
        setSection(doc, 'DRINKS ORDERS');
        doc.text(information.drinksOrders);
    }
    if (information.specialWishes) {
        setSection(doc, 'SPECIAL WISHES');
        doc.text(information.specialWishes);
    }
    if (information.entertainment) {
        setSection(doc, 'ENTERTAINMENT');
        doc.text(information.entertainment);
    }
    if (information.wardRobe) {
        setSection(doc, 'WARD ROBE');
        doc.text(information.wardRobe);
    }
    if (information.dropoutAfterFinalBill) {
        setSection(doc, 'DROP OUT AFTER FINAL BILL');
        doc.text(information.dropoutAfterFinalBill);
    }
    if (information.price) {
        setSection(doc, 'PRICE');
        doc.text(information.price);
    }
    if (information.payment) {
        setSection(doc, 'PAYMENT');
        doc.text(information.payment);
    }

    if (doc.y > 390) {
        doc.addPage();
    }
}

/// Middleware functions
export function getChristmasConfirmation(booking: IChristmasBooking): Promise<IPDFDocument> {
    let doc: IPDFDocument = getChristmasConfirmationTemplate();
    let event: IChristmasEvent = null;// TODO how to get the event?
    //let information: IExternalEventInformation = event.information;
    let information = null;
    let user: IUser = null;
    let customer: ICustomer = <ICustomer>booking.customer;
    let contact: IContactInfo = booking.contactInfo.length > 0? booking.contactInfo[0] : null;

    let customerName: string = customer['firstName']? customer['firstName'] : customer['name'];
    let date: string = 'Aarhus, den 15. marts 2016';// creation date
    let lokale: string = 'Turbinehallen |Kalkværksvej 12 | 8000 Aarhus C.';// location address
    let dato: string = toStringDate(event.date);
    let antal         = 'Booking.numberOfParticipants persons';// TODO IBooking.numberOfParticipants
    let price: string = booking.price.toString();// booking price
    let date2: string = '28.tirsday 2016'; // TODO 8 days later than 'creation date'

    let x: number = doc.page.margins.left;
    let y: number = doc.page.margins.top;

    doc.text(lokale, {align: 'right'})
        .text(date, x+60, 315)
        .moveDown(1)
        .text(dato, x+60)
        .moveDown(1)
        .text(antal, x+60);
    setChristmasInformation(doc, information);

    let range         = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.count - 1);
    doc.font(BOLD_FONT)
        .text('depositum svarende til 50 % af arrangementet kr. ' + price + '- senest den ' + date2, 50, 402, {continued: true})
        .font(FONT)
        .text(' (se vedlagte faktura).')
        .font(BOLD_FONT)
        .text(customerName, 255, 690);

    signDocument(doc, user);
    setContact(doc, customer, null, contact);
    setChristmasConditionsTemplate(doc);
    setBasicTemplate(doc);
    doc.end();
    return Promise.resolve(doc);
}

// TODO External event interface: add 'comment' field
export function getExternalConfirmation(event: IExternalEvent): Promise<IPDFDocument> {
    let doc: IPDFDocument = getExternalEventConfirmationTemplate();
    let booking1: IExternalBooking = <IExternalBooking>event.bookings[0];
    let information: IExternalEventInformation = event.information;

    let customer1: ICustomer      = <ICustomer>booking1.customer;
    let contact: IContactInfo = event.contactInfo.length > 0 ? event.contactInfo[0] : null;
    let place: ILocation      = event.place ? <ILocation>event.place : null;

    let dato = toStringDate(event.startDate);//'Lørdag den 1. oktober 2016';
    let leveringsted = place.name + ' | ' + place.address.street + ', ' + place.address.postcode + ' ' + place.address.city;//'Train | Toldbodgade 6 | 8000 Aarhus C';
    let antal = information.numberOfParticipants + ' persons';
    let komentar = ''; // TODO Must be event.comment

    let range                 = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.start);
    doc.font(FONT)
        .fontSize(10)
        .text(dato, 120, 338)
        .moveDown(1)
        .text(leveringsted)
        .moveDown(1)
        .text(antal)
        .moveDown(1)
        .text(komentar);

    setExternalInformation(doc, information);
    signDocument(doc, <IUser>booking1.coordinator);
    setContact(doc, customer1, null, contact);
    setConditionsTemplate(doc);
    setBasicTemplate(doc);
    doc.end();
    return Promise.resolve(doc);
}

export function getRegularEventConfirmation(event: IRegularEvent): Promise<IPDFDocument> {
    let doc: IPDFDocument = getRegularEventConfirmationTemplate();
    let booking1: IRegularBooking = <IRegularBooking>event.bookings[0];
    let booking2: IRegularBooking = event.bookings.length > 1 ? <IRegularBooking>event.bookings[1] : null;
    let information: IRegularEventInformation = event.information;

    booking2 = booking2 ? <IRegularBooking>booking2 : null;

    let customer1: ICustomer      = <ICustomer>booking1.customer;
    let customer2: ICustomer  = booking2 ? <ICustomer>booking2.customer : null;
    let contact: IContactInfo = event.contactInfo.length > 0 ? event.contactInfo[0] : null;
    let place: ILocation      = event.place ? <ILocation>event.place : null;
    let user: IUser           = <IUser>booking1.coordinator;

    let lokale = place ? place.address.street + ' ' + place.address.postcode + ' ' + place.address.city : '';
    let dato = toStringDate(event.startDate);//'Lørdag den 27. august 2016'
    let antal = information.numberOfParticipants + ' persons';

    let range                 = (<any>doc).bufferedPageRange();
    doc.switchToPage(range.start);
    doc.font(FONT)
        .fontSize(10)
        .text(lokale, 120, 338)
        .moveDown(1)
        .text(dato)
        .moveDown(1)
        .text(antal)
        .moveDown(1);

    setRegularInformation(doc, information);
    setContact(doc, customer1, customer2, null);
    setConditionsTemplate(doc);
    signDocument(doc, user);
    setBasicTemplate(doc);
    doc.end();
    return Promise.resolve(doc);
}

export function getRegularLocationConfirmation(event: IRegularEvent): Promise<IPDFDocument> {
    let doc: IPDFDocument         = getLocationConfirmationTemplate();
    let booking1: IRegularBooking = <IRegularBooking>event.bookings[0];
    let booking2: IRegularBooking = event.bookings.length > 1 ? <IRegularBooking>event.bookings[1] : null;

    booking2 = booking2 ? <IRegularBooking>booking2 : null;

    let customer1: ICustomer      = <ICustomer>booking1.customer;
    let customer2: ICustomer      = booking2 ? <ICustomer>booking2.customer : null;
    let contact: IContactInfo     = event.contactInfo.length > 0 ? event.contactInfo[0] : null;
    let place: ILocation          = event.place ? <ILocation>event.place : null;
    let user: IUser               = <IUser>booking1.coordinator;
    let taxes: number             = 1.25;// Taxes implementation -> price * 1.25

    // Var's that fill the PDF
    let customer1Name: string = customer1['firstName'] ? customer1['firstName'] : customer1['name'];
    let customer2Name: string = customer2 && customer2['firstName'] ? customer2['firstName'] : customer2['name'];

    let lokale: string        = place ? place.address.street + ' ' + place.address.postcode + ' ' + place.address.city : '';// location address
    let dato: string          = event.startDate.toLocaleString();//'Lørdag den 27. august 2016';// event date TODO
    let lokaleje: string      = 'Kr. ' + 'event.locationPrice' + ',- ekskl. moms';// Location price without taxes TODO
    let price: string         = 'event.locationPrice + taxes (1.25)';//event.locationPrice + taxes (1.25);// location price + taxes TODO
    let date2: string         = toStringDate(new Date());

    let x        = doc.page.margins.left;
    let y        = doc.page.margins.top;

    doc.text(lokale, x + 60, 313)
        .moveDown(1)
        .text(dato, x + 60)
        .moveDown(1)
        .text(lokaleje, x + 60)
        .text(price + ',- inkl. moms senest den ' + date2 + ' (se vedhæftede faktura)', x + 180, 408)
        .font(BOLD_FONT)
        .text(customer1Name + customer2Name? ' & ' + customer2Name : '', 255, 690);
    setContact(doc, customer1, customer2, contact);
    signDocument(doc, user);
    doc.end();
    return Promise.resolve(doc);
}