import bcrypt = require('bcryptjs');
import uid = require('uid-safe');
import { tokenByteLength, refreshTokenByteLength, codeByteLength } from "../config/oauth2";

export function verifyPassword(password, hash): Promise<void> {
    return new Promise<void>((resolve, reject)=> {
        bcrypt.compare(password, hash, (err, res)=> {
            if (err || res == false) {
                reject(err || new Error('Incorrect credentials'));
            } else {
                resolve(null);
            }
        });
    });
}

export type TokenPair = {accessToken: string, refreshToken: string};
export function createTokenPair(): Promise<TokenPair> {
    return Promise.all([uid(tokenByteLength), uid(refreshTokenByteLength)])
        .then((tokens: string[]) => Promise.resolve({accessToken: tokens[0], refreshToken: tokens[1]}));
}

export function createAccessToken(): Promise<string> {
    return uid(tokenByteLength);
}

export function createAccessCode(): Promise<string> {
    return uid(codeByteLength);
}