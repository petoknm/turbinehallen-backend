import express = require('express');
import {checkPermissions} from "../middleware/permissions";
import { validateRegularBooking, validateChristmasBooking, validateExternalBooking } from "../validation/booking";
import { isBearerAuthenticated } from "../oauth2-authorization";
import {
    fetchBookings,

    addRegularBooking,
    updateRegularBooking,
    updateRegularBookingById,
    deleteRegularBooking,
    deleteRegularBookingById,
    fetchRegularBookings,
    getRegularBookingById,

    addChristmasBooking,
    updateChristmasBooking,
    updateChristmasBookingById,
    deleteChristmasBooking,
    deleteChristmasBookingById,
    fetchChristmasBookings,
    getChristmasBookingById,

    addExternalBooking,
    updateExternalBooking,
    updateExternalBookingById,
    deleteExternalBooking,
    deleteExternalBookingById,
    fetchExternalBookings,
    getExternalBookingById
} from "../middleware/booking";
export const bookingRouter = express.Router();

bookingRouter.get('/bookings', (req,res)=>{
   checkPermissions('peter')
       .then(() => fetchBookings())
       .then(bookings => res.json(bookings))
       .catch(err => res.sendStatus(500));
});

// === Regular Booking ===
bookingRouter.post('/regularBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateRegularBooking(req.body))
        .then(() => addRegularBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.put('/regularBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateRegularBooking(req.body))
        .then(() => updateRegularBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.put('/regularBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateRegularBooking(req.body))
        .then(() => updateRegularBookingById(req.params.id, req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.delete('/regularBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteRegularBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.delete('/regularBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteRegularBookingById(req.params.id))
        .catch(err => res.sendStatus(500));
});

bookingRouter.get('/regularBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchRegularBookings())
        .then(regularBooking => res.json(regularBooking))
        .catch(err => res.sendStatus(500));
});

bookingRouter.get('/regularBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getRegularBookingById(req.params.id))
        .then(regularBooking => res.json(regularBooking))
        .catch(err => res.sendStatus(500));
});


// === Christmas Booking ===
bookingRouter.post('/christmasBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateChristmasBooking(req.body))
        .then(() => addChristmasBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.put('/christmasBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateChristmasBooking(req.body))
        .then(() => updateChristmasBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.put('/christmasBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateChristmasBooking(req.body))
        .then(() => updateChristmasBookingById(req.params.id, req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.delete('/christmasBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteChristmasBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.delete('/christmasBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteChristmasBookingById(req.params.id))
        .catch(err => res.sendStatus(500));
});

bookingRouter.get('/christmasBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchChristmasBookings())
        .then(christmasBooking => res.json(christmasBooking))
        .catch(err => res.sendStatus(500));
});

bookingRouter.get('/christmasBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getChristmasBookingById(req.params.id))
        .then(christmasBooking => res.json(christmasBooking))
        .catch(err => res.sendStatus(500));
});

// === External Booking ===
bookingRouter.post('/externalBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalBooking(req.body))
        .then(() => addExternalBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.put('/externalBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalBooking(req.body))
        .then(() => updateExternalBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.put('/externalBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalBooking(req.body))
        .then(() => updateExternalBookingById(req.params.id, req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.delete('/externalBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteExternalBooking(req.body))
        .catch(err => res.sendStatus(500));
});

bookingRouter.delete('/externalBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteExternalBookingById(req.params.id))
        .catch(err => res.sendStatus(500));
});

bookingRouter.get('/externalBooking', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchExternalBookings())
        .then(externalBooking => res.json(externalBooking))
        .catch(err => res.sendStatus(500));
});

bookingRouter.get('/externalBooking/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getExternalBookingById(req.params.id))
        .then(externalBooking => res.json(externalBooking))
        .catch(err => res.sendStatus(500));
});