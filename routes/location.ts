import express = require('express');
import { checkPermissions } from "../middleware/permissions";
import { validateInternalLocation, validateExternalLocation } from "../validation/location";
import { isBearerAuthenticated } from "../oauth2-authorization";
import {
    fetchLocations,

    addInternalLocation,
    updateInternalLocation,
    updateInternalLocationById,
    deleteInternalLocation,
    deleteInternalLocationById,
    fetchInternalLocations,
    getInternalLocationById,

    addExternalLocation,
    updateExternalLocation,
    updateExternalLocationById,
    deleteExternalLocation,
    deleteExternalLocationById,
    fetchExternalLocations,
    getExternalLocationById
} from"../middleware/location";
export const locationRouter = express.Router();

locationRouter.get('/locations', isBearerAuthenticated, (req, res) => {
    checkPermissions('peter')
        .then(() => fetchLocations())
        .then(locations => res.json(locations))
        .catch(err => res.sendStatus(500));
});


// === Internal Location ===
locationRouter.post('/internalLocation', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateInternalLocation(req.body))
        .then(() => res.json(addInternalLocation(req.body)))
        .catch(err => res.sendStatus(500));
});

locationRouter.put('/internalLocation', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateInternalLocation(req.body))
        .then(() => updateInternalLocation(req.body))
        .catch(err => res.sendStatus(500));
});

locationRouter.put('/internalLocation/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateInternalLocation(req.body))
        .then(() => updateInternalLocationById(req.params.id, req.body))
        .catch(err => res.sendStatus(500));
});

locationRouter.delete('/internalLocation', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteInternalLocation(req.body))
        .catch(err => res.sendStatus(500));
});

locationRouter.delete('/internalLocation/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteInternalLocationById(req.params.id))
        .catch(err => res.sendStatus(500));
});

locationRouter.get('/internalLocation', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchInternalLocations())
        .then(internalLocation => res.json(internalLocation))
        .catch(err => res.sendStatus(500));
});

locationRouter.get('/internalLocation/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getInternalLocationById(req.params.id))
        .then(internalLocation => res.json(internalLocation))
        .catch(err => res.sendStatus(500));
});


// === External Location ===
locationRouter.post('/externalLocation', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalLocation(req.body))
        .then(() => res.json(addExternalLocation(req.body)))
        .catch(err => res.sendStatus(500));
});

locationRouter.put('/externalLocation', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalLocation(req.body))
        .then(() => updateExternalLocation(req.body))
        .catch(err => res.sendStatus(500));
});

locationRouter.put('/externalLocation/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalLocation(req.body))
        .then(() => updateExternalLocationById(req.params.id, req.body))
        .catch(err => res.sendStatus(500));
});

locationRouter.delete('/externalLocation', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteExternalLocation(req.body))
        .catch(err => res.sendStatus(500));
});

locationRouter.delete('/externalLocation/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteExternalLocationById(req.params.id))
        .catch(err => res.sendStatus(500));
});

locationRouter.get('/externalLocation', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchExternalLocations())
        .then(externalLocation => res.json(externalLocation))
        .catch(err => res.sendStatus(500));
});

locationRouter.get('/externalLocation/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getExternalLocationById(req.params.id))
        .then(externalLocation => res.json(externalLocation))
        .catch(err => res.sendStatus(500));
});