import express = require('express');
import { isBearerAuthenticated } from "../oauth2-authorization";
import { imageRepository } from "../models/database/repository/implementation/mongoose4/image-repository";

export const imageRouter = express.Router();

imageRouter.get('/id/:id', isBearerAuthenticated, (req, res) => {
    imageRepository.readById(req.params.id).pipe(res);
});

imageRouter.get('/name/:name', isBearerAuthenticated, (req, res) => {
    imageRepository.readByName(req.params.name).pipe(res);
});