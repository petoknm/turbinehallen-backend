import express = require('express');
import { checkPermissions } from "../middleware/permissions";
import { isBearerAuthenticated } from "../oauth2-authorization";
import {
    fetchCustomers,

    extractPrivateCustomer,
    extractBusinessCustomer,

    addPrivateCustomer,
    updatePrivateCustomer,
    updatePrivateCustomerById,
    deletePrivateCustomer,
    deletePrivateCustomerById,
    fetchPrivateCustomers,
    getPrivateCustomerById,

    addBusinessCustomer,
    updateBusinessCustomer,
    updateBusinessCustomerById,
    deleteBusinessCustomer,
    deleteBusinessCustomerById,
    fetchBusinessCustomers,
    getBusinessCustomerById,
} from "../middleware/customer";
export const customerRouter = express.Router();

customerRouter.get('/customers', isBearerAuthenticated, (req, res) => {
    checkPermissions('peter')
        .then(() => fetchCustomers())
        .then(customers => res.json(customers))
        .catch(err => res.sendStatus(500));
});


// === Private Customer ===
customerRouter.post('/privateCustomer', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => extractPrivateCustomer(req))
        .then(privateCustomer => res.json(addPrivateCustomer(privateCustomer)))
        .catch(err => res.sendStatus(500));
});

customerRouter.put('/privateCustomer', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => extractPrivateCustomer(req))
        .then(privateCustomer => updatePrivateCustomer(privateCustomer))
        .catch(err => res.sendStatus(500));
});

customerRouter.put('/privateCustomer/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => extractPrivateCustomer(req))
        .then(privateCustomer => updatePrivateCustomerById(req.params.id, privateCustomer))
        .catch(err => res.sendStatus(500));
});

customerRouter.delete('/privateCustomer', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deletePrivateCustomer(req.body))
        .catch(err => res.sendStatus(500));
});

customerRouter.delete('/privateCustomer/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deletePrivateCustomerById(req.params.id))
        .catch(err => res.sendStatus(500));
});

customerRouter.get('/privateCustomer', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchPrivateCustomers())
        .then(privateCustomer => res.json(privateCustomer))
        .catch(err => res.sendStatus(500));
});

customerRouter.get('/privateCustomer/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getPrivateCustomerById(req.params.id))
        .then(privateCustomer => res.json(privateCustomer))
        .catch(err => res.sendStatus(500));
});

// === Business Customer ===
customerRouter.post('/businessCustomer', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => extractBusinessCustomer(req))
        .then(businessCustomer => res.json(addBusinessCustomer(businessCustomer)))
        .catch(err => res.sendStatus(500));
});

customerRouter.put('/businessCustomer', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => extractBusinessCustomer(req))
        .then(businessCustomer => updateBusinessCustomer(businessCustomer))
        .catch(err => res.sendStatus(500));
});

customerRouter.put('/businessCustomer/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => extractBusinessCustomer(req))
        .then(businessCustomer => updateBusinessCustomerById(req.params.id, businessCustomer))
        .catch(err => res.sendStatus(500));
});

customerRouter.delete('/businessCustomer', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteBusinessCustomer(req.body))
        .catch(err => res.sendStatus(500));
});

customerRouter.delete('/businessCustomer/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteBusinessCustomerById(req.params.id))
        .catch(err => res.sendStatus(500));
});

customerRouter.get('/businessCustomer', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchBusinessCustomers())
        .then(businessCustomer => res.json(businessCustomer))
        .catch(err => res.sendStatus(500));
});

customerRouter.get('/businessCustomer/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getBusinessCustomerById(req.params.id))
        .then(businessCustomer => res.json(businessCustomer))
        .catch(err => res.sendStatus(500));
});