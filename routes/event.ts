import express = require('express');
import {checkPermissions} from "../middleware/permissions";
import { validateRegularEvent, validateChristmasEvent, validateExternalEvent } from "../validation/event";
import { isBearerAuthenticated } from "../oauth2-authorization";
import {
    fetchEvents,

    addRegularEvent,
    updateRegularEvent,
    updateRegularEventById,
    deleteRegularEvent,
    deleteRegularEventById,
    fetchRegularEvents,
    getRegularEventById,

    addChristmasEvent,
    updateChristmasEvent,
    updateChristmasEventById,
    deleteChristmasEvent,
    deleteChristmasEventById,
    fetchChristmasEvents,
    getChristmasEventById,

    addExternalEvent,
    updateExternalEvent,
    updateExternalEventById,
    deleteExternalEvent,
    deleteExternalEventById,
    fetchExternalEvents,
    getExternalEventById, extractRegularEvent
} from "../middleware/event";
export const eventRouter = express.Router();

eventRouter.get('/events',isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchEvents())
        .then(events => res.json(events))
        .catch(err => res.sendStatus(500));
});


// === Regular Event ===
eventRouter.post('/regularEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => extractRegularEvent(req))
        .then(regularEvent => res.json(addRegularEvent(regularEvent)))
        .catch(err => res.sendStatus(500));
});

eventRouter.put('/regularEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateRegularEvent(req.body))
        .then(() => updateRegularEvent(req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.put('/regularEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateRegularEvent(req.body))
        .then(() => updateRegularEventById(req.params.id, req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.delete('/regularEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteRegularEvent(req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.delete('/regularEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteRegularEventById(req.params.id))
        .catch(err => res.sendStatus(500));
});

eventRouter.get('/regularEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchRegularEvents())
        .then(regularEvent => res.json(regularEvent))
        .catch(err => res.sendStatus(500));
});

eventRouter.get('/regularEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getRegularEventById(req.params.id))
        .then(regularEvent => res.json(regularEvent))
        .catch(err => res.sendStatus(500));
});


// === Christmas Event ===
eventRouter.post('/christmasEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateChristmasEvent(req.body))
        .then(() => addChristmasEvent(req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.put('/christmasEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateChristmasEvent(req.body))
        .then(() => updateChristmasEvent(req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.put('/christmasEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateChristmasEvent(req.body))
        .then(() => updateChristmasEventById(req.params.id, req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.delete('/christmasEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteChristmasEvent(req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.delete('/christmasEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteChristmasEventById(req.params.id))
        .catch(err => res.sendStatus(500));
});

eventRouter.get('/christmasEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchChristmasEvents())
        .then(christmasEvent => res.json(christmasEvent))
        .catch(err => res.sendStatus(500));
});

eventRouter.get('/christmasEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getChristmasEventById(req.params.id))
        .then(christmasEvent => res.json(christmasEvent))
        .catch(err => res.sendStatus(500));
});

// === External Event ===
eventRouter.post('/externalEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalEvent(req.body))
        .then(() => addExternalEvent(req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.put('/externalEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalEvent(req.body))
        .then(() => updateExternalEvent(req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.put('/externalEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => validateExternalEvent(req.body))
        .then(() => updateExternalEventById(req.params.id, req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.delete('/externalEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteExternalEvent(req.body))
        .catch(err => res.sendStatus(500));
});

eventRouter.delete('/externalEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => deleteExternalEventById(req.params.id))
        .catch(err => res.sendStatus(500));
});

eventRouter.get('/externalEvent', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => fetchExternalEvents())
        .then(externalEvent => res.json(externalEvent))
        .catch(err => res.sendStatus(500));
});

eventRouter.get('/externalEvent/:id', isBearerAuthenticated, (req,res)=>{
    checkPermissions('peter')
        .then(() => getExternalEventById(req.params.id))
        .then(externalEvent => res.json(externalEvent))
        .catch(err => res.sendStatus(500));
});