import express = require('express');
import { isBearerAuthenticated } from "../oauth2-authorization";
import { regularEventRepository } from "../models/database/repository/implementation/mongoose4/regular-event-repository";
import { getRegularEventConfirmation } from "../middleware/pdf";
import { IPrivateCustomer } from "../models/interface/private-customer";
import { IUser } from "../models/interface/oauth2/user";
import { IRegularBookingInformation } from "../models/interface/regular-booking-information";
import { internalLocationRepository } from "../models/database/repository/implementation/mongoose4/internal-location-repository";
import { IRegularEventInformation } from "../models/interface/regular-event-information";

export const pdfRouter = express.Router();

pdfRouter.get('/:id', (req, res) => {

    let privateCustomer: IPrivateCustomer;
    let user: IUser;
    let information: IRegularBookingInformation;
    let eventInformation: IRegularEventInformation;
    let address     = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
    privateCustomer = {
        addresses:      [address],
        emailAddresses: ['puiu@puiu.com'],
        firstName:      'Leahu',
        lastName:       'Cristian',
        phoneNumbers:   ['12345678', '123456789'],
        kind:           'private customer'
    };
    user            = <IUser>{
        firstName: 'Natalie',
        lastName:  'Cerveza',
        email:     'email@gmail.com',
        password:  'pass',
        roles:     ['coordinator']
    };

    information = <IRegularBookingInformation>{
        price:   999,
        payment: 'cash'
    };

    eventInformation = <IRegularEventInformation> {
        program:                   [
            {
                time:        '22:00',
                description: 'Beginning'
            },
            {
                time:        '23:00',
                description: 'Fancy dinner'
            }],
        price:                     '1000kr',
        payment:                   'cash',
        menu:                      {
            normal:  [{
                title:       'Desert',
                description: 'Waffles'
            }],
            special: 'Wine'
        },
        drinks:                    {
            description: 'Alcohol Beverages',
            longDrinks:  ['Cuba Libre']
        },
        extraMaterial:             'Dancers',
        entertainment:             'DJ',
        tableSettings:             [{
            title:        'A1-A56',
            descriptions: ['VIPs']
        }],
        tableLinen:                'Table linen included',
        extension:                 'Extensions depending on customer\'s wishes',
        numberOfParticipants:      20,
        aproxNumberOfParticipants: 20
    };

    let regularBooking = {
        information: information,
        customer:    privateCustomer,
        payment:     'cash',
        price:       2134.2,
        coordinator: user
    };

    regularEventRepository.findById(req.params.id)
        .then(event => {
            event.bookings    = [regularBooking, regularBooking];
            event.information = eventInformation;
            return internalLocationRepository.findById(event.place.toString())
                .then(location => {
                    event.place = location;
                    return getRegularEventConfirmation(event);
                })
        })
        .then(pdf => {
            res.contentType('application/pdf');
            res.setHeader('Content-Disposition', 'attachment; filename="document.pdf"');
            res.setHeader('Content-Transfer-Encoding', 'binary');
            pdf.pipe(res);
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        });
});