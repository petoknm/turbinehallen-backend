import express = require('express');
import { isBearerAuthenticated } from "../oauth2-authorization";
import { extractUser, saveUser, removeUser } from "../middleware/user";

export const userRouter = express.Router();

userRouter.get('', isBearerAuthenticated, (req, res) => {
    res.json(req.user);
});

userRouter.post('', isBearerAuthenticated, (req, res) => {
    extractUser(req)
        .then(user => saveUser(user))
        .then(user => res.json(user))
        .catch(err => res.sendStatus(500));
});

userRouter.delete('', isBearerAuthenticated, (req, res) => {
    removeUser(req.user)
        .then(() => res.sendStatus(200))
        .catch(err => res.sendStatus(500));
});