export const tokenExpiresIn         = 3600;
export const codeByteLength         = 18;
export const tokenByteLength        = 258;
export const refreshTokenByteLength = 258;