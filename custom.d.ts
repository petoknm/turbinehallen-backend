declare module 'oauth2orize' {

    export function createServer(): {
        serializeClient(_);
        deserializeClient(_);
        grant(_);
        exchange(_);
        authorization(_): any;
        decision(): any;
        token(): any;
        errorHandler(): any;
    };

    export const grant: {
        (_);
        code: any;
    };

    export const exchange: {
        (_);
        code: any;
        refreshToken: any;
        clientCredentials: any;
        password: any;
    };

}

declare module 'passport-http' {
    export const BasicStrategy: any;
}

declare module 'passport-http-bearer' {
    export const Strategy: any;
}

declare module 'uid-safe' {
    const uid: (byteLength: number)=>Promise<string>;
    export = uid;
}

declare module 'mockgoose' {
    const mockgoose: {
        (_): any;
        reset(_): any;
    };

    export = mockgoose;
}