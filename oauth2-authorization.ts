import oauth2orize = require('oauth2orize');
import passport = require('passport');
import passportHttp = require('passport-http');
import passportHttpBearer = require('passport-http-bearer');
import { tokenRepository } from "./models/database/repository/implementation/mongoose4/oauth2/token-repository";
import { userRepository } from "./models/database/repository/implementation/mongoose4/oauth2/user-repository";
import { clientRepository } from "./models/database/repository/implementation/mongoose4/oauth2/client-repository";
import { oauthServer } from "./oauth2-server";
import { verifyPassword } from "./middleware/authentication";
import { Response, Request } from "express-serve-static-core";

const BasicStrategy  = passportHttp.BasicStrategy;
const BearerStrategy = passportHttpBearer.Strategy;

passport.use(new BasicStrategy((username, password, callback) => {
        let user;
        userRepository.findByEmail(username)
            .then(_user => {
                user = _user;
                return verifyPassword(password, user.password)
            })
            .then(valid => {
                if (valid) {
                    callback(null, user);
                } else {
                    callback(null, false);
                }
            })
            .catch(err => callback(err));
    }
));

export const isAuthenticated = passport.authenticate('basic', {session: false});

passport.use('client-basic', new BasicStrategy((username, password, callback) => {
        clientRepository.findById(username)
            .then(client => {
                if (client.secret === password) {
                    callback(null, client);
                } else {
                    callback(null, false);
                }
            })
            .catch(err => callback(err));
    }
));

export const isClientAuthenticated = passport.authenticate('client-basic', {session: false});

passport.use(new BearerStrategy((accessToken: string, callback) => {
        tokenRepository.findByValue(accessToken)
            .then(token => {
                if (Date.now() >= token.expiresIn) return Promise.reject('Token has expired');
                return userRepository.findById(token.user);
            })
            .then(user => callback(null, user, {scope: '*'}))
            .catch(err => callback(err));
    }
));

export const isBearerAuthenticated = passport.authenticate('bearer', {session: false});

export const authorization = [
    isAuthenticated,
    oauthServer.authorization((clientID, redirectURI, done) => {
        clientRepository.findById(clientID)
            .then(client => {
                if (client.redirectUri === redirectURI) {
                    done(null, client, redirectURI)
                } else {
                    return Promise.reject('Invalid redirectURI');
                }
            })
            .catch(err => done(err));
    }),
    (req: Request | any, res: Response) => {
        res.json({transactionID: req.oauth2.transactionID, user: req.user, client: req.oauth2.client});
    }
];

export const decision = [
    isAuthenticated,
    oauthServer.decision()
];

export const token = [
    isClientAuthenticated,
    oauthServer.token(),
    oauthServer.errorHandler()
];