import express = require('express');
import cors = require('cors');
import mongoose = require('mongoose');
import bodyParser = require('body-parser');
import session = require('express-session');
import helmet = require('helmet');
import { port, version } from "./config/config";
import { customerRouter } from "./routes/customer";
import { locationRouter } from "./routes/location";
import { uri } from "./config/database";
import { authorization, decision, token } from "./oauth2-authorization";
import { userRouter } from "./routes/user";
import { imageRouter } from "./routes/image";
import { eventRouter } from "./routes/event";
import { bookingRouter } from "./routes/booking";
import { pdfRouter } from "./routes/pdf";

mongoose.Promise = Promise;
const options    = {promiseLibrary: Promise};
mongoose.connect(uri, options);

const app = express();

app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({secret: 'h4idWX-:E[f7hmCa/mgF[r', resave: false, saveUninitialized: false}));

app.use('/customer', customerRouter);
app.use('/location', locationRouter);
app.use('/event',eventRouter);
app.use('/booking',bookingRouter);

app.use('/user', userRouter);
app.use('/image', imageRouter);
app.use('/pdf', pdfRouter);

app.get('/version', (req, res) => {
    res.send(version);
});

app.get('/authorize', ...authorization);
app.post('/authorize/decision', ...decision);
app.post('/authorize/token', ...token);

app.listen(port);