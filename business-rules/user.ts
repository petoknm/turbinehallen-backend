export const userFirstNameRegex = /^[A-Z][a-z]+$/;
export const userLastNameRegex  = userFirstNameRegex;
export const userPasswordRegex  = /^[A-Za-z0-9]{8,24}$/;