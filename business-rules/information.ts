export const informationNumberOfParticipantsRegex = /^[0-9]+$/;
export const informationAproxNumberOfParticipantsRegex = informationNumberOfParticipantsRegex;
export const informationPriceRegex = /(\+)?([0-9]+(\.[0-9]+))/;