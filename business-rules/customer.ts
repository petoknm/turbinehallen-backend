export const customerFirstNameRegex   = /^[A-Z][a-z]+$/;
export const customerLastNameRegex    = customerFirstNameRegex;
export const customerPhoneNumberRegex = /^\d{8,12}$/;

export const businessCustomerCvrRegex = /^\d{10}$/;
