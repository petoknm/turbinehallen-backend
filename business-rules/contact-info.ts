export const contactInfoFirstNameRegex   = /^[A-Z][a-z]+$/;
export const contactInfoLastNameRegex    = contactInfoFirstNameRegex;
export const contactInfoPhoneNumberRegex = /^\d{8,12}$/;

