export const locationNameRegex = /^[a-zA-Z\s]*$/;
export const latitudeRegex     = /^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}/;
export const longitudeRegex    = latitudeRegex;
export const capacityRegex     = /^\d{1,10}$/;
export const defaultPriceRegex = /(\+)?([0-9]+(\.[0-9]+))/;