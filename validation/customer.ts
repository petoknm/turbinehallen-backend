import { ICustomer } from "../models/interface/customer";
import { IPrivateCustomer } from "../models/interface/private-customer";
import {
    customerFirstNameRegex, customerLastNameRegex, customerPhoneNumberRegex,
    businessCustomerCvrRegex
} from "../business-rules/customer";
import { emailAddressRegex } from "../business-rules/email";
import { IBusinessCustomer } from "../models/interface/business-customer";


export function validatePrivateCustomer(privateCustomer: IPrivateCustomer): boolean {
    if (!customerFirstNameRegex.test(privateCustomer.firstName))
        return false;

    if (!customerLastNameRegex.test(privateCustomer.lastName))
        return false;
    for (let phoneNumber of privateCustomer.phoneNumbers) {
        if (!customerPhoneNumberRegex.test(phoneNumber))
            return false;
    }

    //TODO address regex?

    for (let emailAddress of privateCustomer.emailAddresses) {
        //TODO change the type from String to string ?
        if (!emailAddressRegex.test(emailAddress.toString()))
            return false;
    }

    return true;
}

export function validateBusinessCustomer(businessCustomer: IBusinessCustomer): boolean {
    if (!customerFirstNameRegex.test(businessCustomer.name.toString()))
        return false;

    if (!businessCustomerCvrRegex.test(businessCustomer.cvr.toString())) {
        return false;
    }

    //TODO address regex?

    for (let emailAddress of businessCustomer.emailAddresses) {
        //TODO change the type from String to string ?
        if (!emailAddressRegex.test(emailAddress.toString()))
            return false;
    }

    return true;
}