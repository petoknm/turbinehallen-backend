import { IRegularEvent } from "../models/interface/regular-event";
import { IExternalEvent } from "../models/interface/external-event";
import { IChristmasEvent } from "../models/interface/christmas-event";
import { isDate } from "util";
import { isNumber } from "util";
import { validateContactInfo } from "./contact-info";
import { IInternalLocation } from "../models/interface/internal-location";
import { validateInternalLocation, validateExternalLocation } from "./location";
import { IExternalLocation } from "../models/interface/external-location";
import { isString } from "util";
import { IRegularBooking } from "../models/interface/regular-booking";
import { IExternalBooking } from "../models/interface/external-booking";
import { validateRegularBooking, validateExternalBooking, validateChristmasBooking } from "./booking";
import { IChristmasBooking } from "../models/interface/christmas-booking";
import { isUndefined } from "util";
import {
    validateRegularEventInformation, validateExternalEventInformation,
    validateChristmasEventInformation
} from "./information";
import { ILocation } from "../models/interface/location";
import { IBooking } from "../models/interface/booking";

/**
 * Validates the base IEvent fields
 * @param event
 * @returns {boolean}
 */
function validateEvent(event:any):boolean {
    if (!isDate(event.startDate))
        return false;

    if (!isDate(event.endDate))
        return false;

    if (event.startDate > event.endDate)
        return false;

    for (let contactInfo of event.contactInfo) {
        if (validateContactInfo(contactInfo) === false)
            return false;
    }
    if(!event.information) {
        return false;
    }

    return true;
}

export function validateEventFromJson(event):boolean{

    if (!isDate(event.startDate))
        return false;

    if (!isDate(event.endDate))
        return false;

    if (event.startDate > event.endDate)
        return false;

    for (let contactInfo of event.contactInfo) {
        if (validateContactInfo(contactInfo) === false)
            return false;
    }

    return true;
}

export function validateRegularEventFromJson(regularEvent:IRegularEvent):boolean {

    if (validateEventFromJson(regularEvent) === false)
        return false;

    switch ((<ILocation>regularEvent.place).kind) {
        case 'internal location':
            if (validateInternalLocation(<IInternalLocation>regularEvent.place) === false)
                return false;
            break;
        default:
            if (validateInternalLocation(<IInternalLocation>regularEvent.place) === false)
                return false;
            break;
    }

    if (!isString(regularEvent.description))
        return false;

    if (!isString(regularEvent.name))
        return false;

    if (!isString(regularEvent.type))
        return false;

    if (!isNumber(regularEvent.totalPrice) || regularEvent.totalPrice < 0)
        return false;

    return true;
}

export function validateRegularEvent(regularEvent:IRegularEvent):boolean {

    if (validateEvent(regularEvent) === false)
        return false;

    switch ((<ILocation>regularEvent.place).kind) {
        case 'internal location':
            if (validateInternalLocation(<IInternalLocation>regularEvent.place) === false)
                return false;
            break;
        default:
            if (validateInternalLocation(<IInternalLocation>regularEvent.place) === false)
                return false;
            break;
    }

    if (!isString(regularEvent.description))
        return false;

    for (let booking of regularEvent.bookings) {
        if (validateRegularBooking(<IRegularBooking>booking) === false)
            return false;
    }

    if(!regularEvent.information || !validateRegularEventInformation(regularEvent.information)){
        return false;
    }

    if (!isString(regularEvent.name))
        return false;

    if (!isString(regularEvent.type))
        return false;

    if (!isNumber(regularEvent.totalPrice) || regularEvent.totalPrice < 0)
        return false;

    return true;
}

export function validateExternalEvent(externalEvent:IExternalEvent):boolean {
    if (validateEvent(externalEvent) === false)
        return false;

    switch ((<ILocation>externalEvent.place).kind) {
        case 'external location':
            if (validateExternalLocation(<IExternalLocation>externalEvent.place) === false)
                return false;
            break;
        default:
            return false;
    }

    if(!externalEvent.information || !validateExternalEventInformation(externalEvent.information)){
        return false;
    }

    if (!isString(externalEvent.description))
        return false;

    for (let booking of externalEvent.bookings) {
        if (validateExternalBooking(<IBooking>booking) === false)
            return false;
    }

    if (!isString(externalEvent.name))
        return false;

    if (!isString(externalEvent.type))
        return false;

    if (!isNumber(externalEvent.totalPrice) || externalEvent.totalPrice < 0)
        return false;

    return true;
}

export function validateChristmasEvent(christmasEvent:IChristmasEvent):boolean {
    for (let booking of christmasEvent.bookings) {
        if (validateExternalBooking(<IBooking>booking) === false)
            return false;
    }

    if(!christmasEvent.information || !validateChristmasEventInformation(christmasEvent.information)){
        return false;
    }

    switch ((<ILocation>christmasEvent.place).kind) {
        case 'internal location':
            if (validateInternalLocation(<IInternalLocation>christmasEvent.place) === false)
                return false;
            break;
        case 'external location':
            if (validateExternalLocation(<IExternalLocation>christmasEvent.place) === false)
                return false;
            break;
        default:
            return false;
    }


    if (!isDate(christmasEvent.date))
        return false;

    if (!isNumber(christmasEvent.pricePerSeat) || christmasEvent.pricePerSeat < 0)
        return false;

    return true;
}

