import { IUser } from "../models/interface/oauth2/user";
import { userFirstNameRegex, userLastNameRegex, userPasswordRegex } from "../business-rules/user";
import { emailAddressRegex } from "../business-rules/email";

export function validateUser(user: IUser): boolean {

    if (!userFirstNameRegex.test(user.firstName))
        return false;
    if (!userLastNameRegex.test(user.lastName))
        return false;
    if (!emailAddressRegex.test(user.email))
        return false;
    if (!userPasswordRegex.test(user.password))
        return false;

    return true;

}