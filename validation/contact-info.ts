import { IContactInfo } from "../models/interface/contact-info";
import {
    contactInfoFirstNameRegex, contactInfoLastNameRegex,
    contactInfoPhoneNumberRegex
} from "../business-rules/contact-info";
import { emailAddressRegex } from "../business-rules/email";

export function validateContactInfo(contactInfo:IContactInfo){
    if(!contactInfoFirstNameRegex.test(contactInfo.firstName))
        return false;

    if(!contactInfoLastNameRegex.test(contactInfo.lastName))
        return false;

    for(let phoneNumber of contactInfo.phoneNumbers) {
        if (!contactInfoPhoneNumberRegex.test(phoneNumber))
            return false;
    }

    for(let emailAddress of contactInfo.emailAddresses){
        if(!emailAddressRegex.test(emailAddress))
            return false;
    }

    return true;
}