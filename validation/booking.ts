import { IRegularBooking } from "../models/interface/regular-booking";
import { IExternalBooking } from "../models/interface/external-booking";
import { IChristmasBooking } from "../models/interface/christmas-booking";

import {
    validateChristmasBookingInformation,
    validateExternalBookingInformation,
    validateRegularBookingInformation
} from"./information";
import { validatePrivateCustomer, validateBusinessCustomer } from "./customer";
import { isNumber } from "util";
import { validateContactInfo } from "./contact-info";

/**
 * Validates the IBooking
 * @param booking
 */
function validateBooking(booking: any): boolean {

    switch (booking.customer.kind) {
        case "private customer":
            if (validatePrivateCustomer(booking.customer) === false)
                return false;
            break;
        case "business customer":
            if (validateBusinessCustomer(booking.customer) === false)
                return false;
            break;
    }

    //TODO validateCoordinator

    if (!isNumber(booking.price) || booking.price < 0)
        return false;

    return true;

}

export function validateRegularBooking(regularBooking: IRegularBooking): boolean {
    if(!validateBooking(regularBooking)){
        return false;
    }
    if(!regularBooking.information || !validateRegularBookingInformation(regularBooking.information)){
        return false;
    }
    return true;
}

export function validateExternalBooking(externalBooking: IExternalBooking): boolean {
    if(!validateBooking(externalBooking)){
        return false;
    }
    if(externalBooking.information){
        return validateExternalBookingInformation(externalBooking.information);
    }
    return true;
}

export function validateChristmasBooking(christmasBooking: IChristmasBooking): boolean {
    if (validateBooking(christmasBooking) === false)
        return false;

    for (let contactInfo of christmasBooking.contactInfo) {
        if (validateContactInfo(contactInfo) === false)
            return false;
    }

    if(!christmasBooking.information || !validateChristmasBookingInformation(christmasBooking.information)){
        return false;
    }

    return true;
}