import { informationAproxNumberOfParticipantsRegex, informationNumberOfParticipantsRegex, informationPriceRegex } from "../business-rules/information";
import { IRegularBookingInformation } from "../models/interface/regular-booking-information";
import { IChristmasEventInformation } from "../models/interface/christmas-event-information";
import { IChristmasBookingInformation } from "../models/interface/christmas-booking-information";
import { IExternalEventInformation } from "../models/interface/external-event-information";
import { IExternalBookingInformation } from "../models/interface/external-booking-information";
import { IRegularEventInformation } from "../models/interface/regular-event-information";
import { isNumber } from "util";

export function validateRegularBookingInformation(information: IRegularBookingInformation): boolean {
    return isNumber(information.price);
}

export function validateRegularEventInformation(information: IRegularEventInformation): boolean {
    if (!informationNumberOfParticipantsRegex.test(information.numberOfParticipants.toString()))
        return false;
    return informationAproxNumberOfParticipantsRegex.test(information.aproxNumberOfParticipants.toString());
}

export function validateExternalBookingInformation(information: IExternalBookingInformation): boolean {
    return true;
}

export function validateExternalEventInformation(information: IExternalEventInformation): boolean {
    return informationNumberOfParticipantsRegex.test(information.numberOfParticipants.toString());
}

export function validateChristmasBookingInformation(information: IChristmasBookingInformation): boolean {
    if (!informationNumberOfParticipantsRegex.test(information.numberOfParticipants.toString()))
        return false;
    return isNumber(information.price);
}

export function validateChristmasEventInformation(information: IChristmasEventInformation): boolean {
    return true;
}