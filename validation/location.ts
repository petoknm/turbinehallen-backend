import { IExternalLocation } from "../models/interface/external-location";
import { IInternalLocation } from "../models/interface/internal-location";
import {
    locationNameRegex, latitudeRegex, longitudeRegex, capacityRegex,
    defaultPriceRegex
} from "../business-rules/location";
export function validateExternalLocation(externalLocation: IExternalLocation) {

    if (!locationNameRegex.test(externalLocation.name.toString()))
        return false;

    if (!latitudeRegex.test(externalLocation.latitude.toString()))
        return false;

    if (!longitudeRegex.test(externalLocation.longitude.toString()))
        return false;

    //description and address can not be tested.
    return true;
}

export function validateInternalLocation(internalLocation: IInternalLocation) {
    if (!locationNameRegex.test(internalLocation.name.toString()))
        return false;

    if (!capacityRegex.test(internalLocation.capacity.toString()))
        return false;

    if (!latitudeRegex.test(internalLocation.latitude.toString()))
        return false;


    if (!longitudeRegex.test(internalLocation.longitude.toString()))
        return false;


    if (internalLocation.defaultPrice < 0 )
        return false;

    return true;
}