import chai = require('chai');
import { IBooking } from "../../models/interface/booking";
import { IRegularBooking } from "../../models/interface/regular-booking";
import { IPrivateCustomer } from "../../models/interface/private-customer";
import { validateRegularBooking, validateExternalBooking, validateChristmasBooking } from "../../validation/booking";
import { IExternalBooking } from "../../models/interface/external-booking";
import { IChristmasBooking } from "../../models/interface/christmas-booking";
import { IContactInfo } from "../../models/interface/contact-info";
import { IUser } from "../../models/interface/oauth2/user";
import { IRegularBookingInformation } from "../../models/interface/regular-booking-information";
import { IExternalBookingInformation } from "../../models/interface/external-booking-information";
import { IChristmasBookingInformation } from "../../models/interface/christmas-booking-information";
const expect = chai.expect;

describe('Regular booking validation', ()=> {
    let regularBooking:IRegularBooking;

    beforeEach(()=> {
        let privateCustomer:IPrivateCustomer;
        let user: IUser;
        let information: IRegularBookingInformation;
        let address     = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        privateCustomer = {
            addresses:      [address],
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'Leahu',
            lastName:       'Cristian',
            phoneNumbers:   ['12345678', '123456789'],
            kind:           'private customer'
        };
        user = <IUser>{
            firstName: 'first name',
            lastName: 'last name',
            email: 'emailkk@gmail.com',
            password: 'pass',
            roles: ['coordinator']
        };

        information = <IRegularBookingInformation>{
            price: 999,
            payment: ''
        };

        regularBooking = {
            information: information,
            customer:    privateCustomer,
            payment:     'cash',
            price:       2134.2,
            coordinator: user
        }
    });

    it('should pass the regular booking creation validation with private customer', ()=> {
        expect(validateRegularBooking(regularBooking)).to.be.true;
    });

    it('should pass the regular booking creation validation with private customer', ()=> {
        let address             = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        let businessCustomer    = {
            name:           'Uwe',
            cvr:            '1234567890',
            addresses:      [address],
            emailAddresses: ['normal_business_man@nato.com'],
            kind:           'business customer'
        };
        regularBooking.customer = businessCustomer;
        expect(validateRegularBooking(regularBooking)).to.be.true;
    });

    it('should not pass the regular booking creation validation when price is smalled than 0', ()=> {
        regularBooking.price = -23;
        expect(validateRegularBooking(regularBooking)).to.be.false;
    });
});

describe('External booking validation', ()=> {
    let externalBooking:IExternalBooking;

    beforeEach(()=> {
        let privateCustomer:IPrivateCustomer;
        let user: IUser;
        let information: IExternalBookingInformation;
        let address     = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        privateCustomer = {
            addresses:      [address],
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'Leahu',
            lastName:       'Cristian',
            phoneNumbers:   ['12345678', '123456789'],
            kind:           'private customer'
        };

        user = <IUser>{
            firstName: 'first name',
            lastName: 'last name',
            email: 'emailkk@gmail.com',
            password: 'pass',
            roles: ['coordinator']
        };

        information = <IExternalBookingInformation>{};

        externalBooking = {
            information: information,
            customer:    privateCustomer,
            payment:     'cash',
            price:       2134.2,
            coordinator: user
        }
    });

    it('should pass the external booking creation validation with private customer', ()=> {
        expect(validateExternalBooking(externalBooking)).to.be.true;
    });

    it('should pass the external booking creation validation with private customer', ()=> {
        let address              = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        let businessCustomer     = {
            name:           'Uwe',
            cvr:            '1234567890',
            addresses:      [address],
            emailAddresses: ['normal_business_man@nato.com'],
            kind:           'business customer'
        };
        externalBooking.customer = businessCustomer;
        expect(validateExternalBooking(externalBooking)).to.be.true;
    });

    it('should not pass the external booking creation validation when price is smalled than 0', ()=> {
        externalBooking.price = -23;
        expect(validateExternalBooking(externalBooking)).to.be.false;
    });
});

describe('Christmas booking validation', ()=> {
    let christmasBooking:IChristmasBooking;

    beforeEach(()=> {
        let privateCustomer:IPrivateCustomer;
        let user: IUser;
        let information: IChristmasBookingInformation;
        let address     = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        privateCustomer = {
            addresses:      [address],
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'Leahu',
            lastName:       'Cristian',
            phoneNumbers:   ['12345678', '123456789'],
            kind:           'private customer'
        };

        let contactInfo:IContactInfo;
        contactInfo      = {
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'John',
            lastName:       'Will',
            phoneNumbers:   ['50366711', '50367711']
        };
        user = <IUser>{
            firstName: 'first name',
            lastName: 'last name',
            email: 'emailkk@gmail.com',
            password: 'pass',
            roles: ['coordinator']
        };
        information = <IChristmasBookingInformation>{
            numberOfParticipants: 9,
            price: 333
        };
        christmasBooking = {
            information: information,
            customer:    privateCustomer,
            payment:     'cash',
            price:       2134.2,
            coordinator: user,
            contactInfo:[contactInfo]
        }
    });

    it('should pass the christmas booking creation validation with private customer', ()=> {
        expect(validateChristmasBooking(christmasBooking)).to.be.true;
    });

    it('should pass the christmas booking creation validation with private customer', ()=> {
        let address               = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        let businessCustomer      = {
            name:           'Uwe',
            cvr:            '1234567890',
            addresses:      [address],
            emailAddresses: ['normal_business_man@nato.com'],
            kind:           'business customer'
        };
        christmasBooking.customer = businessCustomer;
        expect(validateChristmasBooking(christmasBooking)).to.be.true;
    });

    it('should not pass the christmas booking creation validation when price is smalled than 0', ()=> {
        christmasBooking.price = -23;
        expect(validateChristmasBooking(christmasBooking)).to.be.false;
    });
});
