import chai = require('chai');
import { validateUser } from "../../validation/user";
const expect = chai.expect;

describe('User validation', () => {
    // TODO more thorough testing needed

    it('should be valid with a valid user', () => {
        expect(validateUser(
            {
                firstName: 'Mikael',
                lastName:  'Dogeson',
                email:     'mikey@doge.com',
                password:  'ihatecate',
                roles:     []
            })).to.be.true;
    });

    it('should fail with a numeric character in the first name', () => {
        expect(validateUser(
            {
                firstName: 'Mik4el',
                lastName:  'Dogeson',
                email:     'mikey@doge.com',
                password:  'ihatecate',
                roles:     []
            })).to.be.false;
    });

    it('should fail with a numeric character in the last name', () => {
        expect(validateUser(
            {
                firstName: 'Mikael',
                lastName:  'Doges0n',
                email:     'mikey@doge.com',
                password:  'ihatecate',
                roles:     []
            })).to.be.false;
    });

    it('should fail with a password that is too short', () => {
        expect(validateUser(
            {
                firstName: 'Mikael',
                lastName:  'Dogeson',
                email:     'mikey@doge.com',
                password:  'ihateca',
                roles:     []
            })).to.be.false;
    });

    it('should fail with a password that is too long', () => {
        expect(validateUser(
            {
                firstName: 'Mikael',
                lastName:  'Dogeson',
                email:     'mikey@doge.com',
                password:  'ihatecateihatecateihateca',
                roles:     []
            })).to.be.false;
    });

    it('should fail with a bcrypt hash as a password', () => {
        expect(validateUser(
            {
                firstName: 'Mikael',
                lastName:  'Dogeson',
                email:     'mikey@doge.com',
                password:  '$2a$08$w.m4ezlJglJG852kWwL9ZumFGZdS.ciBAkC5P3QK8CiSRPI0kOaAW',
                roles:     []
            })).to.be.false;
    });

});