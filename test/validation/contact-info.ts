import chai = require('chai');
import { validateContactInfo } from "../../validation/contact-info";
import { IContactInfo } from "../../models/interface/contact-info";
const expect = chai.expect;

describe('Contact Info creation', ()=> {
    let contactInfo:IContactInfo;
    beforeEach(() => {
        contactInfo = {
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'John',
            lastName:       'Will',
            phoneNumbers:   ['50366711', '50367711']
        }
    });

    it('should pass the creation validation', () => {
        expect(validateContactInfo(contactInfo)).to.be.true;
    });

    it('should not pass the creation validation due to 2 names for firstname',() =>{
        contactInfo.firstName='John Doe';
       expect(validateContactInfo(contactInfo)).to.be.false;
    });

    it('should not pass the creation validation due to 2 names for lastname',() =>{
        contactInfo.lastName='Will Smith';
        expect(validateContactInfo(contactInfo)).to.be.false;
    });

    it('should not pass the validation on the e-mail due to .test domain', () => {
        contactInfo.emailAddresses.push('test@test.test');
        expect(validateContactInfo(contactInfo)).to.be.false;
    });

    it('should not pass the validation on the phone numbers not having right size', () => {
        contactInfo.phoneNumbers.push('1234567');
        expect(validateContactInfo(contactInfo)).to.be.false;
    });
});