import chai = require('chai');
import { IRegularEvent } from "../../models/interface/regular-event";
import { IPrivateCustomer } from "../../models/interface/private-customer";
import { IRegularBooking } from "../../models/interface/regular-booking";
//import { IInformation } from "../../models/interface/information";
import { IContactInfo } from "../../models/interface/contact-info";
import { IAddress } from "../../models/interface/address";
import { validateRegularEvent, validateExternalEvent, validateChristmasEvent } from "../../validation/event";
import { IInternalLocation } from "../../models/interface/internal-location";
import { IExternalEvent } from "../../models/interface/external-event";
import { IExternalBooking } from "../../models/interface/external-booking";
import { IChristmasEvent } from "../../models/interface/christmas-event";
import { IExternalLocation } from "../../models/interface/external-location";
import { IRegularBookingInformation } from "../../models/interface/regular-booking-information";
import { IRegularEventInformation } from "../../models/interface/regular-event-information";
import { IExternalEventInformation } from "../../models/interface/external-event-information";
import { IExternalBookingInformation } from "../../models/interface/external-booking-information";
import { IChristmasEventInformation } from "../../models/interface/christmas-event-information";
import { IChristmasBookingInformation } from "../../models/interface/christmas-booking-information";
const expect = chai.expect;

describe('Regular event validation', () => {
    let regularEvent: IRegularEvent;
    beforeEach(() => {
        let regularBooking: IRegularBooking;
        let privateCustomer: IPrivateCustomer;
        let bookingInformation: IRegularBookingInformation;
        let eventInformation: IRegularEventInformation;
        let address     = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        privateCustomer = {
            addresses:      [address],
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'Leahu',
            lastName:       'Cristian',
            phoneNumbers:   ['12345678', '123456789'],
            kind:           'private customer'
        };

        bookingInformation = <IRegularBookingInformation>{
            price: 33,
            payment: ''
        };
        eventInformation = <IRegularEventInformation>{
            program: [{
                         time: '',
                         description: ''
                     }],
            price: '',
            payment: '',
            menu: {
                normal: [{
                            title: '',
                            description: ''
                        }],
                special: ''
            },
            drinks: {
                description: '',
                longDrinks: ['']
            },
            extraMaterial: '',
            entertainment: '',
            tableSettings: [{
                      title: '',
                      descriptions: ['']
                  }],
            tableLinen: '',
            extension: '',
            numberOfParticipants: 10,
            aproxNumberOfParticipants: 10
        };

        regularBooking = {
            information: bookingInformation,
            customer:    privateCustomer,
            payment:     'cash',
            price:       2134.2,
            coordinator: null
        };

        let contactInfo: IContactInfo;
        contactInfo = {
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'John',
            lastName:       'Will',
            phoneNumbers:   ['50366711', '50367711']
        };

        let internalLocation: IInternalLocation;
        address          = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        internalLocation = {
            name:         "Aarhus hall",
            latitude:     55.233,
            longitude:    -10.24,
            address:      address,
            description:  "Just a simple description",
            capacity:     120,
            imagePaths:   [''],
            defaultPrice: 531,
            kind:         'internal location'
        };
        let startDate    = new Date('1968-11-16T00:00:00');
        let endDate      = new Date('1968-11-16T00:00:00');

        regularEvent = {
            bookings:      [regularBooking],
            information: eventInformation,
            contactInfo:   [contactInfo],
            description:   'Test description',
            startDate:     startDate,
            endDate:       endDate,
            name:          'test',
            place:         internalLocation,
            totalPrice:    4343.11,
            type:          'test',
            getTotalPrice: () => 323
        }
    });

    it('should pass the regular event validation', () => {
        expect(validateRegularEvent(regularEvent)).to.be.true;
    });

    // it('should not pass the regular event validation with an external location', () => {
    //     let address: IAddress;
    //     address                                 = {
    //         city:     'Aarhus',
    //         street:   'Xvej',
    //         postcode: '8000',
    //         country:  'Denmark'
    //     };
    //     let externalLocation: IExternalLocation = {
    //         name:        "Aarhus hall",
    //         latitude:    55.233,
    //         longitude:   -10.24,
    //         address:     address,
    //         description: "Just a simple description",
    //         kind:        'external location'
    //     };
    //     regularEvent.place                      = externalLocation;
    //     expect(validateRegularEvent(regularEvent)).to.be.false;
    // });

    it('should not pass the regular event validation on start date after the end date', () => {
        let startDate          = new Date('1969-11-16T00:00:00');
        let endDate            = new Date('1968-11-16T00:00:00');
        regularEvent.startDate = startDate;
        regularEvent.endDate   = endDate;
        expect(validateRegularEvent(regularEvent)).to.be.false;
    });

    it('should not pass the reuglar event validation on total price smaller than 0', () => {
        let dummyPrice          = -28299.4;
        regularEvent.totalPrice = dummyPrice;
        expect(validateRegularEvent(regularEvent)).to.be.false;
    });

});

describe('External event validation', () => {
    let externalEvent: IExternalEvent;
    beforeEach(() => {
        let regularBooking: IExternalBooking;
        let privateCustomer: IPrivateCustomer;
        let bookingInformation: IExternalBookingInformation;
        let eventInformation: IExternalEventInformation;
        let address     = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        privateCustomer = {
            addresses:      [address],
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'Leahu',
            lastName:       'Cristian',
            phoneNumbers:   ['12345678', '123456789'],
            kind:           'private customer'
        };

        bookingInformation = <IExternalBookingInformation>{
        };
        eventInformation = <IExternalEventInformation>{
            program: [{
                time: '',
                description: ''
            }],
            price: '',
            payment: '',
            service: '',
            drinks: '',
            tableSettings: '',
            extension: '',
            pickedUp: '',
            numberOfParticipants: 10
        };

        regularBooking = {
            information: bookingInformation,
            customer:    privateCustomer,
            payment:     'cash',
            price:       2134.2,
            coordinator: null
        };

        let contactInfo: IContactInfo;
        contactInfo = {
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'John',
            lastName:       'Will',
            phoneNumbers:   ['50366711', '50367711']
        };

        address                                 = {
            city:     'Aarhus',
            street:   'Xvej',
            postcode: '8000',
            country:  'Denmark'
        };
        let externalLocation: IExternalLocation = {
            name:        "Aarhus hall",
            latitude:    55.233,
            longitude:   -10.24,
            address:     address,
            description: "Just a simple description",
            kind:        'external location'
        };
        let startDate                           = new Date('1968-11-16T00:00:00');
        let endDate                             = new Date('1968-11-16T00:00:00');

        externalEvent = {
            bookings:      [regularBooking],
            information: eventInformation,
            contactInfo:   [contactInfo],
            description:   'Test description',
            startDate:     startDate,
            endDate:       endDate,
            name:          'test',
            place:         externalLocation,
            totalPrice:    4343.11,
            type:          'test',
            getTotalPrice: () => 323
        }
    });

    it('should pass the external event validation', () => {
        expect(validateExternalEvent(externalEvent)).to.be.true;
    });

    it('should not pass the external event validation with an internal location', () => {
        let address: IAddress;
        address                                 = {
            city:     'Aarhus',
            street:   'Xvej',
            postcode: '8000',
            country:  'Denmark'
        };
        let internalLocation: IInternalLocation = {
            name:         "Aarhus hall",
            latitude:     55.233,
            longitude:    -10.24,
            address:      address,
            description:  "Just a simple description",
            capacity:     120,
            imagePaths:   [''],
            defaultPrice: 531,
            kind:         'internal location'
        };
        externalEvent.place                     = internalLocation;
        expect(validateExternalEvent(externalEvent)).to.be.false;
    });

    it('should not pass the external event validation on start date after the end date', () => {
        let startDate           = new Date('1969-11-16T00:00:00');
        let endDate             = new Date('1968-11-16T00:00:00');
        externalEvent.startDate = startDate;
        externalEvent.endDate   = endDate;
        expect(validateExternalEvent(externalEvent)).to.be.false;
    });

    it('should not pass the external event validation on total price smaller than 0', () => {
        let dummyPrice           = -28299.4;
        externalEvent.totalPrice = dummyPrice;
        expect(validateExternalEvent(externalEvent)).to.be.false;
    });

});

describe('Christmas event validation', () => {
    let christmasEvent: IChristmasEvent;
    beforeEach(() => {
        let regularBooking: IExternalBooking;
        let privateCustomer: IPrivateCustomer;
        let bookingInformation: IChristmasBookingInformation;
        let eventInformation: IChristmasEventInformation;
        let address     = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        privateCustomer = {
            addresses:      [address],
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'Leahu',
            lastName:       'Cristian',
            phoneNumbers:   ['12345678', '123456789'],
            kind:           'private customer'
        };

        bookingInformation = <IChristmasBookingInformation>{
            price: 33,
            numberOfParticipants: 10
        };
        eventInformation = <IChristmasEventInformation>{
            program: [{
                time: '',
                description: ''
            }],
            price: '',
            payment: '',
            menu: {
                normal: [{
                    title: '',
                    description: ''
                }],
                special: ''
            },
            drinks: '',
            coffee: '',
            bar: '',
            personalBill: '',
            drinksOrders: '',
            specialWishes: '',
            entertainment: '',
            wardRobe: '',
            dropoutAfterFinalBill: ''
        };

        regularBooking = {
            information: bookingInformation,
            customer:    privateCustomer,
            payment:     'cash',
            price:       2134.2,
            coordinator: null
        };

        let contactInfo: IContactInfo;
        contactInfo = {
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'John',
            lastName:       'Will',
            phoneNumbers:   ['50366711', '50367711']
        };

        address                                 = {
            city:     'Aarhus',
            street:   'Xvej',
            postcode: '8000',
            country:  'Denmark'
        };
        let externalLocation: IExternalLocation = {
            name:        "Aarhus hall",
            latitude:    55.233,
            longitude:   -10.24,
            address:     address,
            description: "Just a simple description",
            kind:        'external location'
        };
        let date                                = new Date('1968-11-16T00:00:00');

        christmasEvent = {
            bookings:      [regularBooking],
            information: eventInformation,
            description:   'Test description',
            date:          date,
            name:          'test',
            place:         externalLocation,
            pricePerSeat:  24,
            type:          'test',
            getTotalPrice: () => 323
        }
    });

    it('should pass the christmas event validation', () => {
        expect(validateChristmasEvent(christmasEvent)).to.be.true;
    });

    it('should pass the christmas event validation with an internal location', () => {
        let address: IAddress;
        address                                 = {
            city:     'Aarhus',
            street:   'Xvej',
            postcode: '8000',
            country:  'Denmark'
        };
        let internalLocation: IInternalLocation = {
            name:         "Aarhus hall",
            latitude:     55.233,
            longitude:    -10.24,
            address:      address,
            description:  "Just a simple description",
            capacity:     120,
            imagePaths:   [''],
            defaultPrice: 531,
            kind:         'internal location'
        };
        christmasEvent.place                    = internalLocation;
        expect(validateChristmasEvent(christmasEvent)).to.be.true;
    });

    it('should not pass the christmas event validation with an custom location kind', () => {
        let address: IAddress;
        address                    = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        let customLocationKindTest = {
            name:        "Aarhus hall",
            latitude:    55.233,
            longitude:   -10.24,
            address:     address,
            description: "Just a simple description",
            capacity:    120,
            imagePaths:  [''],
            kind:        'test'
        };
        christmasEvent.place       = customLocationKindTest;
        expect(validateChristmasEvent(christmasEvent)).to.be.false;
    });


    it('should not pass the christmas event validation on total price smaller than 0', () => {
        let dummyPricePerSeat       = -5;
        christmasEvent.pricePerSeat = dummyPricePerSeat;
        expect(validateChristmasEvent(christmasEvent)).to.be.false;
    });

});