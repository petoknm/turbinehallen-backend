import chai = require('chai');
import mocha = require('mocha');
import { validateInternalLocation, validateExternalLocation } from "../../validation/location";
import { IAddress } from "../../models/interface/address";
import { IInternalLocation } from "../../models/interface/internal-location";
import { IExternalLocation } from "../../models/interface/external-location";
const expect = chai.expect;

describe('Internal location test', () => {
    let address: IAddress;
    let internalLocation: IInternalLocation;

    beforeEach(()=> {
        address          = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        internalLocation = {
            name:        "Aarhus hall",
            latitude:    55.233,
            longitude:   -10.24,
            address:     address,
            description: "Just a simple description",
            capacity:    120,
            imagePaths:  [''],
            defaultPrice: 531,
            kind:'internal location'
        };

    });

    it('should pass the internal location validation', ()=> {
        expect(validateInternalLocation(internalLocation)).to.be.true;
    });

    it('should not pass the internal location validation on the name contains strange characters', ()=> {
        internalLocation.name = '43.;sxc';
        expect(validateInternalLocation(internalLocation)).to.be.false;
    });

    describe('Default price test', () =>{
        it('should pass the internal location validation on right amount of price with decimal', ()=> {
            internalLocation.defaultPrice=555.34;
            expect(validateInternalLocation(internalLocation)).to.be.true;
        });

        it('should pass the internal location validation on the price equal to 0', ()=> {
            internalLocation.defaultPrice=0;
            expect(validateInternalLocation(internalLocation)).to.be.true;
        });

        it('should not pass the internal location validation on negative price', ()=> {
            internalLocation.defaultPrice= -555;
            expect(validateInternalLocation(internalLocation)).to.be.false;
        });

        it('should not pass the internal location validation on negative price containing digits', ()=> {
            internalLocation.defaultPrice= -555.34;
            expect(validateInternalLocation(internalLocation)).to.be.false;
        });
    });

    describe('Capacity test', () => {
        it('should not pass on decimal capacity', ()=> {
            internalLocation.capacity = 4.3;
            expect(validateInternalLocation(internalLocation)).to.be.false;
        });

        it('should not pass on negative capacity', ()=> {
            internalLocation.capacity = -12;
            expect(validateInternalLocation(internalLocation)).to.be.false;
        });
    });
    //LATITUDE AND LONGITUDE HAVE SAME VALIDATION LOGIC, THEREFORE IT IS ENOUGH TO TEST ONLY ONE OF THEM.
    describe('Latitude and logitude test', () => {

        it('should pass on one digit before .', ()=> {
            internalLocation.latitude = 5.233;
            expect(validateInternalLocation(internalLocation)).to.be.true;
        });

        it('should pass on one negative digit before .', ()=> {
            internalLocation.latitude = -5.233;
            expect(validateInternalLocation(internalLocation)).to.be.true;
        });

        it('should pass on negative latitude', ()=> {
            internalLocation.latitude = -55.233;
            expect(validateInternalLocation(internalLocation)).to.be.true;
        });
        it('should pass on negative latitude with precision', ()=> {
            internalLocation.latitude = -55.233323131;
            expect(validateInternalLocation(internalLocation)).to.be.true;
        });


        it('should not pass on 3 digits before .', ()=> {
            internalLocation.latitude = -553.233;
            expect(validateInternalLocation(internalLocation)).to.be.false;
        });

        it('should not pass on , after the first 2 digits', ()=> {
            internalLocation.latitude = 43, 2;
            expect(validateInternalLocation(internalLocation)).to.be.false;
        });

        it('should not pass on value 0', ()=> {
            internalLocation.latitude = 0;
            expect(validateInternalLocation(internalLocation)).to.be.false;
        });

        //(i know it is strange, but is it still a RegEx and it can be interpreted as a string)
        it('should not pass on negative 0', ()=> {
            internalLocation.latitude = -0;
            expect(validateInternalLocation(internalLocation)).to.be.false;
        });
    });
});

describe('External location test', () => {
    let address: IAddress;
    let externalLocation: IExternalLocation;

    beforeEach(()=> {
        address          = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        externalLocation = {
            name:        "Aarhus hall",
            latitude:    55.233,
            longitude:   -10.24,
            address:     address,
            description: "Just a simple description",
            kind: 'external location'
        };
    });

    it('should pass the internal location validation', ()=> {
        expect(validateExternalLocation(externalLocation)).to.be.true;
    });

    it('should not pass the internal location validation on the name containing invalid characters', ()=> {
        externalLocation.name = '43.;sxc';
        expect(validateExternalLocation(externalLocation)).to.be.false;
    });

    //LATITUDE AND LONGITUDE HAVE SAME VALIDATION LOGIC, THEREFORE FOR NOW IT IS ENOUGH TO TEST ONLY ONE OF THEM.
    describe('Latitude and longitude test', () => {

        it('should pass on one digit before .', ()=> {
            externalLocation.latitude = 5.233;
            expect(validateExternalLocation(externalLocation)).to.be.true;
        });

        it('should pass on one negative digit before .', ()=> {
            externalLocation.latitude = -5.233;
            expect(validateExternalLocation(externalLocation)).to.be.true;
        });

        it('should pass on negative latitude', ()=> {
            externalLocation.latitude = -55.233;
            expect(validateExternalLocation(externalLocation)).to.be.true;
        });
        it('should pass on negative latitude with precision', ()=> {
            externalLocation.latitude = -55.233323131;
            expect(validateExternalLocation(externalLocation)).to.be.true;
        });


        it('should not pass on 3 digits before .', ()=> {
            externalLocation.latitude = -553.233;
            expect(validateExternalLocation(externalLocation)).to.be.false;
        });

        it('should not pass on , after the first 2 digits', ()=> {
            externalLocation.latitude = 43, 2;
            expect(validateExternalLocation(externalLocation)).to.be.false;
        });

        it('should not pass on value 0', ()=> {
            externalLocation.latitude = 0;
            expect(validateExternalLocation(externalLocation)).to.be.false;
        });

        //(i know it is strange, but is it still a RegEx and it can be interpreted as a string)
        it('should not pass on negative 0', ()=> {
            externalLocation.latitude = -0;
            expect(validateExternalLocation(externalLocation)).to.be.false;
        });
    });
});