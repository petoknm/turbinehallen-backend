import chai = require('chai');
import { IRegularEventInformation } from "../../models/interface/regular-event-information";
import { validateRegularEventInformation } from "../../validation/information";
const expect = chai.expect;

/**
 * Anything else but strings inside the information will throw compiler errors.
 * The strings inside do not have any constraints.
 */
describe('Regular information validation tests', ()=> {
    let information:IRegularEventInformation;
    beforeEach(()=> {
        information = <IRegularEventInformation> {
            program: [{
                         time: '22.19',
                         description: 'Program description'
                     }],
            price: '',
            payment: '',
            menu: {
                normal: [{
                            title: '',
                            description: ''
                        }],
                special: ''
            },
            drinks: {
                description: '',
                longDrinks: ['']
            },
            extraMaterial: '',
            entertainment: '',
            tableSettings: [{
                      title: '',
                      descriptions: ['']
                  }],
            tableLinen: '',
            extension: '',
            numberOfParticipants: 20,
            aproxNumberOfParticipants: 20
        };
    });

    it('should pass the information creation validation', () => {
        expect(validateRegularEventInformation(information)).to.be.true;
    });

});
