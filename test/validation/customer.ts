import chai = require('chai');
import { validatePrivateCustomer, validateBusinessCustomer } from "../../validation/customer";
import { IPrivateCustomer } from "../../models/interface/private-customer";
import { IAddress } from "../../models/interface/address";
import { IBusinessCustomer } from "../../models/interface/business-customer";
const expect = chai.expect;

describe('Private customer validation', () => {
    let address: IAddress;
    let privateCustomer: IPrivateCustomer;

    beforeEach(()=> {
        address         = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        privateCustomer = {
            addresses:      [address],
            emailAddresses: ['puiu@puiu.com'],
            firstName:      'Leahu',
            lastName:       'Cristian',
            phoneNumbers:   ['12345678', '123456789'],
            kind:'private customer'
        };
    });

    it('should pass the validation', () => {
        expect(validatePrivateCustomer(privateCustomer)).to.be.true;
    });

    it('should not pass the validation on the first name being a number', () => {
        privateCustomer.firstName = '3432';
        expect(validatePrivateCustomer(privateCustomer)).to.be.false;
    });

    it('should not pass the validation on the last name including spaces (only one name for now)', () => {
        privateCustomer.lastName = 'Test test';
        expect(validatePrivateCustomer(privateCustomer)).to.be.false;
    });

    it('should not pass the validation on the e-mail due to .test domain', () => {
        privateCustomer.emailAddresses.push('test@test.test');
        expect(validatePrivateCustomer(privateCustomer)).to.be.false;
    });

    it('should not pass the validation on the phone numbers not having right size', () => {
        privateCustomer.phoneNumbers.push('1234567');
        expect(validatePrivateCustomer(privateCustomer)).to.be.false;
    });
});

describe('Business customer validation', () => {
    let address: IAddress;
    let businessCustomer: IBusinessCustomer;

    beforeEach(()=> {
        address          = {city: 'Aarhus', street: 'Xvej', postcode: '8000', country: 'Denmark'};
        businessCustomer = {
            name:           'Uwe',
            cvr:            '1234567890',
            addresses:      [address],
            emailAddresses: ['normal_business_man@nato.com'],
            kind: 'business customer'
        };
    });

    it('should pass the validation', () => {
        expect(validateBusinessCustomer(businessCustomer)).to.be.true;
    });

    it('should not pass the validation on the email not having a good domain.', () => {
        businessCustomer.emailAddresses.push('restin@peace.notRight');
        expect(validateBusinessCustomer(businessCustomer)).to.be.false;
    });

    it('should not pass the validation on the name that includes spaces', () => {
        businessCustomer.name = '4322 test';
        expect(validateBusinessCustomer(businessCustomer)).to.be.false;
    });
});