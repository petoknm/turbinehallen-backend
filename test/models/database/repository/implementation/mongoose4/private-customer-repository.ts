import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { privateCustomerRepository } from "../../../../../../models/database/repository/implementation/mongoose4/private-customer-repository";
import { IPrivateCustomer } from "../../../../../../models/interface/private-customer";
import { IAddress } from "../../../../../../models/interface/address";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Private Customer Repository', () => {

    let testPrivateCustomer: IPrivateCustomer;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {
        let testAddress: IAddress;

        testAddress = {
            country:    'testCountry',
            city:       'testCity',
            postcode:   'testPostcode',
            street:     'testStreet'
        };

        testPrivateCustomer = {
            //Private Customer
            firstName:      'testFirstName',
            lastName:       'testLastName',
            phoneNumbers:   ['88'],

            //Customer
            addresses:      [testAddress],
            emailAddresses: ['someone.else@something.com'],
            kind:           'private'
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(privateCustomerRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
        return privateCustomerRepository.create(testPrivateCustomer)
            .then(() => expect(privateCustomerRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(privateCustomerRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no privateCustomers when empty', () => {
            return expect(privateCustomerRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all privateCustomers when not empty', () => {
            return Promise.all(
                [
                    privateCustomerRepository.create(testPrivateCustomer),
                    privateCustomerRepository.create(testPrivateCustomer),
                    privateCustomerRepository.create(testPrivateCustomer)
                ])
                .then(() => expect(privateCustomerRepository.findAll().then(privateCustomers => privateCustomers.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return privateCustomerRepository.create(testPrivateCustomer).then(() => {
                const privateCustomer = privateCustomerRepository.findAll().then(privateCustomers => privateCustomers[0]);
                return Promise.all(
                    [
                        expect(privateCustomer).to.eventually.have.property('id'),
                        expect(privateCustomer).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(privateCustomerRepository.findById(new ObjectID().toString())).to.be.rejectedWith('PrivateCustomer not found');
        });

        it('should resolve with found privateCustomer', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    return expect(privateCustomerRepository.findById(privateCustomer.id.toString())).to.eventually.be.deep.equal(privateCustomer);
                })
        });

        it('should not return array', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    return expect(privateCustomerRepository.findById(privateCustomer.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    const found = privateCustomerRepository.findById(privateCustomer.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(privateCustomer.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid privateCustomer', () => {
            return expect(privateCustomerRepository.create(testPrivateCustomer)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => expect(privateCustomer).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    expect(privateCustomer).to.have.property('id');
                    expect(privateCustomer).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testPrivateCustomer
                .id = new ObjectID();
            return expect(privateCustomerRepository.update(testPrivateCustomer)).to.be.rejectedWith('PrivateCustomer not found');
        });

        it('should rename _id to id', () => {
            const updatedPrivateCustomer: IPrivateCustomer = {
                id:             new ObjectID(),
                firstName:      'updatedFirstName',
                lastName:       'updatedLastName',
                phoneNumbers:   ['99'],
                addresses:      [],
                emailAddresses: ['updated@something.com'],
                kind:           'private'
            };
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    updatedPrivateCustomer.id = privateCustomer.id;
                    const update     = privateCustomerRepository.update(updatedPrivateCustomer);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            let updatedAddress: IAddress;

            updatedAddress = {
                country:    'updatedCountry',
                city:       'updatedCity',
                postcode:   'updatedPostcode',
                street:     'updatedStreet'
            };

            const updatedPrivateCustomer: IPrivateCustomer = {
                id:             new ObjectID(),
                firstName:      'updatedFirstName',
                lastName:       'updatedLastName',
                phoneNumbers:   ['99'],
                addresses:      [updatedAddress],
                emailAddresses: ['updated@something.com'],
                kind:           'private'
            };
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    updatedPrivateCustomer.id                  = privateCustomer.id;
                    const update     = privateCustomerRepository.update(updatedPrivateCustomer);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('firstName', 'updatedFirstName'),
                            expect(update).to.eventually.have.property('lastName', 'updatedLastName'),
                            expect(update).to.eventually.have.property('phoneNumbers').that.deep.equal(['99']),
                            expect(update).to.eventually.have.property('addresses').that.deep.equal([updatedAddress]),
                            expect(update).to.eventually.have.property('emailAddresses').that.deep.equal(['updated@something.com']),
                            // expect(update).to.eventually.have.property('kind', 'private'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testPrivateCustomer
                .id = new ObjectID();
            return expect(privateCustomerRepository.remove(testPrivateCustomer)).to.be.rejectedWith('PrivateCustomer not found');
        });

        it('should resolve when found', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    const remove = privateCustomerRepository.remove(privateCustomer);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the privateCustomer', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    return privateCustomerRepository.remove(privateCustomer)
                        .then(() => expect(privateCustomerRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(privateCustomerRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('PrivateCustomer not found');
        });

        it('should resolve when found', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    const remove = privateCustomerRepository.removeById(privateCustomer.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the privateCustomer', () => {
            return privateCustomerRepository.create(testPrivateCustomer)
                .then((privateCustomer: IPrivateCustomer) => {
                    return privateCustomerRepository.removeById(privateCustomer.id.toString())
                        .then(() => expect(privateCustomerRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});