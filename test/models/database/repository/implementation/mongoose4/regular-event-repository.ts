import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { regularEventRepository } from "../../../../../../models/database/repository/implementation/mongoose4/regular-event-repository";
import { IRegularEvent } from "../../../../../../models/interface/regular-event";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
import { IRegularEventInformation } from "../../../../../../models/interface/regular-event-information";
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Regular Event Repository', () => {

    let testRegularEvent: IRegularEvent;
    let testInformation: IRegularEventInformation;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {

        testInformation = {
            payment:                   'Test payment',
            menu:                      {special: 'Test special', normal: []},
            aproxNumberOfParticipants: 10,
            drinks:                    {longDrinks: [], description: 'Test description'},
            entertainment:             'Test entertainment',
            extension:                 'Test extension',
            extraMaterial:             'Test extraMaterial',
            price:                     '1000kr',
            program:                   [],
            tableLinen:                'Test linen',
            tableSettings:             [],
            numberOfParticipants:      10
        };

        testRegularEvent = {
            //Regular Event
            startDate:   new Date(2016, 12, 20, 0, 0, 0),
            endDate:     new Date(2016, 12, 21, 0, 0, 0),
            totalPrice:  1000,
            contactInfo: [],

            //Event
            place:       new ObjectID(),
            description: 'Much fun',
            bookings:    [new ObjectID()],
            name:        'Harty Pard',
            type:        'regular event',
            information: testInformation,

            getTotalPrice: function () {
                return this.totalPrice;
            }
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(regularEventRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return regularEventRepository.create(testRegularEvent)
                .then(() => expect(regularEventRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(regularEventRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no regularEvents when empty', () => {
            return expect(regularEventRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all regularEvents when not empty', () => {
            return Promise.all(
                [
                    regularEventRepository.create(testRegularEvent),
                    regularEventRepository.create(testRegularEvent),
                    regularEventRepository.create(testRegularEvent)
                ])
                .then(() => expect(regularEventRepository.findAll().then(regularEvents => regularEvents.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return regularEventRepository.create(testRegularEvent).then(() => {
                const regularEvent = regularEventRepository.findAll().then(regularEvents => regularEvents[0]);
                return Promise.all(
                    [
                        expect(regularEvent).to.eventually.have.property('id'),
                        expect(regularEvent).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(regularEventRepository.findById(new ObjectID().toString())).to.be.rejectedWith('RegularEvent not found');
        });

        it('should resolve with found regularEvent', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    return expect(regularEventRepository.findById(regularEvent.id.toString())).to.eventually.be.deep.equal(regularEvent);
                })
        });

        it('should not return array', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    return expect(regularEventRepository.findById(regularEvent.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    const found = regularEventRepository.findById(regularEvent.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(regularEvent.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid regularEvent', () => {
            return expect(regularEventRepository.create(testRegularEvent)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => expect(regularEvent).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    expect(regularEvent).to.have.property('id');
                    expect(regularEvent).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testRegularEvent
                .id = new ObjectID();
            return expect(regularEventRepository.update(testRegularEvent)).to.be.rejectedWith('RegularEvent not found');
        });

        it('should rename _id to id', () => {
            const updatedExternalEvent: IRegularEvent = {
                startDate:   new Date(2016, 12, 20, 0, 0, 0),
                endDate:     new Date(2016, 12, 21, 0, 0, 0),
                totalPrice:  2000,
                contactInfo: [],
                place:       new ObjectID(),
                description: 'Much much fun',
                bookings:    [new ObjectID(), new ObjectID()],
                name:        'Harty Pard v2',
                type:        'regular',
                information: testInformation,

                getTotalPrice: function () {
                    return this.totalPrice;
                }
            };
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    updatedExternalEvent.id = regularEvent.id;
                    const update            = regularEventRepository.update(updatedExternalEvent);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            const updatedExternalEvent: IRegularEvent = {
                // id:             new ObjectID(),
                startDate:   new Date(2016, 12, 20, 0, 0, 0),
                endDate:     new Date(2016, 12, 21, 0, 0, 0),
                totalPrice:  2000,
                contactInfo: [],
                place:       new ObjectID(),
                description: 'Much much fun',
                bookings:    [new ObjectID(), new ObjectID()],
                name:        'Harty Pard v2',
                type:        'regular event',
                information: testInformation,

                getTotalPrice: function () {
                    return this.totalPrice;
                }
            };
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    updatedExternalEvent.id = regularEvent.id;
                    const update            = regularEventRepository.update(updatedExternalEvent);
                    return Promise.all(
                        [
                            expect(update.then(u => u.startDate.getTime())).to.eventually.equal(new Date(2016, 12, 20, 0, 0, 0).getTime()),
                            expect(update.then(u => u.endDate.getTime())).to.eventually.equal(new Date(2016, 12, 21, 0, 0, 0).getTime()),
                            expect(update).to.eventually.have.property('totalPrice', 2000),

                            expect(update).to.eventually.have.property('description', 'Much much fun'),
                            expect(update).to.eventually.have.property('name', 'Harty Pard v2'),

                            expect(update).to.eventually.have.property('contactInfo').that.deep.equal(updatedExternalEvent.contactInfo),
                            expect(update).to.eventually.have.property('bookings').that.deep.equal(updatedExternalEvent.bookings),
                            expect(update.then(u => u.place.toString())).to.eventually.equal(updatedExternalEvent.place.toString()),
                            expect(update).to.eventually.have.property('type', 'regular event'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testRegularEvent
                .id = new ObjectID();
            return expect(regularEventRepository.remove(testRegularEvent)).to.be.rejectedWith('RegularEvent not found');
        });

        it('should resolve when found', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    const remove = regularEventRepository.remove(regularEvent);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the regularEvent', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    return regularEventRepository.remove(regularEvent)
                        .then(() => expect(regularEventRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(regularEventRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('RegularEvent not found');
        });

        it('should resolve when found', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    const remove = regularEventRepository.removeById(regularEvent.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the regularEvent', () => {
            return regularEventRepository.create(testRegularEvent)
                .then((regularEvent: IRegularEvent) => {
                    return regularEventRepository.removeById(regularEvent.id.toString())
                        .then(() => expect(regularEventRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});