import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { christmasBookingRepository } from "../../../../../../models/database/repository/implementation/mongoose4/christmas-booking-repository";
import { IChristmasBooking } from "../../../../../../models/interface/christmas-booking";
import { IContactInfo } from "../../../../../../models/interface/contact-info";
import { IChristmasBookingInformation } from "../../../../../../models/interface/christmas-booking-information";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Christmas Booking Repository', () => {
    let testChristmasBooking: IChristmasBooking;

    let testContactInfo: IContactInfo;
    let testInformation: IChristmasBookingInformation;
    let testAddress;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {
        testInformation = {numberOfParticipants: 10, price: 100};

        testAddress = {
            country:  'testCountry',
            city:     'testCity',
            postcode: 'testPostcode',
            street:   'testStreet'
        };

        testContactInfo = {
            id:             new ObjectID(),
            firstName:      'testFirstName',
            lastName:       'testLastName',
            phoneNumbers:   ['88'],
            emailAddresses: ['someone.else@something.com']
        };

        testChristmasBooking = {
            //Christmas Booking
            contactInfo: [testContactInfo],

            //Booking
            information: testInformation,
            customer:    new ObjectID(),
            coordinator: new ObjectID(),
            price:       10000,
            payment:     'cash',
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(christmasBookingRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then(() => expect(christmasBookingRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(christmasBookingRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no christmasBookings when empty', () => {
            return expect(christmasBookingRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all christmasBookings when not empty', () => {
            return Promise.all(
                [
                    christmasBookingRepository.create(testChristmasBooking),
                    christmasBookingRepository.create(testChristmasBooking),
                    christmasBookingRepository.create(testChristmasBooking)
                ])
                .then(() => expect(christmasBookingRepository.findAll().then(christmasBookings => christmasBookings.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return christmasBookingRepository.create(testChristmasBooking).then(() => {
                const christmasBooking = christmasBookingRepository.findAll().then(christmasBookings => christmasBookings[0]);
                return Promise.all(
                    [
                        expect(christmasBooking).to.eventually.have.property('id'),
                        expect(christmasBooking).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(christmasBookingRepository.findById(new ObjectID().toString())).to.be.rejectedWith('ChristmasBooking not found');
        });

        it('should resolve with found christmasBooking', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    return expect(christmasBookingRepository.findById(christmasBooking.id.toString())).to.eventually.be.deep.equal(christmasBooking);
                })
        });

        it('should not return array', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    return expect(christmasBookingRepository.findById(christmasBooking.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    const found = christmasBookingRepository.findById(christmasBooking.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(christmasBooking.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid christmasBooking', () => {
            return expect(christmasBookingRepository.create(testChristmasBooking)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => expect(christmasBooking).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    expect(christmasBooking).to.have.property('id');
                    expect(christmasBooking).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testChristmasBooking
                .id = new ObjectID();
            return expect(christmasBookingRepository.update(testChristmasBooking)).to.be.rejectedWith('ChristmasBooking not found');
        });

        it('should rename _id to id', () => {
            const updatedchristmasBooking: IChristmasBooking = {
                contactInfo: [testContactInfo],
                information: testInformation,
                customer:    new ObjectID(),
                coordinator: new ObjectID(),
                price:       20000,
                payment:     'transfer',
            };
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    updatedchristmasBooking.id = christmasBooking.id;
                    const update               = christmasBookingRepository.update(updatedchristmasBooking);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            let updatedContactInfo: IContactInfo = {
                id:             new ObjectID(),
                firstName:      'updatedFirstName',
                lastName:       'updatedLastName',
                phoneNumbers:   ['99'],
                emailAddresses: ['updated@something.com']
            };

            let updatedInformation: IChristmasBookingInformation = {
                numberOfParticipants: 20,
                price:                200
            };

            const updatedchristmasBooking: IChristmasBooking = {
                id:          new ObjectID(),
                contactInfo: [updatedContactInfo],
                information: updatedInformation,
                customer:    new ObjectID(),
                coordinator: new ObjectID(),
                price:       20000,
                payment:     'transfer',
            };
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    updatedchristmasBooking.id = christmasBooking.id;
                    const update               = christmasBookingRepository.update(updatedchristmasBooking);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('price', 20000),
                            expect(update).to.eventually.have.property('payment', 'transfer'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testChristmasBooking
                .id = new ObjectID();
            return expect(christmasBookingRepository.remove(testChristmasBooking)).to.be.rejectedWith('ChristmasBooking not found');
        });

        it('should resolve when found', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    const remove = christmasBookingRepository.remove(christmasBooking);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the christmasBooking', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    return christmasBookingRepository.remove(christmasBooking)
                        .then(() => expect(christmasBookingRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(christmasBookingRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('ChristmasBooking not found');
        });

        it('should resolve when found', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    const remove = christmasBookingRepository.removeById(christmasBooking.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the christmasBooking', () => {
            return christmasBookingRepository.create(testChristmasBooking)
                .then((christmasBooking: IChristmasBooking) => {
                    return christmasBookingRepository.removeById(christmasBooking.id.toString())
                        .then(() => expect(christmasBookingRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});