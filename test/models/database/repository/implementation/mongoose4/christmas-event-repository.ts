import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { christmasEventRepository } from "../../../../../../models/database/repository/implementation/mongoose4/christmas-event-repository";
import { IChristmasEvent } from "../../../../../../models/interface/christmas-event";
import { IContactInfo } from "../../../../../../models/interface/contact-info";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
import { IChristmasEventInformation } from "../../../../../../models/interface/christmas-event-information";
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Christmas Event Repository', () => {
    let testChristmasEvent: IChristmasEvent;

    let testContactInfo: IContactInfo;
    let testInformation: IChristmasEventInformation;
    let testAddress;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {
        testContactInfo = {
            id:             new ObjectID(),
            firstName:      'testFirstName',
            lastName:       'testLastName',
            phoneNumbers:   ['88'],
            emailAddresses: ['someone.else@something.com']
        };

        testInformation = {
            price:                 '100',
            bar:                   'Test bar',
            coffee:                'Test coffee',
            drinks:                'Test drinks',
            drinksOrders:          'Test Drink Orders',
            dropoutAfterFinalBill: 'Test test???',
            entertainment:         'Test entertainment',
            payment:               'Test payment',
            personalBill:          'Test personal bill',
            wardRobe:              'Test test???',
            specialWishes:         'Test special wishes',
            menu:                  {special: 'Test special', normal: []},
            program:               []
        };

        testAddress = {
            country:  'testCountry',
            city:     'testCity',
            postcode: 'testPostcode',
            street:   'testStreet'
        };

        testChristmasEvent = {
            //Christmas Event
            date:         new Date(2016, 12, 10, 0, 0, 0),
            pricePerSeat: 1000,

            //Event
            place:       new ObjectID(),
            description: 'Much fun',
            bookings:    [new ObjectID()],
            name:        'Harty Pard',
            type:        'christmas',
            information: testInformation,

            getTotalPrice: function () {
                return this.totalPrice;
            }
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(christmasEventRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then(() => expect(christmasEventRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(christmasEventRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no christmasEvents when empty', () => {
            return expect(christmasEventRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all christmasEvents when not empty', () => {
            return Promise.all(
                [
                    christmasEventRepository.create(testChristmasEvent),
                    christmasEventRepository.create(testChristmasEvent),
                    christmasEventRepository.create(testChristmasEvent)
                ])
                .then(() => expect(christmasEventRepository.findAll().then(christmasEvents => christmasEvents.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return christmasEventRepository.create(testChristmasEvent).then(() => {
                const christmasEvent = christmasEventRepository.findAll().then(christmasEvents => christmasEvents[0]);
                return Promise.all(
                    [
                        expect(christmasEvent).to.eventually.have.property('id'),
                        expect(christmasEvent).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(christmasEventRepository.findById(new ObjectID().toString())).to.be.rejectedWith('ChristmasEvent not found');
        });

        it('should resolve with found christmasEvent', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    return expect(christmasEventRepository.findById(christmasEvent.id.toString())).to.eventually.be.deep.equal(christmasEvent);
                })
        });

        it('should not return array', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    return expect(christmasEventRepository.findById(christmasEvent.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    const found = christmasEventRepository.findById(christmasEvent.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(christmasEvent.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid christmasEvent', () => {
            return expect(christmasEventRepository.create(testChristmasEvent)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => expect(christmasEvent).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    expect(christmasEvent).to.have.property('id');
                    expect(christmasEvent).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testChristmasEvent
                .id = new ObjectID();
            return expect(christmasEventRepository.update(testChristmasEvent)).to.be.rejectedWith('ChristmasEvent not found');
        });

        it('should rename _id to id', () => {
            const updatedChristmasEvent: IChristmasEvent = {
                id:           new ObjectID(),
                date:         new Date(2016, 12, 20, 0, 0, 0),
                pricePerSeat: 2000,
                place:        new ObjectID(),
                description:  'Much much fun',
                bookings:     [new ObjectID()],
                name:         'Harty Pard v2',
                type:         'christmas',
                information:  testInformation,

                getTotalPrice: function () {
                    return this.totalPrice;
                }
            };
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    updatedChristmasEvent.id = christmasEvent.id;
                    const update             = christmasEventRepository.update(updatedChristmasEvent);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            const updatedChristmasEvent: IChristmasEvent = {
                id:           new ObjectID(),
                date:         new Date(2016, 12, 20, 0, 0, 0),
                pricePerSeat: 2000,
                place:        new ObjectID(),
                description:  'Much much fun',
                bookings:     [new ObjectID(), new ObjectID()],
                name:         'Harty Pard v2',
                type:         'christmas',
                information:  testInformation,

                getTotalPrice: function () {
                    return this.totalPrice;
                }
            };
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    updatedChristmasEvent.id = christmasEvent.id;
                    const update             = christmasEventRepository.update(updatedChristmasEvent);
                    return Promise.all(
                        [
                            expect(update.then(u => u.date.getTime())).to.eventually.equal(new Date(2016, 12, 20, 0, 0, 0).getTime()),
                            expect(update).to.eventually.have.property('pricePerSeat', 2000),

                            expect(update).to.eventually.have.property('description', 'Much much fun'),
                            expect(update).to.eventually.have.property('name', 'Harty Pard v2'),

                            expect(update).to.eventually.have.property('bookings').that.deep.equal(updatedChristmasEvent.bookings),
                            expect(update).to.eventually.have.property('place').that.deep.equal(updatedChristmasEvent.place),
                            expect(update).to.eventually.have.property('type', 'christmas'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testChristmasEvent
                .id = new ObjectID();
            return expect(christmasEventRepository.remove(testChristmasEvent)).to.be.rejectedWith('ChristmasEvent not found');
        });

        it('should resolve when found', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    const remove = christmasEventRepository.remove(christmasEvent);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the christmasEvent', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    return christmasEventRepository.remove(christmasEvent)
                        .then(() => expect(christmasEventRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(christmasEventRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('ChristmasEvent not found');
        });

        it('should resolve when found', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    const remove = christmasEventRepository.removeById(christmasEvent.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the christmasEvent', () => {
            return christmasEventRepository.create(testChristmasEvent)
                .then((christmasEvent: IChristmasEvent) => {
                    return christmasEventRepository.removeById(christmasEvent.id.toString())
                        .then(() => expect(christmasEventRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});