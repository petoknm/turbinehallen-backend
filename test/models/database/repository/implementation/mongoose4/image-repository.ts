// import chai = require("chai");
// import chaiAsPromised = require("chai-as-promised");
// import { imageRepository } from "../../../../../../models/database/repository/implementation/mongoose4/image-repository";
// import { Writable, PassThrough, Readable } from "stream";
// import { ObjectID } from "mongodb";
// import mongoose = require('mongoose');
// import mockgoose = require('mockgoose');
// import fs = require('fs');
// const expect = chai.expect;
//
// chai.use(chaiAsPromised);
//
// describe('Image Repository', () => {
//
//     before(() => {
//         mongoose.Promise = Promise;
//         const options    = {promiseLibrary: Promise};
//         return mockgoose(mongoose)
//             .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
//     });
//
//     after(() => {
//         return mongoose.disconnect();
//     });
//
//     beforeEach((done) => {
//         mockgoose.reset(() => done());
//     });
//
//     describe('#write', () => {
//
//         it('should return a writable stream', () => {
//             return expect(imageRepository.write('test.txt')).to.be.an.instanceOf(Writable);
//         });
//
//         it('should be able to save file and read it back', (done) => {
//             let file = new PassThrough();
//             file.push('This is a test');
//             file.push(null);
//             file.pipe(imageRepository.write('test.txt')).on('finish', () => {
//                 let read = imageRepository.readByName('test.txt');
//                 let data;
//                 read.on('data', d => {
//                     data = d;
//                 });
//                 read.on('close', () => {
//                     expect(data.toString()).to.equal('This is a test');
//                     done();
//                 });
//             });
//         });
//
//     });
//
//     describe('#readByName', () => {
//
//         it('should return a readable stream', () => {
//             return expect(imageRepository.readByName('test.txt')).to.be.an.instanceOf(Readable);
//         });
//
//         it('should be return error when not found', (done) => {
//             let read = imageRepository.readByName('test.txt');
//             let data = null;
//             read.on('data', d => {
//                 data = d;
//             });
//             read.on('error', e => {
//                 expect(data).to.be.null;
//                 expect(e).to.be.an.instanceOf(Error);
//                 done();
//             });
//         });
//
//         it('should return correct readable stream when found', (done) => {
//             let file = new PassThrough();
//             file.push('This is a test');
//             file.push(null);
//             file.pipe(imageRepository.write('test.txt')).on('finish', () => {
//                 let read = imageRepository.readByName('test.txt');
//                 let data;
//                 read.on('data', d => {
//                     data = d;
//                 });
//                 read.on('close', () => {
//                     expect(data.toString()).to.equal('This is a test');
//                     done();
//                 });
//             });
//         });
//
//     });
//
//     describe('#readById', () => {
//
//         it('should return a readable stream', () => {
//             return expect(imageRepository.readById(new ObjectID().toString())).to.be.an.instanceOf(Readable);
//         });
//
//         it('should be return error when not found', (done) => {
//             let read = imageRepository.readById(new ObjectID().toString());
//             let data = null;
//             read.on('data', d => {
//                 data = d;
//             });
//             read.on('error', e => {
//                 expect(data).to.be.null;
//                 expect(e).to.be.an.instanceOf(Error);
//                 done();
//             });
//         });
//
//         it('should return correct readable stream when found', (done) => {
//             let file = new PassThrough();
//             file.push('This is a test');
//             file.push(null);
//             let write = file.pipe(imageRepository.write('test.txt'));
//             write.on('finish', file => {
//                 let read = imageRepository.readById(file._id.toString());
//                 let data;
//                 read.on('data', d => {
//                     data = d;
//                 });
//                 read.on('close', () => {
//                     expect(data.toString()).to.equal('This is a test');
//                     done();
//                 });
//             });
//         });
//
//     });
//
//     describe('#removeByName', () => {
//
//         it('should return a promise', () => {
//             return expect(imageRepository.removeByName('test.txt').catch(() => {
//             }))
//                 .to.be.an.instanceOf(Promise);
//         });
//
//         it('should reject if not found', () => {
//             return expect(imageRepository.removeByName('test.txt')).to.be.rejectedWith('Image not found');
//         });
//
//         it('should resolve if found', (done) => {
//             let file = new PassThrough();
//             file.push('This is a test');
//             file.push(null);
//             let write = file.pipe(imageRepository.write('test.txt'));
//             write.on('finish', () => {
//                 imageRepository.removeByName('test.txt').then(() => done());
//             });
//         });
//
//         it('should remove file if found', (done) => {
//             let file = new PassThrough();
//             file.push('This is a test');
//             file.push(null);
//             let write = file.pipe(imageRepository.write('test.txt'));
//             write.on('finish', () => {
//                 imageRepository.removeByName('test.txt')
//                     .then(() => {
//                         let read = imageRepository.readByName('test.txt');
//                         let data = null;
//                         read.on('data', d => {
//                             data = d;
//                         });
//                         read.on('error', e => {
//                             expect(data).to.be.null;
//                             expect(e).to.be.an.instanceOf(Error);
//                             done();
//                         });
//                     })
//             });
//         });
//
//     });
//
//     describe('#removeById', () => {
//
//         it('should return a promise', () => {
//             return expect(imageRepository.removeById(new ObjectID().toString()).catch(() => {
//             }))
//                 .to.be.an.instanceOf(Promise);
//         });
//
//         it('should reject if not found', () => {
//             return expect(imageRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('Image not found');
//         });
//
//         it('should resolve if found', (done) => {
//             let file = new PassThrough();
//             file.push('This is a test');
//             file.push(null);
//             let write = file.pipe(imageRepository.write('test.txt'));
//             write.on('finish', file => {
//                 imageRepository.removeById(file._id.toString()).then(() => done());
//             });
//         });
//
//         it('should remove file if found', (done) => {
//             let file = new PassThrough();
//             file.push('This is a test');
//             file.push(null);
//             let write = file.pipe(imageRepository.write('test.txt'));
//             write.on('finish', file => {
//                 imageRepository.removeById(file._id.toString())
//                     .then(() => {
//                         let read = imageRepository.readByName('test.txt');
//                         let data = null;
//                         read.on('data', d => {
//                             data = d;
//                         });
//                         read.on('error', e => {
//                             expect(data).to.be.null;
//                             expect(e).to.be.an.instanceOf(Error);
//                             done();
//                         });
//                     })
//             });
//         });
//
//     });
//
// });