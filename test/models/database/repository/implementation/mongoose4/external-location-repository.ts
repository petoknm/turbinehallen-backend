import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { externalLocationRepository } from "../../../../../../models/database/repository/implementation/mongoose4/external-location-repository";
import { IExternalLocation } from "../../../../../../models/interface/external-location";
import { IAddress } from "../../../../../../models/interface/address";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('External Location Repository', () => {

    let testExternalLocation: IExternalLocation;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {
        let testAddress: IAddress;

        testAddress = {
            country:  'testCountry',
            city:     'testCity',
            postcode: 'testPostcode',
            street:   'testStreet'
        };

        testExternalLocation = {
            name:        'testLocation',
            latitude:    56.1567400,
            longitude:   10.2107600,
            address:     testAddress,
            description: 'Nothing more than just a test location.',
            kind:        'external'
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(externalLocationRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then(() => expect(externalLocationRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(externalLocationRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no externalLocations when empty', () => {
            return expect(externalLocationRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all externalLocations when not empty', () => {
            return Promise.all(
                [
                    externalLocationRepository.create(testExternalLocation),
                    externalLocationRepository.create(testExternalLocation),
                    externalLocationRepository.create(testExternalLocation)
                ])
                .then(() => expect(externalLocationRepository.findAll().then(externalLocations => externalLocations.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return externalLocationRepository.create(testExternalLocation).then(() => {
                const externalLocation = externalLocationRepository.findAll().then(externalLocations => externalLocations[0]);
                return Promise.all(
                    [
                        expect(externalLocation).to.eventually.have.property('id'),
                        expect(externalLocation).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(externalLocationRepository.findById(new ObjectID().toString())).to.be.rejectedWith('ExternalLocation not found');
        });

        it('should resolve with found externalLocation', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    return expect(externalLocationRepository.findById(externalLocation.id.toString())).to.eventually.be.deep.equal(externalLocation);
                })
        });

        it('should not return array', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    return expect(externalLocationRepository.findById(externalLocation.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    const found = externalLocationRepository.findById(externalLocation.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(externalLocation.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid externalLocation', () => {
            return expect(externalLocationRepository.create(testExternalLocation)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => expect(externalLocation).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    expect(externalLocation).to.have.property('id');
                    expect(externalLocation).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testExternalLocation
                .id = new ObjectID();
            return expect(externalLocationRepository.update(testExternalLocation)).to.be.rejectedWith('ExternalLocation not found');
        });

        it('should rename _id to id', () => {
            let updatedAddress: IAddress;
            updatedAddress = {
                country:  'updatedCountry',
                city:     'updatedCity',
                postcode: 'updatedPostcode',
                street:   'updatedStreet'
            };

            const updatedExternalLocation: IExternalLocation = {
                id:          new ObjectID(),
                name:        'updatedLocation',
                latitude:    38.897096,
                longitude:   -77.036545,
                address:     updatedAddress,
                description: 'Guess who is living there now.',
                kind:        'external'
            };
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    updatedExternalLocation.id = externalLocation.id;
                    const update               = externalLocationRepository.update(updatedExternalLocation);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            let updatedAddress: IAddress;
            updatedAddress = {
                country:  'updatedCountry',
                city:     'updatedCity',
                postcode: 'updatedPostcode',
                street:   'updatedStreet'
            };

            const updatedExternalLocation: IExternalLocation = {
                id:          new ObjectID(),
                name:        'updatedLocation',
                latitude:    38.897096,
                longitude:   -77.036545,
                address:     updatedAddress,
                description: 'Guess who is living there now. Yay!',
                kind:        'external'
            };
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    updatedExternalLocation.id = externalLocation.id;
                    const update               = externalLocationRepository.update(updatedExternalLocation);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('name', 'updatedLocation'),
                            expect(update).to.eventually.have.property('latitude', 38.897096),
                            expect(update).to.eventually.have.property('longitude', -77.036545),
                            expect(update).to.eventually.have.property('address').that.deep.equal(updatedAddress),
                            expect(update).to.eventually.have.property('description', 'Guess who is living there now. Yay!'),
                            // expect(update).to.eventually.have.property('kind', 'external'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testExternalLocation
                .id = new ObjectID();
            return expect(externalLocationRepository.remove(testExternalLocation)).to.be.rejectedWith('ExternalLocation not found');
        });

        it('should resolve when found', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    const remove = externalLocationRepository.remove(externalLocation);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the externalLocation', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    return externalLocationRepository.remove(externalLocation)
                        .then(() => expect(externalLocationRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(externalLocationRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('ExternalLocation not found');
        });

        it('should resolve when found', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    const remove = externalLocationRepository.removeById(externalLocation.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the externalLocation', () => {
            return externalLocationRepository.create(testExternalLocation)
                .then((externalLocation: IExternalLocation) => {
                    return externalLocationRepository.removeById(externalLocation.id.toString())
                        .then(() => expect(externalLocationRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});