import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { regularBookingRepository } from "../../../../../../models/database/repository/implementation/mongoose4/regular-booking-repository";
import { IRegularBooking } from "../../../../../../models/interface/regular-booking";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
import { IRegularBookingInformation } from "../../../../../../models/interface/regular-booking-information";
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Regular Booking Repository', () => {

    let testRegularBooking: IRegularBooking;
    let testInformation: IRegularBookingInformation;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {

        testInformation = {
            price:   100,
            payment: 'Test payment'
        };

        testRegularBooking = {
            id: new ObjectID(),

            //Booking
            information: testInformation,
            customer:    new ObjectID(),
            coordinator: new ObjectID(),
            price:       10000,
            payment:     'cash',
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(regularBookingRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then(() => expect(regularBookingRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(regularBookingRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no regularBookings when empty', () => {
            return expect(regularBookingRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all regularBookings when not empty', () => {
            return Promise.all(
                [
                    regularBookingRepository.create(testRegularBooking),
                    regularBookingRepository.create(testRegularBooking),
                    regularBookingRepository.create(testRegularBooking)
                ])
                .then(() => expect(regularBookingRepository.findAll().then(regularBookings => regularBookings.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return regularBookingRepository.create(testRegularBooking).then(() => {
                const regularBooking = regularBookingRepository.findAll().then(regularBookings => regularBookings[0]);
                return Promise.all(
                    [
                        expect(regularBooking).to.eventually.have.property('id'),
                        expect(regularBooking).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(regularBookingRepository.findById(new ObjectID().toString())).to.be.rejectedWith('RegularBooking not found');
        });

        it('should resolve with found regularBooking', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    return expect(regularBookingRepository.findById(regularBooking.id.toString())).to.eventually.be.deep.equal(regularBooking);
                })
        });

        it('should not return array', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    return expect(regularBookingRepository.findById(regularBooking.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    const found = regularBookingRepository.findById(regularBooking.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(regularBooking.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid regularBooking', () => {
            return expect(regularBookingRepository.create(testRegularBooking)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => expect(regularBooking).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    expect(regularBooking).to.have.property('id');
                    expect(regularBooking).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testRegularBooking
                .id = new ObjectID();
            return expect(regularBookingRepository.update(testRegularBooking)).to.be.rejectedWith('RegularBooking not found');
        });

        it('should rename _id to id', () => {
            const updatedRegularBooking: IRegularBooking = {
                id: new ObjectID(),

                //Booking
                information: testInformation,
                customer:    new ObjectID(),
                coordinator: new ObjectID(),
                price:       20000,
                payment:     'transfer',
            };
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    updatedRegularBooking.id = regularBooking.id;
                    const update             = regularBookingRepository.update(updatedRegularBooking);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            const updatedRegularBooking: IRegularBooking = {
                id: new ObjectID(),

                //Booking
                information: testInformation,
                customer:    new ObjectID(),
                coordinator: new ObjectID(),
                price:       20000,
                payment:     'transfer',
            };
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    updatedRegularBooking.id = regularBooking.id;
                    const update             = regularBookingRepository.update(updatedRegularBooking);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('price', 20000),
                            expect(update).to.eventually.have.property('payment', 'transfer'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testRegularBooking
                .id = new ObjectID();
            return expect(regularBookingRepository.remove(testRegularBooking)).to.be.rejectedWith('RegularBooking not found');
        });

        it('should resolve when found', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    const remove = regularBookingRepository.remove(regularBooking);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the regularBooking', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    return regularBookingRepository.remove(regularBooking)
                        .then(() => expect(regularBookingRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(regularBookingRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('RegularBooking not found');
        });

        it('should resolve when found', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    const remove = regularBookingRepository.removeById(regularBooking.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the regularBooking', () => {
            return regularBookingRepository.create(testRegularBooking)
                .then((regularBooking: IRegularBooking) => {
                    return regularBookingRepository.removeById(regularBooking.id.toString())
                        .then(() => expect(regularBookingRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});