import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { businessCustomerRepository } from "../../../../../../models/database/repository/implementation/mongoose4/business-customer-repository";
import { IBusinessCustomer } from "../../../../../../models/interface/business-customer";
import { IAddress } from "../../../../../../models/interface/address";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Business Customer Repository', () => {

    let testBusinessCustomer: IBusinessCustomer;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {
        let testAddress: IAddress;

        testAddress = {
            country:    'testCountry',
            city:       'testCity',
            postcode:   'testPostcode',
            street:     'testStreet'
        };

        testBusinessCustomer = {
            id:             new ObjectID(),

            //Business Customer
            name:           'TestBusinessCustomer',
            cvr:            '666',


            addresses:      [testAddress],
            emailAddresses: ['someone.else@something.com'],
            kind:           'business'
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(businessCustomerRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
        return businessCustomerRepository.create(testBusinessCustomer)
            .then(() => expect(businessCustomerRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(businessCustomerRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no businessCustomers when empty', () => {
            return expect(businessCustomerRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all businessCustomers when not empty', () => {
            return Promise.all(
                [
                    businessCustomerRepository.create(testBusinessCustomer),
                    businessCustomerRepository.create(testBusinessCustomer),
                    businessCustomerRepository.create(testBusinessCustomer)
                ])
                .then(() => expect(businessCustomerRepository.findAll().then(businessCustomers => businessCustomers.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return businessCustomerRepository.create(testBusinessCustomer).then(() => {
                const businessCustomer = businessCustomerRepository.findAll().then(businessCustomers => businessCustomers[0]);
                return Promise.all(
                    [
                        expect(businessCustomer).to.eventually.have.property('id'),
                        expect(businessCustomer).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(businessCustomerRepository.findById(new ObjectID().toString())).to.be.rejectedWith('BusinessCustomer not found');
        });

        it('should resolve with found businessCustomer', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    return expect(businessCustomerRepository.findById(businessCustomer.id.toString())).to.eventually.be.deep.equal(businessCustomer);
                })
        });

        it('should not return array', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    return expect(businessCustomerRepository.findById(businessCustomer.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    const found = businessCustomerRepository.findById(businessCustomer.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(businessCustomer.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid businessCustomer', () => {
            return expect(businessCustomerRepository.create(testBusinessCustomer)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => expect(businessCustomer).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    expect(businessCustomer).to.have.property('id');
                    expect(businessCustomer).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testBusinessCustomer
                .id = new ObjectID();
            return expect(businessCustomerRepository.update(testBusinessCustomer)).to.be.rejectedWith('BusinessCustomer not found');
        });

        it('should rename _id to id', () => {
            const updatedBusinessCustomer: IBusinessCustomer = {
                id:             new ObjectID(),
                name:           'updatedBusinessCustomer',
                cvr:            '777',
                addresses:      [],
                emailAddresses: ['updated@something.com'],
                kind:           'business'
            };
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    updatedBusinessCustomer.id = businessCustomer.id;
                    const update     = businessCustomerRepository.update(updatedBusinessCustomer);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            let updatedAddress: IAddress;

            updatedAddress = {
                country:    'updatedCountry',
                city:       'updatedCity',
                postcode:   'updatedPostcode',
                street:     'updatedStreet'
            };

            const updatedBusinessCustomer: IBusinessCustomer = {
                id:             new ObjectID(),
                name:           'updatedBusinessCustomer',
                cvr:            '777',
                addresses:      [updatedAddress],
                emailAddresses: ['updated@something.com'],
                kind:           'business'
            };
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    updatedBusinessCustomer.id                  = businessCustomer.id;
                    const update     = businessCustomerRepository.update(updatedBusinessCustomer);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('name', 'updatedBusinessCustomer'),
                            expect(update).to.eventually.have.property('cvr', '777'),
                            expect(update).to.eventually.have.property('addresses').that.deep.equal([updatedAddress]),
                            expect(update).to.eventually.have.property('emailAddresses').that.deep.equal(['updated@something.com']),
                            // expect(update).to.eventually.have.property('kind', 'business'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testBusinessCustomer
                .id = new ObjectID();
            return expect(businessCustomerRepository.remove(testBusinessCustomer)).to.be.rejectedWith('BusinessCustomer not found');
        });

        it('should resolve when found', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    const remove = businessCustomerRepository.remove(businessCustomer);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the businessCustomer', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    return businessCustomerRepository.remove(businessCustomer)
                        .then(() => expect(businessCustomerRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(businessCustomerRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('BusinessCustomer not found');
        });

        it('should resolve when found', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    const remove = businessCustomerRepository.removeById(businessCustomer.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the businessCustomer', () => {
            return businessCustomerRepository.create(testBusinessCustomer)
                .then((businessCustomer: IBusinessCustomer) => {
                    return businessCustomerRepository.removeById(businessCustomer.id.toString())
                        .then(() => expect(businessCustomerRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});