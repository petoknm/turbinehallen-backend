import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { externalBookingRepository } from "../../../../../../models/database/repository/implementation/mongoose4/external-booking-repository";
import { IExternalBooking } from "../../../../../../models/interface/external-booking";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('External Booking Repository', () => {

    let testExternalBooking: IExternalBooking;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {
        testExternalBooking = {
            id:             new ObjectID(),

            //Booking
            information:    [new ObjectID()],
            customer:       new ObjectID(),
            coordinator:    new ObjectID(),
            price:          10000,
            payment:        'cash',
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(externalBookingRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
        return externalBookingRepository.create(testExternalBooking)
            .then(() => expect(externalBookingRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(externalBookingRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no externalBookings when empty', () => {
            return expect(externalBookingRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all externalBookings when not empty', () => {
            return Promise.all(
                [
                    externalBookingRepository.create(testExternalBooking),
                    externalBookingRepository.create(testExternalBooking),
                    externalBookingRepository.create(testExternalBooking)
                ])
                .then(() => expect(externalBookingRepository.findAll().then(externalBookings => externalBookings.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return externalBookingRepository.create(testExternalBooking).then(() => {
                const externalBooking = externalBookingRepository.findAll().then(externalBookings => externalBookings[0]);
                return Promise.all(
                    [
                        expect(externalBooking).to.eventually.have.property('id'),
                        expect(externalBooking).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(externalBookingRepository.findById(new ObjectID().toString())).to.be.rejectedWith('ExternalBooking not found');
        });

        it('should resolve with found externalBooking', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    return expect(externalBookingRepository.findById(externalBooking.id.toString())).to.eventually.be.deep.equal(externalBooking);
                })
        });

        it('should not return array', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    return expect(externalBookingRepository.findById(externalBooking.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    const found = externalBookingRepository.findById(externalBooking.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(externalBooking.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid externalBooking', () => {
            return expect(externalBookingRepository.create(testExternalBooking)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => expect(externalBooking).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    expect(externalBooking).to.have.property('id');
                    expect(externalBooking).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testExternalBooking
                .id = new ObjectID();
            return expect(externalBookingRepository.update(testExternalBooking)).to.be.rejectedWith('ExternalBooking not found');
        });

        it('should rename _id to id', () => {
            const updatedexternalBooking: IExternalBooking = {
                id:             new ObjectID(),

                //Booking
                information:    [new ObjectID()],
                customer:       new ObjectID(),
                coordinator:    new ObjectID(),
                price:          20000,
                payment:        'transfer',
            };
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    updatedexternalBooking.id = externalBooking.id;
                    const update     = externalBookingRepository.update(updatedexternalBooking);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            const updatedexternalBooking: IExternalBooking = {
                id:             new ObjectID(),

                //Booking
                information:    [new ObjectID()],
                customer:       new ObjectID(),
                coordinator:    new ObjectID(),
                price:          20000,
                payment:        'transfer',
            };
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    updatedexternalBooking.id                  = externalBooking.id;
                    const update     = externalBookingRepository.update(updatedexternalBooking);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('price', 20000),
                            expect(update).to.eventually.have.property('payment', 'transfer'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testExternalBooking
                .id = new ObjectID();
            return expect(externalBookingRepository.remove(testExternalBooking)).to.be.rejectedWith('ExternalBooking not found');
        });

        it('should resolve when found', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    const remove = externalBookingRepository.remove(externalBooking);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the externalBooking', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    return externalBookingRepository.remove(externalBooking)
                        .then(() => expect(externalBookingRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(externalBookingRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('ExternalBooking not found');
        });

        it('should resolve when found', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    const remove = externalBookingRepository.removeById(externalBooking.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the externalBooking', () => {
            return externalBookingRepository.create(testExternalBooking)
                .then((externalBooking: IExternalBooking) => {
                    return externalBookingRepository.removeById(externalBooking.id.toString())
                        .then(() => expect(externalBookingRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});