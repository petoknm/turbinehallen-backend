import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { internalLocationRepository } from "../../../../../../models/database/repository/implementation/mongoose4/internal-location-repository";
import { IInternalLocation } from "../../../../../../models/interface/internal-location";
import { IAddress } from "../../../../../../models/interface/address";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Internal Location Repository', () => {

    let testInternalLocation: IInternalLocation;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {
        let testAddress: IAddress;

        testAddress = {
            country:  'testCountry',
            city:     'testCity',
            postcode: 'testPostcode',
            street:   'testStreet'
        };

        testInternalLocation = {
            id: new ObjectID(),

            //Internal Location
            capacity:     500,
            imagePaths:   [],
            defaultPrice: 5000,

            //Location
            name:        'testLocation',
            latitude:    56.1567400,
            longitude:   10.2107600,
            address:     testAddress,
            description: 'Nothing more than just a test location.',
            kind:        'internal'
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(internalLocationRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then(() => expect(internalLocationRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(internalLocationRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no internalLocations when empty', () => {
            return expect(internalLocationRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all internalLocations when not empty', () => {
            return Promise.all(
                [
                    internalLocationRepository.create(testInternalLocation),
                    internalLocationRepository.create(testInternalLocation),
                    internalLocationRepository.create(testInternalLocation)
                ])
                .then(() => expect(internalLocationRepository.findAll().then(internalLocations => internalLocations.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return internalLocationRepository.create(testInternalLocation).then(() => {
                const internalLocation = internalLocationRepository.findAll().then(internalLocations => internalLocations[0]);
                return Promise.all(
                    [
                        expect(internalLocation).to.eventually.have.property('id'),
                        expect(internalLocation).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(internalLocationRepository.findById(new ObjectID().toString())).to.be.rejectedWith('InternalLocation not found');
        });

        it('should resolve with found internalLocation', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    return expect(internalLocationRepository.findById(internalLocation.id.toString())).to.eventually.be.deep.equal(internalLocation);
                })
        });

        it('should not return array', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    return expect(internalLocationRepository.findById(internalLocation.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    const found = internalLocationRepository.findById(internalLocation.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(internalLocation.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid internalLocation', () => {
            return expect(internalLocationRepository.create(testInternalLocation)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => expect(internalLocation).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    expect(internalLocation).to.have.property('id');
                    expect(internalLocation).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testInternalLocation
                .id = new ObjectID();
            return expect(internalLocationRepository.update(testInternalLocation)).to.be.rejectedWith('InternalLocation not found');
        });

        it('should rename _id to id', () => {
            let updatedAddress: IAddress;
            updatedAddress = {
                country:  'updatedCountry',
                city:     'updatedCity',
                postcode: 'updatedPostcode',
                street:   'updatedStreet'
            };

            const updatedInternalLocation: IInternalLocation = {
                id:           new ObjectID(),
                capacity:     600,
                imagePaths:   [],
                defaultPrice: 6000,
                name:         'updatedLocation',
                latitude:     38.897096,
                longitude:    -77.036545,
                address:      updatedAddress,
                description:  'Guess who is living there now.',
                kind:         'internal'
            };
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    updatedInternalLocation.id = internalLocation.id;
                    const update = internalLocationRepository.update(updatedInternalLocation);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            let updatedAddress: IAddress;
            updatedAddress = {
                country:  'updatedCountry',
                city:     'updatedCity',
                postcode: 'updatedPostcode',
                street:   'updatedStreet'
            };

            const updatedInternalLocation: IInternalLocation = {
                id:           new ObjectID(),
                capacity:     600,
                imagePaths:   [],
                defaultPrice: 6000,
                name:         'updatedLocation',
                latitude:     38.897096,
                longitude:    -77.036545,
                address:      updatedAddress,
                description:  'Guess who is living there now. Yay!',
                kind:         'internal'
            };
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    updatedInternalLocation.id = internalLocation.id;
                    const update               = internalLocationRepository.update(updatedInternalLocation);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('capacity', 600),
                            expect(update).to.eventually.have.property('imagePaths').that.deep.equal([]),
                            expect(update).to.eventually.have.property('defaultPrice', 6000),
                            expect(update).to.eventually.have.property('name', 'updatedLocation'),
                            expect(update).to.eventually.have.property('latitude', 38.897096),
                            expect(update).to.eventually.have.property('longitude', -77.036545),
                            expect(update).to.eventually.have.property('address').that.deep.equal(updatedAddress),
                            expect(update).to.eventually.have.property('description', 'Guess who is living there now. Yay!'),
                            // expect(update).to.eventually.have.property('kind', 'internal'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testInternalLocation
                .id = new ObjectID();
            return expect(internalLocationRepository.remove(testInternalLocation)).to.be.rejectedWith('InternalLocation not found');
        });

        it('should resolve when found', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    const remove = internalLocationRepository.remove(internalLocation);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the internalLocation', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    return internalLocationRepository.remove(internalLocation)
                        .then(() => expect(internalLocationRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(internalLocationRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('InternalLocation not found');
        });

        it('should resolve when found', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    const remove = internalLocationRepository.removeById(internalLocation.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the internalLocation', () => {
            return internalLocationRepository.create(testInternalLocation)
                .then((internalLocation: IInternalLocation) => {
                    return internalLocationRepository.removeById(internalLocation.id.toString())
                        .then(() => expect(internalLocationRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});