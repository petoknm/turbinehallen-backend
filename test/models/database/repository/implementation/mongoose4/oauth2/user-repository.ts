import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { userRepository } from "../../../../../../../models/database/repository/implementation/mongoose4/oauth2/user-repository";
import { IUser } from "../../../../../../../models/interface/oauth2/user";
import { ObjectID } from "mongodb";
import { verifyPassword } from "../../../../../../../middleware/authentication";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('User Repository', () => {

    let testUser: IUser;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {

        testUser = {
            firstName: 'Mikael',
            lastName:  'Dogeson',
            email:     'mikey@doge.com',
            password:  'ihatecate123',
            roles:     []
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(userRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return userRepository.create(testUser)
                .then(() => expect(userRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(userRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no users when empty', () => {
            return expect(userRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all users when not empty', () => {
            return Promise.all(
                [
                    userRepository.create(testUser),
                    userRepository.create(testUser),
                    userRepository.create(testUser)
                ])
                .then(() => expect(userRepository.findAll().then(users => users.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return userRepository.create(testUser).then(() => {
                const user = userRepository.findAll().then(users => users[0]);
                return Promise.all(
                    [
                        expect(user).to.eventually.have.property('id'),
                        expect(user).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(userRepository.findById(new ObjectID().toString())).to.be.rejectedWith('User not found');
        });

        it('should resolve with found user', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    return expect(userRepository.findById(user.id.toString())).to.eventually.be.deep.equal(user);
                })
        });

        it('should not return array', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    return expect(userRepository.findById(user.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    const found = userRepository.findById(user.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(user.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid user', () => {
            return expect(userRepository.create(testUser)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => expect(user).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    expect(user).to.have.property('id');
                    expect(user).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testUser.id = new ObjectID();
            return expect(userRepository.update(testUser)).to.be.rejectedWith('User not found');
        });

        it('should rename _id to id', () => {
            const updatedUser: IUser = {
                firstName: 'Benny',
                lastName:  'Cateson',
                email:     'benny@cate.com',
                password:  'ihatedoge987',
                roles:     []
            };
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    updatedUser.id = user.id;
                    const update   = userRepository.update(updatedUser);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            const updatedUser: IUser = {
                firstName: 'Benny',
                lastName:  'Cateson',
                email:     'benny@cate.com',
                password:  'ihatedoge987',
                roles:     []
            };
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    updatedUser.id = user.id;
                    const update   = userRepository.update(updatedUser);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('firstName', 'Benny'),
                            expect(update).to.eventually.have.property('lastName', 'Cateson'),
                            expect(update).to.eventually.have.property('email', 'benny@cate.com'),
                            expect(update).to.eventually.have.property('password'),
                            expect(update.then(u => verifyPassword('ihatedoge987', u.password))).to.not.be.rejected
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testUser.id = new ObjectID();
            return expect(userRepository.remove(testUser)).to.be.rejectedWith('User not found');
        });

        it('should resolve when found', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    const remove = userRepository.remove(user);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the user', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    return userRepository.remove(user)
                        .then(() => expect(userRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(userRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('User not found');
        });

        it('should resolve when found', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    const remove = userRepository.removeById(user.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the user', () => {
            return userRepository.create(testUser)
                .then((user: IUser) => {
                    return userRepository.removeById(user.id.toString())
                        .then(() => expect(userRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});