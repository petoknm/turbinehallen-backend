import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { codeRepository } from "../../../../../../../models/database/repository/implementation/mongoose4/oauth2/code-repository";
import { ICode } from "../../../../../../../models/interface/oauth2/code";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Code Repository', () => {

    let testCode: ICode;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {

        testCode = {
            value:  'g45d64hr564dsr54h6es4r54hgre46',
            user:   new ObjectID(),
            client: new ObjectID()
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(codeRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return codeRepository.create(testCode)
                .then(() => expect(codeRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(codeRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no codes when empty', () => {
            return expect(codeRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all codes when not empty', () => {
            return Promise.all(
                [
                    codeRepository.create(testCode),
                    codeRepository.create(testCode),
                    codeRepository.create(testCode)
                ])
                .then(() => expect(codeRepository.findAll().then(codes => codes.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return codeRepository.create(testCode).then(() => {
                const code = codeRepository.findAll().then(codes => codes[0]);
                return Promise.all(
                    [
                        expect(code).to.eventually.have.property('id'),
                        expect(code).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(codeRepository.findById(new ObjectID().toString())).to.be.rejectedWith('Code not found');
        });

        it('should resolve with found code', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    return expect(codeRepository.findById(code.id.toString())).to.eventually.be.deep.equal(code);
                })
        });

        it('should not return array', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    return expect(codeRepository.findById(code.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    const found = codeRepository.findById(code.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(code.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid code', () => {
            return expect(codeRepository.create(testCode)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => expect(code).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    expect(code).to.have.property('id');
                    expect(code).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testCode.id = new ObjectID();
            return expect(codeRepository.update(testCode)).to.be.rejectedWith('Code not found');
        });

        it('should rename _id to id', () => {
            const updatedCode: ICode = {
                value:  'hge16hw5634tw56s43gw6',
                user:   new ObjectID(),
                client: new ObjectID()
            };
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    updatedCode.id = code.id;
                    const update   = codeRepository.update(updatedCode);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            const updatedCode: ICode = {
                value:  'ghhr44we6h4h5446ew',
                user:   new ObjectID(),
                client: new ObjectID()
            };
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    updatedCode.id = code.id;
                    const update   = codeRepository.update(updatedCode);
                    return expect(update).to.eventually.have.property('value', 'ghhr44we6h4h5446ew');
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testCode.id = new ObjectID();
            return expect(codeRepository.remove(testCode)).to.be.rejectedWith('Code not found');
        });

        it('should resolve when found', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    const remove = codeRepository.remove(code);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the code', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    return codeRepository.remove(code)
                        .then(() => expect(codeRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(codeRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('Code not found');
        });

        it('should resolve when found', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    const remove = codeRepository.removeById(code.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the code', () => {
            return codeRepository.create(testCode)
                .then((code: ICode) => {
                    return codeRepository.removeById(code.id.toString())
                        .then(() => expect(codeRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});