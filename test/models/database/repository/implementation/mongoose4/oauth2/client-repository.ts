import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { clientRepository } from "../../../../../../../models/database/repository/implementation/mongoose4/oauth2/client-repository";
import { IClient } from "../../../../../../../models/interface/oauth2/client";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Client Repository', () => {

    let testClient: IClient;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {

        testClient = {
            name:        'TestClient',
            redirectUri: 'http://example.com',
            secret:      'doge',
            user:        new ObjectID()
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(clientRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return clientRepository.create(testClient)
                .then(() => expect(clientRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(clientRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no clients when empty', () => {
            return expect(clientRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all clients when not empty', () => {
            return Promise.all(
                [
                    clientRepository.create(testClient),
                    clientRepository.create(testClient),
                    clientRepository.create(testClient)
                ])
                .then(() => expect(clientRepository.findAll().then(clients => clients.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return clientRepository.create(testClient).then(() => {
                const client = clientRepository.findAll().then(clients => clients[0]);
                return Promise.all(
                    [
                        expect(client).to.eventually.have.property('id'),
                        expect(client).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(clientRepository.findById(new ObjectID().toString())).to.be.rejectedWith('Client not found');
        });

        it('should resolve with found client', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    return expect(clientRepository.findById(client.id.toString())).to.eventually.be.deep.equal(client);
                })
        });

        it('should not return array', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    return expect(clientRepository.findById(client.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    const found = clientRepository.findById(client.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(client.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid client', () => {
            return expect(clientRepository.create(testClient)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => expect(client).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    expect(client).to.have.property('id');
                    expect(client).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testClient.id = new ObjectID();
            return expect(clientRepository.update(testClient)).to.be.rejectedWith('Client not found');
        });

        it('should rename _id to id', () => {
            const updatedClient: IClient = {
                name:        'TestClientUpdate',
                redirectUri: 'http://example.com/example',
                secret:      'doge-update',
                user:        new ObjectID()
            };
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    updatedClient.id = client.id;
                    const update     = clientRepository.update(updatedClient);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            const updatedClient: IClient = {
                name:        'TestClientUpdate',
                redirectUri: 'http://example.com/example',
                secret:      'doge-update',
                user:        new ObjectID()
            };
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    updatedClient.id = client.id;
                    const update     = clientRepository.update(updatedClient);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('name', 'TestClientUpdate'),
                            expect(update).to.eventually.have.property('redirectUri', 'http://example.com/example'),
                            expect(update).to.eventually.have.property('secret', 'doge-update'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testClient.id = new ObjectID();
            return expect(clientRepository.remove(testClient)).to.be.rejectedWith('Client not found');
        });

        it('should resolve when found', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    const remove = clientRepository.remove(client);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the client', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    return clientRepository.remove(client)
                        .then(() => expect(clientRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(clientRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('Client not found');
        });

        it('should resolve when found', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    const remove = clientRepository.removeById(client.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the client', () => {
            return clientRepository.create(testClient)
                .then((client: IClient) => {
                    return clientRepository.removeById(client.id.toString())
                        .then(() => expect(clientRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});