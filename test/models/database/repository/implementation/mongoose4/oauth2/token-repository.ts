import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { tokenRepository } from "../../../../../../../models/database/repository/implementation/mongoose4/oauth2/token-repository";
import { IToken } from "../../../../../../../models/interface/oauth2/token";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('Token Repository', () => {

    let testToken: IToken;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {

        testToken = {
            value:        'fgdgdggh45h1245gf5',
            refreshToken: 'wtq456h4gh4h56ewse',
            expiresIn:    new Date(2016, 11, 10),
            user:         new ObjectID(),
            client:       new ObjectID()
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(tokenRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return tokenRepository.create(testToken)
                .then(() => expect(tokenRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(tokenRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no tokens when empty', () => {
            return expect(tokenRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all tokens when not empty', () => {
            return Promise.all(
                [
                    tokenRepository.create(testToken),
                    tokenRepository.create(testToken),
                    tokenRepository.create(testToken)
                ])
                .then(() => expect(tokenRepository.findAll().then(tokens => tokens.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return tokenRepository.create(testToken).then(() => {
                const token = tokenRepository.findAll().then(tokens => tokens[0]);
                return Promise.all(
                    [
                        expect(token).to.eventually.have.property('id'),
                        expect(token).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(tokenRepository.findById(new ObjectID().toString())).to.be.rejectedWith('Token not found');
        });

        it('should resolve with found token', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    return expect(tokenRepository.findById(token.id.toString())).to.eventually.be.deep.equal(token);
                })
        });

        it('should not return array', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    return expect(tokenRepository.findById(token.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    const found = tokenRepository.findById(token.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(token.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid token', () => {
            return expect(tokenRepository.create(testToken)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => expect(token).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    expect(token).to.have.property('id');
                    expect(token).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testToken.id = new ObjectID();
            return expect(tokenRepository.update(testToken)).to.be.rejectedWith('Token not found');
        });

        it('should rename _id to id', () => {
            const updatedToken: IToken = {
                value:        'ewyur4h6bh6',
                refreshToken: 'ewy345gh6g4',
                expiresIn:    new Date(2016, 11, 15),
                user:         new ObjectID(),
                client:       new ObjectID()
            };
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    updatedToken.id = token.id;
                    const update    = tokenRepository.update(updatedToken);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            const updatedToken: IToken = {
                value:        'r5hjed64r5',
                refreshToken: 'ewyas4y56h',
                expiresIn:    new Date(2016, 11, 15),
                user:         new ObjectID(),
                client:       new ObjectID()
            };
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    updatedToken.id = token.id;
                    const update    = tokenRepository.update(updatedToken);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('value', 'r5hjed64r5'),
                            expect(update).to.eventually.have.property('refreshToken', 'ewyas4y56h'),
                            expect(update).to.eventually.have.property('expiresIn'),
                            expect(update.then(t => t.expiresIn.getTime())).to.eventually.equal(new Date(2016, 11, 15).getTime())
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testToken.id = new ObjectID();
            return expect(tokenRepository.remove(testToken)).to.be.rejectedWith('Token not found');
        });

        it('should resolve when found', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    const remove = tokenRepository.remove(token);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the token', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    return tokenRepository.remove(token)
                        .then(() => expect(tokenRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(tokenRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('Token not found');
        });

        it('should resolve when found', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    const remove = tokenRepository.removeById(token.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the token', () => {
            return tokenRepository.create(testToken)
                .then((token: IToken) => {
                    return tokenRepository.removeById(token.id.toString())
                        .then(() => expect(tokenRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});