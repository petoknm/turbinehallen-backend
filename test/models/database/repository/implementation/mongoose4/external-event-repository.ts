import chai = require("chai");
import chaiAsPromised = require("chai-as-promised");
import { externalEventRepository } from "../../../../../../models/database/repository/implementation/mongoose4/external-event-repository";
import { IExternalEvent } from "../../../../../../models/interface/external-event";
import { IInternalLocation } from "../../../../../../models/interface/internal-location";
import { IContactInfo } from "../../../../../../models/interface/contact-info";
import { IPrivateCustomer } from "../../../../../../models/interface/private-customer";
import { IRegularBooking } from "../../../../../../models/interface/regular-booking";
import { IExternalEventInformation } from "../../../../../../models/interface/external-event-information";
import { ObjectID } from "mongodb";
import mongoose = require('mongoose');
import mockgoose = require('mockgoose');
const expect = chai.expect;

chai.use(chaiAsPromised);

describe('External Event Repository', () => {
    let testExternalEvent: IExternalEvent;

    let testContactInfo: IContactInfo;
    let testInformation: IExternalEventInformation;
    let testAddress;

    before(() => {
        mongoose.Promise = Promise;
        const options    = {promiseLibrary: Promise};
        return mockgoose(mongoose)
            .then(() => mongoose.connect('mongodb://example.com/TestingDB', options));
    });

    after(() => {
        return mongoose.disconnect();
    });

    beforeEach((done) => {

        testContactInfo = {
            id:             new ObjectID(),
            firstName:      'testFirstName',
            lastName:       'testLastName',
            phoneNumbers:   ['88'],
            emailAddresses: ['someone.else@something.com']
        };

        testAddress = {
            country:  'testCountry',
            city:     'testCity',
            postcode: 'testPostcode',
            street:   'testStreet'
        };

        testExternalEvent = {
            //External Event
            startDate:   new Date(2016, 12, 20, 0, 0, 0),
            endDate:     new Date(2016, 12, 21, 0, 0, 0),
            totalPrice:  1000,
            contactInfo: [testContactInfo],

            //Event
            place:       new ObjectID(),
            description: 'Much fun',
            bookings:    [new ObjectID()],
            name:        'Harty Pard',
            type:        'external',
            information: testInformation,

            getTotalPrice: function () {
                return this.totalPrice;
            }
        };

        mockgoose.reset(() => done());
    });

    describe('#findAll', () => {

        it('should not reject when empty', () => {
            return expect(externalEventRepository.findAll()).to.eventually.be.ok;
        });

        it('should not reject when not empty', () => {
            return externalEventRepository.create(testExternalEvent)
                .then(() => expect(externalEventRepository.findAll()).to.eventually.be.ok);
        });

        it('should return array', () => {
            return expect(externalEventRepository.findAll()).to.eventually.be.instanceOf(Array);
        });

        it('should find no externalEvents when empty', () => {
            return expect(externalEventRepository.findAll()).to.eventually.deep.equal([]);
        });

        it('should find all externalEvents when not empty', () => {
            return Promise.all(
                [
                    externalEventRepository.create(testExternalEvent),
                    externalEventRepository.create(testExternalEvent),
                    externalEventRepository.create(testExternalEvent)
                ])
                .then(() => expect(externalEventRepository.findAll().then(externalEvents => externalEvents.length)).to.eventually.equal(3));
        });

        it('should rename _id to id', () => {
            return externalEventRepository.create(testExternalEvent).then(() => {
                const externalEvent = externalEventRepository.findAll().then(externalEvents => externalEvents[0]);
                return Promise.all(
                    [
                        expect(externalEvent).to.eventually.have.property('id'),
                        expect(externalEvent).to.eventually.not.have.property('_id')
                    ]);
            });
        });

    });

    describe('#findById', () => {

        it('should reject when not found', () => {
            return expect(externalEventRepository.findById(new ObjectID().toString())).to.be.rejectedWith('ExternalEvent not found');
        });

        it('should resolve with found externalEvent', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    return expect(externalEventRepository.findById(externalEvent.id.toString())).to.eventually.be.deep.equal(externalEvent);
                })
        });

        it('should not return array', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    return expect(externalEventRepository.findById(externalEvent.id.toString())).to.eventually.not.be.instanceOf(Array);
                });
        });

        it('should rename _id to id', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    const found = externalEventRepository.findById(externalEvent.id.toString());
                    return Promise.all(
                        [
                            expect(found).to.eventually.have.property('id'),
                            expect(found).to.eventually.not.have.property('_id'),
                            expect(found.then(c => c.id.equals(externalEvent.id))).to.eventually.be.true,
                        ]
                    )
                });
        });

    });

    describe('#create', () => {

        it('should resolve when creating valid externalEvent', () => {
            return expect(externalEventRepository.create(testExternalEvent)).to.eventually.be.ok;
        });

        it('should not return array', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => expect(externalEvent).to.not.be.instanceOf(Array));
        });

        it('should rename _id to id', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    expect(externalEvent).to.have.property('id');
                    expect(externalEvent).to.not.have.property('_id');
                });
        });

    });

    describe('#update', () => {

        it('should reject if not found', () => {
            testExternalEvent
                .id = new ObjectID();
            return expect(externalEventRepository.update(testExternalEvent)).to.be.rejectedWith('ExternalEvent not found');
        });

        it('should rename _id to id', () => {
            const updatedExternalEvent: IExternalEvent = {
                id:          new ObjectID(),
                startDate:   new Date(2016, 12, 20, 0, 0, 0),
                endDate:     new Date(2016, 12, 21, 0, 0, 0),
                totalPrice:  2000,
                contactInfo: [testContactInfo],
                place:       new ObjectID(),
                description: 'Much much fun',
                bookings:    [new ObjectID()],
                name:        'Harty Pard v2',
                type:        'external',
                information: testInformation,

                getTotalPrice: function () {
                    return this.totalPrice;
                }
            };
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    updatedExternalEvent.id = externalEvent.id;
                    const update            = externalEventRepository.update(updatedExternalEvent);
                    return Promise.all(
                        [
                            expect(update).to.eventually.have.property('id'),
                            expect(update).to.eventually.not.have.property('_id')
                        ]);
                });
        });

        it('should perform update on all the fields', () => {
            let updatedContactInfo = {
                id:             new ObjectID(),
                firstName:      'updatedFirstName',
                lastName:       'updatedLastName',
                phoneNumbers:   ['99'],
                emailAddresses: ['updated@something.com']
            };

            const updatedExternalEvent: IExternalEvent = {
                id:          new ObjectID(),
                startDate:   new Date(2016, 12, 20, 0, 0, 0),
                endDate:     new Date(2016, 12, 21, 0, 0, 0),
                totalPrice:  2000,
                contactInfo: [updatedContactInfo],
                place:       new ObjectID(),
                description: 'Much much fun',
                bookings:    [new ObjectID()],
                name:        'Harty Pard v2',
                type:        'external',
                information: testInformation,

                getTotalPrice: function () {
                    return this.totalPrice;
                }
            };
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    updatedExternalEvent.id = externalEvent.id;
                    const update            = externalEventRepository.update(updatedExternalEvent);
                    return Promise.all(
                        [
                            expect(update.then(u => u.startDate.getTime())).to.eventually.equal(new Date(2016, 12, 20, 0, 0, 0).getTime()),
                            expect(update.then(u => u.endDate.getTime())).to.eventually.equal(new Date(2016, 12, 21, 0, 0, 0).getTime()),
                            expect(update).to.eventually.have.property('totalPrice', 2000),

                            expect(update).to.eventually.have.property('description', 'Much much fun'),
                            expect(update).to.eventually.have.property('name', 'Harty Pard v2'),

                            expect(update).to.eventually.have.property('contactInfo').that.deep.equal([updatedContactInfo]),
                            expect(update).to.eventually.have.property('bookings').that.deep.equal(updatedExternalEvent.bookings),
                            expect(update).to.eventually.have.property('place').that.deep.equal(updatedExternalEvent.place),
                            expect(update).to.eventually.have.property('type', 'external'),
                        ]);
                });
        });

    });

    describe('#remove', () => {

        it('should reject if not found', () => {
            testExternalEvent
                .id = new ObjectID();
            return expect(externalEventRepository.remove(testExternalEvent)).to.be.rejectedWith('ExternalEvent not found');
        });

        it('should resolve when found', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    const remove = externalEventRepository.remove(externalEvent);
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the externalEvent', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    return externalEventRepository.remove(externalEvent)
                        .then(() => expect(externalEventRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

    describe('#removeById', () => {

        it('should reject if not found', () => {
            return expect(externalEventRepository.removeById(new ObjectID().toString())).to.be.rejectedWith('ExternalEvent not found');
        });

        it('should resolve when found', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    const remove = externalEventRepository.removeById(externalEvent.id.toString());
                    return Promise.all(
                        [
                            expect(remove).to.eventually.be.null,
                            expect(remove).to.not.be.rejected
                        ]);
                });
        });

        it('should remove the externalEvent', () => {
            return externalEventRepository.create(testExternalEvent)
                .then((externalEvent: IExternalEvent) => {
                    return externalEventRepository.removeById(externalEvent.id.toString())
                        .then(() => expect(externalEventRepository.findAll()).to.eventually.deep.equal([]));
                })
        });

    });

});