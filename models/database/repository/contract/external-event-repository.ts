import { IRepository } from "./repository";
import { IExternalEvent } from "../../../interface/external-event";

export interface IExternalEventRepository extends IRepository<IExternalEvent> {

}