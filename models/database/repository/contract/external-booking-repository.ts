import { IRepository } from "./repository";
import { IExternalBooking } from "../../../interface/external-booking";

export interface IExternalBookingRepository extends IRepository<IExternalBooking> {

}