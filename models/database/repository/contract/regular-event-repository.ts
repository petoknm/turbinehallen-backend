import { IRepository } from "./repository";
import { IRegularEvent } from "../../../interface/regular-event";

export interface IRegularEventRepository extends IRepository<IRegularEvent> {

}