import { IRepository } from "./repository";
import { IInternalLocation } from "../../../interface/internal-location";

export interface IInternalLocationRepository extends IRepository<IInternalLocation> {

}