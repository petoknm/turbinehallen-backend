import { IRepository } from "./repository";
import { IRegularBooking } from "../../../interface/regular-booking";

export interface IRegularBookingRepository extends IRepository<IRegularBooking> {

}