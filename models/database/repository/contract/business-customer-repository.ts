import { IRepository } from "./repository";
import { IBusinessCustomer } from "../../../interface/business-customer";

export interface IBusinessCustomerRepository extends IRepository<IBusinessCustomer> {

}