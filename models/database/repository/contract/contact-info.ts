import { IRepository } from "./repository";
import { IContactInfo } from "../../../interface/contact-info";

export interface IContactInfoRepository extends IRepository<IContactInfo>{

}