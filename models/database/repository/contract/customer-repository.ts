import { IRepository } from "./repository";
import { ICustomer } from "../../../interface/customer";

export interface ICustomerRepository extends IRepository<ICustomer> {

}