import { IRepository } from "./repository";
import { IPrivateCustomer } from "../../../interface/private-customer";

export interface IPrivateCustomerRepository extends IRepository<IPrivateCustomer> {

}