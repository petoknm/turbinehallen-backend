import { IRepository } from "./repository";
import { IChristmasEvent } from "../../../interface/christmas-event";

export interface IChristmasEventRepository extends IRepository<IChristmasEvent> {

}