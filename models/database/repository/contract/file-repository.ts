import { Writable, Readable } from "stream";

export interface IFileRepository {

    write(name: string): Writable;

    readByName(name: string): Readable;
    readById(id: string): Readable;

    removeByName(name: string): Promise<void>;
    removeById(id: string): Promise<void>;

}