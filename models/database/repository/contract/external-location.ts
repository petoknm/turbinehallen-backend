import { IRepository } from "./repository";
import { IExternalLocation } from "../../../interface/external-location";

export interface IExternalLocationRepository extends IRepository<IExternalLocation> {

}