import { IRepository } from "./repository";
import { ILocation } from "../../../interface/location";

export interface ILocationRepository extends IRepository<ILocation> {

}