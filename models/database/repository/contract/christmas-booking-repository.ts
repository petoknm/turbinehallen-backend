import { IRepository } from "./repository";
import { IChristmasBooking } from "../../../interface/christmas-booking";

export interface IChristmasBookingRepository extends IRepository<IChristmasBooking> {

}