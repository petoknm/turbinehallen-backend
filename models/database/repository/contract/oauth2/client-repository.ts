import { IRepository } from "../repository";
import { IClient } from "../../../../interface/oauth2/client";

export interface IClientRepository extends IRepository<IClient> {

}