import { IRepository } from "../repository";
import { IUser } from "../../../../interface/oauth2/user";

export interface IUserRepository extends IRepository<IUser> {

    findByEmail(email: string): Promise<IUser|void>;

}