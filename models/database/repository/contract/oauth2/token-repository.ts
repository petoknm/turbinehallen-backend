import { IRepository } from "../repository";
import { IToken } from "../../../../interface/oauth2/token";

export interface ITokenRepository extends IRepository<IToken> {

    findByValue(value: string): Promise<IToken|void>;

    findByRefreshToken(refreshToken: string): Promise<IToken|void>;

}