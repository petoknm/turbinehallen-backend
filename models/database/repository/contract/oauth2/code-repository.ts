import { IRepository } from "../repository";
import { ICode } from "../../../../interface/oauth2/code";

export interface ICodeRepository extends IRepository<ICode> {

    findByValue(value: string): Promise<ICode|void>;

    findByClientAndUser(clientId: string, userId: string): Promise<ICode|void>;

}