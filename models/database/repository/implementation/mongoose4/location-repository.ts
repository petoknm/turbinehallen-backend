import { ILocationRepository } from "../../contract/location-repository";
import { ILocation } from "../../../../interface/location";
import mongoose = require('mongoose');

export interface ILocationModel extends ILocation, mongoose.Document {
}

const locationSchema = new mongoose.Schema({
    name:        String,
    latitude:    Number,
    longitude:   Number,
    address:     mongoose.SchemaTypes.Mixed,
    description: String,
    type:        String
});

const LocationModel = mongoose.model<ILocationModel>('Location', locationSchema);

export class LocationRepository implements ILocationRepository {

    create(document: ILocation): Promise<ILocation> {
        return LocationModel.create(document);
    }

    update(document: ILocation): Promise<ILocation> {
        return LocationModel.findByIdAndUpdate(document.id, document).exec();
    }

    updateById(id: string, document: ILocation): Promise<ILocation>{
        return LocationModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: ILocation): Promise<void> {
        return LocationModel.findByIdAndRemove(document.id).exec()
            .then(() => Promise.resolve(null));
    }

    removeById(id: string): Promise<void> {
        return LocationModel.findByIdAndRemove(id).exec()
            .then(() => Promise.resolve(null));
    }

    findAll(): Promise<ILocation[]> {
        return LocationModel.find().exec();
    }

    findById(id: string): Promise<null|ILocation> {
        return LocationModel.findById(id).exec();
    }

}

export const locationRepository = new LocationRepository();