import mongoose = require("mongoose");
import { IRegularEvent } from "../../../../interface/regular-event";
import { IRegularEventRepository } from "../../contract/regular-event-repository";
import { rejectIfNull, toObject, toObjectAll, renameIdAll, renameId } from "./helpers";

export interface IRegularEventModel extends IRegularEvent, mongoose.Document {
}

const regularEventSchema = new mongoose.Schema({
    place:       mongoose.Schema.Types.ObjectId,// ILocation
    description: String,
    bookings:    [mongoose.Schema.Types.ObjectId],// IRegularBooking
    name:        String,
    type:        String,
    startDate:   Date,
    endDate:     Date,
    totalPrice:  Number,
    contactInfo: [mongoose.Schema.Types.Mixed],// IContactInfo
    information: mongoose.Schema.Types.Mixed
});

export const RegularEventModel = mongoose.model<IRegularEventModel>('RegularEvent', regularEventSchema);

export class RegularEventRepository implements IRegularEventRepository {

    create(document: IRegularEvent): Promise<IRegularEvent> {
        return RegularEventModel.create(document)
            .then(rejectIfNull('RegularEvent not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IRegularEvent): Promise<IRegularEvent> {
        return RegularEventModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('RegularEvent not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IRegularEvent): Promise<IRegularEvent>{
        return RegularEventModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: IRegularEvent): Promise<void> {
        return RegularEventModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('RegularEvent not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return RegularEventModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('RegularEvent not found'))
            .then(() => null);
    }

    findAll(): Promise<IRegularEvent[]> {
        return RegularEventModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IRegularEvent> {
        return RegularEventModel.findById(id).exec()
            .then(rejectIfNull('RegularEvent not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const regularEventRepository = new RegularEventRepository();