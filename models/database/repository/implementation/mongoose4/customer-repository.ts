import { ICustomerRepository } from "../../contract/customer-repository";
import { ICustomer } from "../../../../interface/customer";
import mongoose = require('mongoose');

export interface ICustomerModel extends ICustomer, mongoose.Document {
}

const customerSchema = new mongoose.Schema({
    // TODO
});

const CustomerModel = mongoose.model<ICustomerModel>('Customer', customerSchema);

export class CustomerRepository implements ICustomerRepository {

    create(document: ICustomer): Promise<ICustomer> {
        return CustomerModel.create(document);
    }

    update(document: ICustomer): Promise<ICustomer> {
        return CustomerModel.findByIdAndUpdate(document.id, document).exec();
    }

    updateById(id: string, document: ICustomer): Promise<ICustomer>{
        return CustomerModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: ICustomer): Promise<void> {
        return CustomerModel.findByIdAndRemove(document.id).exec()
            .then(() => Promise.resolve(null));
    }

    removeById(id: string): Promise<void> {
        return CustomerModel.findByIdAndRemove(id).exec()
            .then(() => Promise.resolve(null));
    }

    findAll(): Promise<ICustomer[]> {
        return CustomerModel.find().exec();
    }

    findById(id: string): Promise<null|ICustomer> {
        return CustomerModel.findById(id).exec();
    }

}

export const customerRepository = new CustomerRepository();