import { IPrivateCustomerRepository } from "../../contract/private-customer-repository";
import { IPrivateCustomer } from "../../../../interface/private-customer";
import { rejectIfNull, toObject, renameId, toObjectAll, renameIdAll } from "./helpers";
import mongoose = require('mongoose');

export interface IPrivateCustomerModel extends IPrivateCustomer, mongoose.Document {
}

const privateCustomerSchema = new mongoose.Schema({
    firstName:      String,
    lastName:       String,
    kind:           String,
    phoneNumbers:   [String],
    addresses:      [mongoose.Schema.Types.Mixed],// IAddress
    emailAddresses: [String]
});

const PrivateCustomerModel = mongoose.model<IPrivateCustomerModel>('PrivateCustomer', privateCustomerSchema);

export class PrivateCustomerRepository implements IPrivateCustomerRepository {

    create(document: IPrivateCustomer): Promise<IPrivateCustomer> {
        return PrivateCustomerModel.create(document)
            .then(rejectIfNull('PrivateCustomer not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IPrivateCustomer): Promise<IPrivateCustomer> {
        return PrivateCustomerModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('PrivateCustomer not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IPrivateCustomer): Promise<IPrivateCustomer>{
        return PrivateCustomerModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: IPrivateCustomer): Promise<void> {
        return PrivateCustomerModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('PrivateCustomer not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return PrivateCustomerModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('PrivateCustomer not found'))
            .then(() => null);
    }

    findAll(): Promise<IPrivateCustomer[]> {
        return PrivateCustomerModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IPrivateCustomer> {
        return PrivateCustomerModel.findById(id).exec()
            .then(rejectIfNull('PrivateCustomer not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const privateCustomerRepository = new PrivateCustomerRepository();