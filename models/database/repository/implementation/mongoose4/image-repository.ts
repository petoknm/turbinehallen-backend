import { ObjectID } from "mongodb";
import { IImageRepository } from "../../contract/image-repository";
import { Readable, Writable } from "stream";
import mongoose = require('mongoose');

const mongo: any   = mongoose.mongo;
const GridFSBucket = mongo.GridFSBucket;

class ImageRepository implements IImageRepository {

    private bucket: any;
    private options = {
        bucketName: 'images'
    };

    constructor() {
        let connected;
        if (mongoose.connection.readyState === 1) { // readyState(1) := connected
            connected = Promise.resolve(mongoose.connection.db);
        } else {
            connected = new Promise((resolve, reject) =>
                mongoose.connection.once('open', () => resolve(mongoose.connection.db)));
        }
        connected.then(db => this.bucket = new GridFSBucket(db, this.options))
    }

    write(name: string): Writable {
        return this.bucket.openUploadStream(name);
    }

    readByName(name: string): Readable {
        return this.bucket.openDownloadStreamByName(name);
    }

    readById(id: string): Readable {
        return this.bucket.openDownloadStream(new ObjectID(id));
    }

    removeByName(name: string): Promise<void> {
        return this.bucket.find({filename: name}).toArray()
            .then(docs => {
                if (docs.length === 0) {
                    throw new Error('Image not found');
                } else {
                    return docs[0];
                }
            })
            .then(doc => this.removeById(doc._id.toString()));
    }

    removeById(id: string): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            this.bucket.delete(new ObjectID(id))
                .then(() => resolve())
                .catch(() => reject('Image not found'));
        });
    }

}

export const imageRepository = new ImageRepository();