import mongoose = require("mongoose");
import { IExternalBooking } from "../../../../interface/external-booking";
import { IExternalBookingRepository } from "../../contract/external-booking-repository";
import { rejectIfNull, toObject, renameId, toObjectAll, renameIdAll } from "./helpers";

export interface IExternalBookingModel extends IExternalBooking, mongoose.Document {
}

const externalBookingSchema = new mongoose.Schema({
    customer:    mongoose.Schema.Types.ObjectId,// ICustomer
    coordinator: mongoose.Schema.Types.ObjectId,
    information: mongoose.Schema.Types.Mixed,
    price:       Number,
    payment:     String
});

export const ExternalBookingModel = mongoose.model<IExternalBookingModel>('ExternalBooking', externalBookingSchema);

export class ExternalBookingRepository implements IExternalBookingRepository {

    create(document: IExternalBooking): Promise<IExternalBooking> {
        return ExternalBookingModel.create(document)
            .then(rejectIfNull('ExternalBooking not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IExternalBooking): Promise<IExternalBooking> {
        return ExternalBookingModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('ExternalBooking not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IExternalBooking): Promise<IExternalBooking>{
        return ExternalBookingModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: IExternalBooking): Promise<void> {
        return ExternalBookingModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('ExternalBooking not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return ExternalBookingModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('ExternalBooking not found'))
            .then(() => null);
    }

    findAll(): Promise<IExternalBooking[]> {
        return ExternalBookingModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IExternalBooking> {
        return ExternalBookingModel.findById(id).exec()
            .then(rejectIfNull('ExternalBooking not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const externalBookingRepository = new ExternalBookingRepository();