import mongoose = require("mongoose");
import { IExternalLocation } from "../../../../interface/external-location";
import { IExternalLocationRepository } from "../../contract/external-location";
import { rejectIfNull, toObject, renameId, toObjectAll, renameIdAll } from "./helpers";

export interface IExternalLocationModel extends IExternalLocation,mongoose.Document {

}

const externalLocationSchema = new mongoose.Schema({
    name:        String,
    latitude:    Number,
    longitude:   Number,
    address:     mongoose.Schema.Types.Mixed,// IAddress
    description: String
});

export const ExternalLocationModel = mongoose.model<IExternalLocationModel>('ExternalLocation', externalLocationSchema);

export class ExternalLocationRepository implements IExternalLocationRepository {

    create(document: IExternalLocation): Promise<IExternalLocation> {
        return ExternalLocationModel.create(document)
            .then(rejectIfNull('ExternalLocation not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IExternalLocation): Promise<IExternalLocation> {
        return ExternalLocationModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('ExternalLocation not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IExternalLocation): Promise<IExternalLocation>{
        return ExternalLocationModel.findByIdAndUpdate(id, document).exec()
            .then(rejectIfNull('ExternalLocation not found'))
            .then(toObject)
            .then(renameId);
    }

    remove(document: IExternalLocation): Promise<void> {
        return ExternalLocationModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('ExternalLocation not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return ExternalLocationModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('ExternalLocation not found'))
            .then(() => null);
    }

    findAll(): Promise<IExternalLocation[]> {
        return ExternalLocationModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IExternalLocation> {
        return ExternalLocationModel.findById(id).exec()
            .then(rejectIfNull('ExternalLocation not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const externalLocationRepository = new ExternalLocationRepository();