import { IChristmasBookingRepository } from "../../contract/christmas-booking-repository";
import { IChristmasBooking } from "../../../../interface/christmas-booking";
import mongoose = require('mongoose');
import { rejectIfNull, toObject, renameId, toObjectAll, renameIdAll } from "./helpers";

export interface IChristmasBookingModel extends IChristmasBooking, mongoose.Document {
}

const christmasBookingSchema = new mongoose.Schema({
    customer:    mongoose.Schema.Types.ObjectId,// ICustomer
    coordinator: mongoose.Schema.Types.ObjectId,
    information: mongoose.Schema.Types.Mixed,
    price:       Number,
    payment:     String,
    contactInfo: [mongoose.Schema.Types.Mixed]// IContactInfo
});

const ChristmasBookingModel = mongoose.model<IChristmasBookingModel>('ChristmasBooking', christmasBookingSchema);

export class ChristmasBookingRepository implements IChristmasBookingRepository {

    create(document: IChristmasBooking): Promise<IChristmasBooking> {
        return ChristmasBookingModel.create(document)
            .then(rejectIfNull('ChristmasBooking not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IChristmasBooking): Promise<IChristmasBooking> {
        return ChristmasBookingModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('ChristmasBooking not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IChristmasBooking): Promise<IChristmasBooking>{
        return ChristmasBookingModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: IChristmasBooking): Promise<void> {
        return ChristmasBookingModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('ChristmasBooking not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return ChristmasBookingModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('ChristmasBooking not found'))
            .then(() => null);
    }

    findAll(): Promise<IChristmasBooking[]> {
        return ChristmasBookingModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IChristmasBooking> {
        return ChristmasBookingModel.findById(id).exec()
            .then(rejectIfNull('ChristmasBooking not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const christmasBookingRepository = new ChristmasBookingRepository();