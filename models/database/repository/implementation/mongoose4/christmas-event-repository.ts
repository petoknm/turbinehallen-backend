import mongoose = require("mongoose");
import { IChristmasEvent } from "../../../../interface/christmas-event";
import { IChristmasEventRepository } from "../../contract/christmas-event-repository";
import { rejectIfNull, toObject, renameId, toObjectAll, renameIdAll } from "./helpers";

export interface IChristmasEventModel extends IChristmasEvent, mongoose.Document {
}

const christmasEventSchema = new mongoose.Schema({
    place:        mongoose.Schema.Types.ObjectId,// IInternalLocation
    description:  String,
    bookings:     [mongoose.Schema.Types.ObjectId],// IChristmasBooking
    name:         String,
    type:         String,
    date:         Date,
    pricePerSeat: Number,
    information: mongoose.Schema.Types.Mixed
});

export const ChristmasEventModel = mongoose.model<IChristmasEventModel>('ChristmasEvent', christmasEventSchema);

export class ChristmasEventRepository implements IChristmasEventRepository {

    create(document: IChristmasEvent): Promise<IChristmasEvent> {
        return ChristmasEventModel.create(document)
            .then(rejectIfNull('ChristmasEvent not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IChristmasEvent): Promise<IChristmasEvent> {
        return ChristmasEventModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('ChristmasEvent not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IChristmasEvent): Promise<IChristmasEvent>{
        return ChristmasEventModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: IChristmasEvent): Promise<void> {
        return ChristmasEventModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('ChristmasEvent not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return ChristmasEventModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('ChristmasEvent not found'))
            .then(() => null);
    }

    findAll(): Promise<IChristmasEvent[]> {
        return ChristmasEventModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IChristmasEvent> {
        return ChristmasEventModel.findById(id).exec()
            .then(rejectIfNull('ChristmasEvent not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const christmasEventRepository = new ChristmasEventRepository();