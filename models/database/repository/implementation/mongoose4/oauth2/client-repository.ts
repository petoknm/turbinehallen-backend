import { IClientRepository } from "../../../contract/oauth2/client-repository";
import { IClient } from "../../../../../interface/oauth2/client";
import { renameId, renameIdAll, toObjectAll, toObject, rejectIfNull } from "../helpers";
import mongoose = require('mongoose');

export interface IClientModel extends IClient, mongoose.Document {
}

const clientSchema = new mongoose.Schema({
    name:        String,
    secret:      String,
    redirectUri: String,
    user:        mongoose.SchemaTypes.ObjectId
});

const ClientModel = mongoose.model<IClientModel>('Client', clientSchema);

class ClientRepository implements IClientRepository {

    findAll(): Promise<IClient[]> {
        return ClientModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<any|IClient> {
        return ClientModel.findById(id).exec()
            .then(rejectIfNull('Client not found'))
            .then(toObject)
            .then(renameId);
    }

    create(document: IClient): Promise<IClient> {
        return ClientModel.create(document)
            .then(toObject)
            .then(renameId);
    }

    update(document: IClient): Promise<IClient> {
        return ClientModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('Client not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IClient): Promise<IClient> {
        return ClientModel.findByIdAndUpdate(id, document, {'new': true}).exec()
            .then(rejectIfNull('Client not found'))
            .then(toObject)
            .then(renameId);
    }

    remove(document: IClient): Promise<void> {
        return ClientModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('Client not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return ClientModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('Client not found'))
            .then(() => null);
    }

}

export const clientRepository = new ClientRepository();