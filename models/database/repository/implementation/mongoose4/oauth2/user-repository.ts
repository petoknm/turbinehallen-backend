import { IUserRepository } from "../../../contract/oauth2/user-repository";
import { IUser } from "../../../../../interface/oauth2/user";
import { toObjectAll, renameIdAll, rejectIfNull, toObject, renameId } from "../helpers";
import { saltRounds } from "../../../../../../config/bcrypt";
import mongoose = require('mongoose');
import bcrypt = require('bcryptjs');

export interface IUserModel extends IUser, mongoose.Document {
}

const userSchema = new mongoose.Schema({
    firstName: String,
    lastName:  String,
    email:     String,
    password:  String,
    roles:     [String]
});

function hashIfNeeded(document: IUser): Promise<IUser> {
    return new Promise((resolve, reject) => {
        // if the password is not a hash
        if (!/^\$2[ayb]\$.{56}$/.test(document.password)) {
            bcrypt.hash(document.password, saltRounds, (err, hash) => {
                if (err) return reject(err);

                document.password = hash;
                resolve(document);
            });
        } else {
            resolve(document);
        }
    });
}

// Encrypt user's password before saving it to the database
userSchema.pre('save', function (callback) {
    const user: IUserModel = this;
    hashIfNeeded(user)
        .then(user => callback())
        .catch(err => callback(err));
});

const UserModel = mongoose.model<IUserModel>('User', userSchema);

class UserRepository implements IUserRepository {

    findAll(): Promise<IUser[]> {
        return UserModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<any|IUser> {
        return UserModel.findById(id).exec()
            .then(rejectIfNull('User not found'))
            .then(toObject)
            .then(renameId);
    }

    findByEmail(email: string): Promise<any|IUser> {
        return UserModel.findOne({email}).exec();
    }

    create(document: IUser): Promise<IUser> {
        return UserModel.create(document)
            .then(toObject)
            .then(renameId);
    }

    update(document: IUser): Promise<IUser> {
        return hashIfNeeded(document)
            .then(document => UserModel.findByIdAndUpdate(document.id, document, {'new': true}).exec())
            .then(rejectIfNull('User not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IUser): Promise<IUser> {
        return UserModel.findByIdAndUpdate(id, document, {'new': true}).exec()
            .then(rejectIfNull('User not found'))
            .then(toObject)
            .then(renameId);
    }

    remove(document: IUser): Promise<void> {
        return UserModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('User not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return UserModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('User not found'))
            .then(() => null);
    }

}

export const userRepository = new UserRepository();