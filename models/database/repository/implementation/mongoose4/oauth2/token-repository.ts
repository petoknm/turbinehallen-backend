import { ITokenRepository } from "../../../contract/oauth2/token-repository";
import { IToken } from "../../../../../interface/oauth2/token";
import { toObjectAll, renameIdAll, rejectIfNull, toObject, renameId } from "../helpers";
import mongoose = require('mongoose');

export interface ITokenModel extends IToken, mongoose.Document {
}

const tokenSchema = new mongoose.Schema({
    value:        String,
    refreshToken: String,
    expiresIn:    Date,
    user:         mongoose.SchemaTypes.ObjectId,
    client:       mongoose.SchemaTypes.ObjectId
});

const TokenModel = mongoose.model<ITokenModel>('Token', tokenSchema);

class TokenRepository implements ITokenRepository {

    findAll(): Promise<IToken[]> {
        return TokenModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<any|IToken> {
        return TokenModel.findById(id).exec()
            .then(rejectIfNull('Token not found'))
            .then(toObject)
            .then(renameId);
    }

    findByValue(value: string): Promise<IToken|any> {
        return TokenModel.findOne({value}).exec()
            .then(rejectIfNull('Token not found'))
            .then(toObject)
            .then(renameId);
    }

    findByRefreshToken(refreshToken: string): Promise<IToken|any> {
        return TokenModel.findOne({refreshToken}).exec()
            .then(rejectIfNull('Token not found'))
            .then(toObject)
            .then(renameId);
    }

    create(document: IToken): Promise<IToken> {
        return TokenModel.create(document)
            .then(toObject)
            .then(renameId);
    }

    update(document: IToken): Promise<IToken> {
        return TokenModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('Token not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IToken): Promise<IToken> {
        return TokenModel.findByIdAndUpdate(id, document, {'new': true}).exec()
            .then(rejectIfNull('Token not found'))
            .then(toObject)
            .then(renameId);
    }

    remove(document: IToken): Promise<void> {
        return TokenModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('Token not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return TokenModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('Token not found'))
            .then(() => null);
    }

}

export const tokenRepository = new TokenRepository();