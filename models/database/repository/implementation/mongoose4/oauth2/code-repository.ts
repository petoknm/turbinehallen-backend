import { ICodeRepository } from "../../../contract/oauth2/code-repository";
import { ICode } from "../../../../../interface/oauth2/code";
import { toObjectAll, renameIdAll, rejectIfNull, toObject, renameId } from "../helpers";
import mongoose = require('mongoose');

export interface ICodeModel extends ICode, mongoose.Document {
}

const codeSchema = new mongoose.Schema({
    value:  String,
    user:   mongoose.SchemaTypes.ObjectId,
    client: mongoose.SchemaTypes.ObjectId
});

const CodeModel = mongoose.model<ICodeModel>('Code', codeSchema);

class CodeRepository implements ICodeRepository {

    findAll(): Promise<ICode[]> {
        return CodeModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<any|ICode> {
        return CodeModel.findById(id).exec()
            .then(rejectIfNull('Code not found'))
            .then(toObject)
            .then(renameId);
    }

    findByValue(value: string): Promise<any|ICode> {
        return CodeModel.findOne({value}).exec();
    }

    findByClientAndUser(clientId: string, userId: string): Promise<any|ICode> {
        return CodeModel.findOne({client: clientId, user: userId}).exec();
    }

    create(document: ICode): Promise<ICode> {
        return CodeModel.create(document)
            .then(toObject)
            .then(renameId);
    }

    update(document: ICode): Promise<ICode> {
        return CodeModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('Code not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: ICode): Promise<ICode> {
        return CodeModel.findByIdAndUpdate(id, document, {'new': true}).exec()
            .then(rejectIfNull('Code not found'))
            .then(toObject)
            .then(renameId);
    }

    remove(document: ICode): Promise<void> {
        return CodeModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('Code not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return CodeModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('Code not found'))
            .then(() => null);
    }

}

export const codeRepository = new CodeRepository();