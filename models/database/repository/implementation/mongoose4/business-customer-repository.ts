import { IBusinessCustomerRepository } from "../../contract/business-customer-repository";
import { IBusinessCustomer } from "../../../../interface/business-customer";
import { toObject, renameId, toObjectAll, renameIdAll, rejectIfNull } from "./helpers";
import mongoose = require('mongoose');

export interface IBusinessCustomerModel extends IBusinessCustomer, mongoose.Document {
}

const businessCustomerSchema = new mongoose.Schema({
    name:           String,
    cvr:            String,
    addresses:      [mongoose.Schema.Types.Mixed],// IAddress
    emailAddresses: [String]
});

const BusinessCustomerModel = mongoose.model<IBusinessCustomerModel>('BusinessCustomer', businessCustomerSchema);

export class BusinessCustomerRepository implements IBusinessCustomerRepository {

    create(document: IBusinessCustomer): Promise<IBusinessCustomer> {
        return BusinessCustomerModel.create(document)
            .then(toObject)
            .then(renameId);
    }

    update(document: IBusinessCustomer): Promise<IBusinessCustomer> {
        return BusinessCustomerModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('BusinessCustomer not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IBusinessCustomer): Promise<IBusinessCustomer>{
        return BusinessCustomerModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: IBusinessCustomer): Promise<void> {
        return BusinessCustomerModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('BusinessCustomer not found'))
            .then(() => Promise.resolve(null));
    }

    removeById(id: string): Promise<void> {
        return BusinessCustomerModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('BusinessCustomer not found'))
            .then(() => Promise.resolve(null));
    }

    findAll(): Promise<IBusinessCustomer[]> {
        return BusinessCustomerModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IBusinessCustomer> {
        return BusinessCustomerModel.findById(id).exec()
            .then(rejectIfNull('BusinessCustomer not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const businessCustomerRepository = new BusinessCustomerRepository();