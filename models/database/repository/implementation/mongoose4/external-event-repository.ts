import mongoose = require("mongoose");
import { IExternalEvent } from "../../../../interface/external-event";
import { IExternalEventRepository } from "../../contract/external-event-repository";
import { rejectIfNull, toObject, renameId, toObjectAll, renameIdAll } from "./helpers";

export interface IExternalEventModel extends IExternalEvent, mongoose.Document {
}

const externalEventSchema = new mongoose.Schema({
    place:       mongoose.Schema.Types.ObjectId,// IExternalLocation
    description: String,
    bookings:    [mongoose.Schema.Types.ObjectId],// IExternalBooking
    name:        String,
    type:        String,
    startDate:   Date,
    endDate:     Date,
    totalPrice:  Number,
    contactInfo: [mongoose.Schema.Types.Mixed],// IContactInfo
    information: mongoose.Schema.Types.Mixed
});

export const ExternalEventModel = mongoose.model<IExternalEventModel>('ExternalEvent', externalEventSchema);

export class ExternalEventRepository implements IExternalEventRepository {

    create(document: IExternalEvent): Promise<IExternalEvent> {
        return ExternalEventModel.create(document)
            .then(rejectIfNull('ExternalEvent not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IExternalEvent): Promise<IExternalEvent> {
        return ExternalEventModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('ExternalEvent not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IExternalEvent): Promise<IExternalEvent>{
        return ExternalEventModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: IExternalEvent): Promise<void> {
        return ExternalEventModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('ExternalEvent not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return ExternalEventModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('ExternalEvent not found'))
            .then(() => null);
    }

    findAll(): Promise<IExternalEvent[]> {
        return ExternalEventModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IExternalEvent> {
        return ExternalEventModel.findById(id).exec()
            .then(rejectIfNull('ExternalEvent not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const externalEventRepository = new ExternalEventRepository();