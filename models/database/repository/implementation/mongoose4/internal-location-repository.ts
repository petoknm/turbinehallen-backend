import mongoose = require("mongoose");
import { IInternalLocation } from "../../../../interface/internal-location";
import { IInternalLocationRepository } from "../../contract/internal-location";
import { rejectIfNull, toObject, renameId, toObjectAll, renameIdAll } from "./helpers";

export interface IInternalLocationModel extends IInternalLocation,mongoose.Document {

}

const internalLocationSchema = new mongoose.Schema({
    name:         String,
    latitude:     Number,
    longitude:    Number,
    address:      mongoose.Schema.Types.Mixed,// IAddress
    description:  String,
    capacity:     Number,
    imagePaths:   [String],
    defaultPrice: Number
});

export const InternalLocationModel = mongoose.model<IInternalLocationModel>('InternalLocation', internalLocationSchema);

export class InternalLocationRepository implements IInternalLocationRepository {

    create(document: IInternalLocation): Promise<IInternalLocation> {
        return InternalLocationModel.create(document)
            .then(rejectIfNull('InternalLocation not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IInternalLocation): Promise<IInternalLocation> {
        return InternalLocationModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('InternalLocation not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IInternalLocation): Promise<IInternalLocation>{
        return InternalLocationModel.findByIdAndUpdate(id, document).exec();
    }

    remove(document: IInternalLocation): Promise<void> {
        return InternalLocationModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('InternalLocation not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return InternalLocationModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('InternalLocation not found'))
            .then(() => null);
    }

    findAll(): Promise<IInternalLocation[]> {
        return InternalLocationModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IInternalLocation> {
        return InternalLocationModel.findById(id).exec()
            .then(rejectIfNull('InternalLocation not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const internalLocationRepository = new InternalLocationRepository();