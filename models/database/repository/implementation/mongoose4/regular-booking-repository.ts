import mongoose = require("mongoose");
import { IRegularBooking } from "../../../../interface/regular-booking";
import { IRegularBookingRepository } from "../../contract/regular-booking-repository";
import { rejectIfNull, toObject, renameId, toObjectAll, renameIdAll } from "./helpers";

export interface IRegularBookingModel extends IRegularBooking, mongoose.Document {
}

const regularBookingSchema = new mongoose.Schema({
    customer:    mongoose.Schema.Types.ObjectId,// ICustomer
    coordinator: mongoose.Schema.Types.ObjectId,
    information: mongoose.Schema.Types.Mixed,
    price:       Number,
    payment:     String
});

export const RegularBookingModel = mongoose.model<IRegularBookingModel>('RegularBooking', regularBookingSchema);

export class RegularBookingRepository implements IRegularBookingRepository {

    create(document: IRegularBooking): Promise<IRegularBooking> {
        return RegularBookingModel.create(document)
            .then(rejectIfNull('RegularBooking not found'))
            .then(toObject)
            .then(renameId);
    }

    update(document: IRegularBooking): Promise<IRegularBooking> {
        return RegularBookingModel.findByIdAndUpdate(document.id, document, {'new': true}).exec()
            .then(rejectIfNull('RegularBooking not found'))
            .then(toObject)
            .then(renameId);
    }

    updateById(id: string, document: IRegularBooking): Promise<IRegularBooking>{
        return RegularBookingModel.findByIdAndUpdate(id, document).exec();
    }


    remove(document: IRegularBooking): Promise<void> {
        return RegularBookingModel.findByIdAndRemove(document.id).exec()
            .then(rejectIfNull('RegularBooking not found'))
            .then(() => null);
    }

    removeById(id: string): Promise<void> {
        return RegularBookingModel.findByIdAndRemove(id).exec()
            .then(rejectIfNull('RegularBooking not found'))
            .then(() => null);
    }

    findAll(): Promise<IRegularBooking[]> {
        return RegularBookingModel.find().exec()
            .then(toObjectAll)
            .then(renameIdAll);
    }

    findById(id: string): Promise<null|IRegularBooking> {
        return RegularBookingModel.findById(id).exec()
            .then(rejectIfNull('RegularBooking not found'))
            .then(toObject)
            .then(renameId);
    }

}

export const regularBookingRepository = new RegularBookingRepository();