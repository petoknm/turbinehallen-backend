export function rejectIfNull(error: string) {
    return function (document: any) {
        if (!document) {
            throw new Error(error);
        } else {
            return document;
        }
    };
}

export function toObjectAll(documents: any[]) {
    return documents.map(toObject);
}

export function toObject(document: any) {
    return document.toObject();
}

export function renameIdAll(documents: any[]) {
    return documents.map(renameId);
}

export function renameId(document: any) {
    document.id = document._id;
    delete document._id;
    return document;
}