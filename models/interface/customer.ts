import { IAddress } from "./address";
import { ObjectID } from "mongodb";

export interface ICustomer {
    id?: ObjectID;
    addresses: IAddress[];
    emailAddresses: string[];
    //kind is a discriminator
    kind: string;
}