import { ILocation } from "./location";
import { ObjectID } from "mongodb";

export interface IExternalLocation extends ILocation {
    id?: ObjectID;
}