import { ICustomer } from "./customer";
import { ObjectID } from "mongodb";

export interface IBusinessCustomer extends ICustomer {
    id?: ObjectID;
    name: string;
    cvr: string;
}