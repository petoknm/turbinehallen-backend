import { IBooking } from "./booking";
import { ObjectID } from "mongodb";
import { IRegularBookingInformation } from "./regular-booking-information";

export interface IRegularBooking extends IBooking {
    id?: ObjectID;
    information: IRegularBookingInformation;
}