import { IEventInformation } from "./event-information";

export interface IExternalEventInformation extends IEventInformation{
    service: string,
    drinks: string,
    tableSettings: string,
    extension: string,
    pickedUp: string,
    numberOfParticipants: number
}
