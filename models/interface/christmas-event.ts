import { IEvent } from "./event";
import { ObjectID } from "mongodb";
import { IChristmasEventInformation } from "./christmas-event-information";

export interface IChristmasEvent extends IEvent {
    id?: ObjectID;
    date: Date;
    pricePerSeat: number;
    information: IChristmasEventInformation;
}