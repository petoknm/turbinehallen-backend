import { IAddress } from "./address";
import { ObjectID } from "mongodb";

export interface ILocation {
    id?: ObjectID;
    name: string;
    latitude: number;
    longitude: number;
    address: IAddress;
    description: string;
    //kind is a discriminator so we can check against the interface
    kind: string;
}