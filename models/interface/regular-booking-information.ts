import { IBookingInformation } from "./booking-information";

export interface IRegularBookingInformation extends IBookingInformation {
    price: number,
    payment: string
}
