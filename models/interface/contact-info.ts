import { ObjectID } from "mongodb";
export interface IContactInfo {
    id?: ObjectID;
    firstName: string;
    lastName: string;
    phoneNumbers: string[];
    emailAddresses: string[];
}