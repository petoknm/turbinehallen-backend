import { IBooking } from "./booking";
import { ObjectID } from "mongodb";
import { IExternalBookingInformation } from "./external-booking-information";

export interface IExternalBooking extends IBooking {
    id?: ObjectID;
    information: IExternalBookingInformation;
}