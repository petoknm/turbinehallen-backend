import { ILocation } from "./location";
import { IBooking } from "./booking";
import { ObjectID } from "mongodb";
import { IEventInformation } from "./event-information";

export interface IEvent {
    id?: ObjectID;
    place: ILocation | ObjectID;
    description: string;
    bookings: IBooking[] | ObjectID[];
    name: string;
    type: string;
    information: IEventInformation;

    getTotalPrice(): number;
}