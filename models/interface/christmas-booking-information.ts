import { IBookingInformation } from "./booking-information";

export interface IChristmasBookingInformation extends IBookingInformation {
    numberOfParticipants: number,
    price: number
}
