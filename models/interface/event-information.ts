import { ObjectID } from "mongodb";

export interface IEventInformation {
    id?: ObjectID;
    program: {
        time: string,
        description: string
    }[],
    price: string,
    payment: string
}
