export interface IAddress {
    country: string;
    city: string;
    postcode: string;
    street: string;
}