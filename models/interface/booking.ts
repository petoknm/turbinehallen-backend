import { ICustomer } from "./customer";
import { ObjectID } from "mongodb";
import { IBookingInformation } from "./booking-information";
import { IUser } from "./oauth2/user";

export interface IBooking {
    id?: ObjectID;
    information: IBookingInformation;
    customer: ICustomer | ObjectID;
    coordinator: IUser | ObjectID;
    price: number;
    payment: string;
}