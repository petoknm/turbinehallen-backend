import { IEvent } from "./event";
import { IContactInfo } from "./contact-info";
import { ObjectID } from "mongodb";
import { IExternalEventInformation } from "./external-event-information";
import { IBookingInformation } from "./booking-information";
import { IExternalBooking } from "./external-booking";

export interface IExternalEvent extends IEvent {
    id?:            ObjectID;
    startDate:      Date;
    endDate:        Date;
    totalPrice:     number;
    information:    IExternalEventInformation;
    contactInfo:    IContactInfo[];
    bookings:       IExternalBooking[] | ObjectID[];
}