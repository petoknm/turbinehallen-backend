import { IBooking } from "./booking";
import { IContactInfo } from "./contact-info";
import { ObjectID } from "mongodb";
import { IChristmasBookingInformation } from "./christmas-booking-information";

export interface IChristmasBooking extends IBooking {
    id?: ObjectID;
    contactInfo: IContactInfo[];
    information: IChristmasBookingInformation;
}