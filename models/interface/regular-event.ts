import { IEvent } from "./event";
import { IContactInfo } from "./contact-info";
import { ObjectID } from "mongodb";
import { IRegularEventInformation } from "./regular-event-information";

export interface IRegularEvent extends IEvent {
    id?:            ObjectID;
    startDate:      Date;
    endDate:        Date;
    totalPrice:     number;
    contactInfo:    IContactInfo[];
    information:    IRegularEventInformation;
}