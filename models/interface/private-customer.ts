import { ICustomer } from "./customer";
import { ObjectID } from "mongodb";

export interface IPrivateCustomer extends ICustomer {
    id?: ObjectID;
    firstName: string;
    lastName: string;
    phoneNumbers: string[];
}