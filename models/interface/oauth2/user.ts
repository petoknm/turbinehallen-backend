import { ObjectID } from "mongodb";

export interface IUser {
    id?: ObjectID;
    firstName: string;
    lastName: string;
    email: string;
    password: string;   // bcrypted password
    roles: string[]
}