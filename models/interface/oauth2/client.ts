import { IUser } from "./user";
import { ObjectID } from "mongodb";

export interface IClient {
    id?: ObjectID;
    name: string;
    secret: string;   // TODO hash
    redirectUri: string;
    user: IUser | ObjectID;
}