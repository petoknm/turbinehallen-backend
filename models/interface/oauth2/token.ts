import { IUser } from "./user";
import { IClient } from "./client";
import { ObjectID } from "mongodb";

export interface IToken {
    id?: ObjectID;
    value: string;
    refreshToken: string;
    expiresIn: Date;
    user: IUser | ObjectID;
    client: IClient | ObjectID;
}