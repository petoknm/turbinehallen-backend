import { IUser } from "./user";
import { IClient } from "./client";
import { ObjectID } from "mongodb";

export interface ICode {
    id?: ObjectID;
    value: string;
    user: IUser | ObjectID;
    client: IClient | ObjectID;
}