import { IEventInformation } from "./event-information";

export interface IRegularEventInformation extends IEventInformation {
    menu: {
        normal: {
            title: string,
            description: string
        }[],
        special: string
    },
    drinks: {
        description: string,
        longDrinks: string[]
    },
    extraMaterial: string,
    entertainment: string,
    tableSettings: {
        title: string,
        descriptions: string[]
    }[],
    tableLinen: string,
    extension: string,
    numberOfParticipants: number,
    aproxNumberOfParticipants: number // It is reflected in the confirmation pdf
}
