import { IEventInformation } from "./event-information";

export interface IChristmasEventInformation extends IEventInformation {
    menu: {
        normal: {
            title: string,
            description: string
        }[],
        special: string
    },
    drinks: string,
    coffee: string,
    bar: string,
    personalBill: string,
    drinksOrders: string,
    specialWishes: string,
    entertainment: string,
    wardRobe: string,
    dropoutAfterFinalBill: string
}
