import { ILocation } from "./location";
import { ObjectID } from "mongodb";

export interface IInternalLocation extends ILocation {
    id?: ObjectID;
    capacity: number;
    imagePaths: string[];
    defaultPrice: number;
}