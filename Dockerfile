FROM node:6-slim

RUN apt-get update && \
    apt-get install -y \
        gcc \
        g++ \
        make \
        python && \
    apt-get clean

WORKDIR /backend

ADD ./ /backend

RUN npm install && \
    npm run compile

CMD node index.js